#pragma once
#include "pattern.h"
#include "radamsa-rose/radamsa_definitions.h"

namespace Radamsa {
    class PatternAdapter : public Pattern {
    private:
        struct input_chunk* (*runFunc)(struct input_chunk*, radamsa_mutation, struct radamsa_constants, radamsa_random);
    public:
        PatternAdapter(struct input_chunk* (*runFunc)(struct input_chunk*, radamsa_mutation, struct radamsa_constants, radamsa_random), int pri, struct radamsa_constants constants, Random& random, std::string name);
        std::deque<std::string> run(std::deque<std::string> inputChunks, Radamsa::Mutation *mutation) override;
    protected:
        std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) override;
    };
}