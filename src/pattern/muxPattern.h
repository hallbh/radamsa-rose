#pragma once
#include "pattern.h"

namespace Radamsa {
	class muxPattern: public Pattern{
	private:
		// struct radamsa_constants constants;
		// Random* random;
		std::vector<Radamsa::Pattern*> patternList;
	public:
		muxPattern(int priority, struct radamsa_constants constants, Random& random);
		std::deque<std::string> run(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) override;
		void addPattern(Radamsa::Pattern* pat);
	protected:
		std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) override;
	};
}