#pragma once
#include <string>
#include <deque>
#include "radamsa-rose/constants.h"
#include "mutation/mutation.h"

namespace Radamsa {
	class Pattern {
	private:
		std::string name;
	public:
		int priority;
		Pattern(int pri, struct radamsa_constants constants, Random& random, std::string name);
		virtual ~Pattern();
		virtual std::deque<std::string> run(std::deque<std::string> inputChunks, Radamsa::Mutation *mutation);
		std::string getName();
	protected:
		struct radamsa_constants constants;
		int mutationsRun;
		Random* random;
		virtual std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) = 0;
	};
}