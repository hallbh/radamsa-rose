#include "pat_burst.h"

/// BurstMutationsPattern
///
/// @param pri int repesents priority
/// @param constants radamsa_constants object
/// @param random a random object

Radamsa::BurstMutationsPattern::BurstMutationsPattern(int pri, radamsa_constants constants, Random& random) : Pattern(pri, constants, random, "bu") {}

/// BurstMutationsPattern will perform two or more mutations on the same block
///
/// @param inputChunks queue of string
/// @param mutation radamsa mutation pointer

std::deque<std::string> Radamsa::BurstMutationsPattern::applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
	// Apply the same mutation to the selected chunk at least one more time
	this->random->doesOccur(this->constants.remutateProbability); // Aki calls does occur once before running the mutations
	do {
		mutation->mutate(inputChunks);
	} while (this->random->doesOccur(this->constants.remutateProbability) && ++this->mutationsRun < this->constants.maxMutations);
	this->mutationsRun = 0;

	return inputChunks;
}