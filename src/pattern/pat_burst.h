#pragma once
#include "pattern.h"

namespace Radamsa {
	class BurstMutationsPattern : public Pattern {
	public:
		BurstMutationsPattern(int pri, struct radamsa_constants constants, Random& random);
	protected:
		std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) override;
	};
}