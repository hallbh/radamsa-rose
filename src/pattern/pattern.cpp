#include "pattern.h"
#include <stdexcept>


/// General Pattern constructer
///
/// Pattern object keep track of how many times mutations have been called
///
/// @param pri int
/// @param constants a radamsa_constants object
/// @param random a random object
/// @param name string represents name of pattern


Radamsa::Pattern::Pattern(int pri, struct radamsa_constants constants, Random& random, std::string name) {
	this->priority = pri;
	this->constants = constants;
	this->name = name;
	this->mutationsRun = 0;
	this->random = &random;
}

Radamsa::Pattern::~Pattern() {
	// Nothing to deconstruct
}

/// General pattern that apply the mutation on the first chunk of input, 
/// and then output the mutated inputChunks.
///
/// @param inputChunks a queue of string that needs to be broken
/// @param mutation a mutation pointer 
std::deque<std::string> Radamsa::Pattern::run(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
	if (this->constants.initialIp < 1) {
		throw std::logic_error("Initial Inverse Probability cannot be less than 1");
	}

	int ip = this->random->nextInteger(this->constants.initialIp);
	if (inputChunks.empty() || this->constants.maxMutations < 1)
		return inputChunks;
	std::deque<std::string> out;

	// Move through the chunks with a 1/(n + i) probability of choosing chunk i
	while (this->random->nextInteger(ip) != 0 && inputChunks.size() > 1) {
		out.push_back(inputChunks.front());
		inputChunks.pop_front();
	}

	mutation->mutate(inputChunks);
	this->mutationsRun++;

	if (this->constants.maxMutations > 1) {
		std::deque<std::string> patOut = this->applyPattern(std::move(inputChunks), mutation);
		while (!patOut.empty()) {
			out.push_back(patOut.front());
			patOut.pop_front();
		}
	} else {
		while (!inputChunks.empty()) {
			out.push_back(inputChunks.front());
			inputChunks.pop_front();
		}
	}

	return out;
}

/// General pattern getter 

std::string Radamsa::Pattern::getName() {
	return this->name;
}