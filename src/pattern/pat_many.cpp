#include "pat_many.h"

/// ManyMutationsPattern
///
/// @param pri int repesents priority
/// @param constants radamsa_constants object
/// @param random a random object

Radamsa::ManyMutationsPattern::ManyMutationsPattern(int pri, radamsa_constants constants, Random& random) : Pattern(pri, constants, random, "nd") {}


/// ManyMutationsPattern has the possibility of mutating more 
/// generated blocks in the line.
///
/// @param inputChunks queue of string
/// @param mutation radamsa mutation pointer

std::deque<std::string> Radamsa::ManyMutationsPattern::applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
	if (this->random->doesOccur(this->constants.remutateProbability) && this->mutationsRun < this->constants.maxMutations)
		return this->run(std::move(inputChunks), mutation);
	this->mutationsRun = 0;
	return inputChunks;
}