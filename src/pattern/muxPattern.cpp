#include "muxPattern.h"
#include "mutation/mutation.h"

/// General muxPattern constructer
///
/// @param priority int
/// @param constants radamsa_constant
/// @param random a random object

Radamsa::muxPattern::muxPattern(int priority, struct radamsa_constants constants, Random& random) : Pattern(priority, constants, random, "muxPattern") {
	this->constants = constants;
	this->random = &random;
}

bool comparePatternByPriority(Radamsa::Pattern* pat1, Radamsa::Pattern* pat2) {
	return pat1->priority > pat2->priority;
}

/// A Pattern picks a pattern from pat_burst, pat_many and pat_once
/// by sorting pattern list and comparing pattern by pripority.
///
/// @param input string 

std::deque<std::string> Radamsa::muxPattern::run(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
	std::sort(this->patternList.begin(), this->patternList.end(), comparePatternByPriority);

	int priSum = 0;
	for (Radamsa::Pattern* p : this->patternList) {
		priSum += p->priority;
	}
	int n = this->random->nextInteger(priSum);

	for (auto p = this->patternList.begin(); p != this->patternList.end(); ++p) {
		int currentPri = (*p)->priority;
		if (n < currentPri) {
			return (*p)->run(std::move(inputChunks), mutation);
		}
		n -= currentPri;
	}
	return inputChunks;
}

void Radamsa::muxPattern::addPattern(Radamsa::Pattern* pat) {
	this->patternList.push_back(pat);
}

std::deque<std::string> Radamsa::muxPattern::applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
	return inputChunks;
}

