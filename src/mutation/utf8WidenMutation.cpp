#include "utf8WidenMutation.h"

/// utf8WidenMutation Constructer
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

Radamsa::utf8WidenMutation::utf8WidenMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "uw") {
	this->constants = constants;
	this->random = &random;
    this->del = 0;
}

/// A mutation hits a 6-bit ascii char and expands it into 
/// 8-bit ascii char by adding 11 in the front
///
/// @param input string 

std::string Radamsa::utf8WidenMutation::mutate(std::string input) {
    std::string out = std::string(input);

    int pos = random->nextInteger(input.length());
    del = random->randomDelta();
    if (input.empty()) return input; // Blame edit-byte-vector
    if ((input[pos] & 0b111111) == input[pos]) {
        out[pos] = input[pos] | 0b10000000;
        out.insert(pos, std::string({ (char) 0b11000000 }));
    }

    return out;
}
    
int Radamsa::utf8WidenMutation::nextDelta() {
	return del;
}