#include "bytePermuteMutation.h"

/// Byte permute mutation construstor 
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

Radamsa::BytePermuteMutation::BytePermuteMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "bp") {
	this->constants = constants;
	this->random = &random;
}

/// A mutation permute byte
///
/// This mutation permute a byte value at random index in certain range.
/// If the input string is empty, return empty string.
///
/// @param input an string

std::string Radamsa::BytePermuteMutation::mutate(std::string input) {
	int index = this->random->nextInteger(input.size());
	this->delta = this->random->randomDelta();

	Radamsa::Random rand(*(this->random));
	int range = rand.nextIntegerRange(2,20);
	if (input.empty()) return input; // Blame edit-byte-vector
	std::vector<char> v(input.begin(), input.end());
	rand.permute(v, index, index+range);
	std::string res(v.begin(), v.end());
	return res;
}

int Radamsa::BytePermuteMutation::nextDelta() {
	return this->delta;
}

