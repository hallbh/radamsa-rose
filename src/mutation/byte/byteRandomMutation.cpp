#include "byteRandomMutation.h"

/// Byte random mutation construstor 
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

Radamsa::ByteRandomMutation::ByteRandomMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "ber") {
	this->constants = constants;
	this->random = &random;
}

/// A mutation random byte
///
/// This mutation insert a random value at random index.
/// If the input is empty string, 
/// the mutation returns the random value. 
///
/// @param input an string

std::string Radamsa::ByteRandomMutation::mutate(std::string input) {
	int len = input.length();
	int index = this->random->nextInteger(len);
	this->delta = this->random->randomDelta();
	int amount = this->random->nextInteger(256);
	if (len > 0) {
		input[index] = char(amount);
		return input;
	}
	return input;
}

int Radamsa::ByteRandomMutation::nextDelta() {
	return this->delta;
}

