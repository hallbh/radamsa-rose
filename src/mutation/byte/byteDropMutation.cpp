#include "byteDropMutation.h"
#include "iostream"


/// Byte drop mutation construstor 
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

Radamsa::ByteDropMutation::ByteDropMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "bd") {
	this->constants = constants;
	this->random = &random;
}

/// A mutation drop byte
///
/// This mutation drops a random byte value.
/// If input string is empty, it will return empty string.
///
/// @param input an string

std::string Radamsa::ByteDropMutation::mutate(std::string input) {
	int len = input.length();
	int index = this->random->nextInteger(len);
	this->delta = this->random->randomDelta();
	if (len > 0) {
		input.erase(index, 1);
		return input;
	}
	return input;
}

int Radamsa::ByteDropMutation::nextDelta() {
	return this->delta;
}

