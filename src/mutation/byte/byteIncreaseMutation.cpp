#include "byteIncreaseMutation.h"

/// Byte increase mutation construstor 
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

Radamsa::ByteIncreaseMutation::ByteIncreaseMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "bei") {
	this->constants = constants;
	this->random = &random;
}

/// A mutation increase byte
///
/// This mutation increase a random byte value mod 256.
/// If input string is empty,
/// the mutation will insert start of header.
///
/// @param input an string


std::string Radamsa::ByteIncreaseMutation::mutate(std::string input) {
	int len = input.length();
	int index = this->random->nextInteger(len);
	this->delta = this->random->randomDelta();
	if (len > 0) {
		input[index] = char((input[index] + 1) % 256);
		return input;
	}
	return input;
}

int Radamsa::ByteIncreaseMutation::nextDelta() {
	return this->delta;
}

