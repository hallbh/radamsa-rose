#pragma once
#include "mutation/mutation.h"

namespace Radamsa {
	class ByteFlipMutation : public Radamsa::Mutation {
	private:
		struct radamsa_constants constants;
		Random* random;
		int delta;
	public:
		ByteFlipMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input) override;
		int nextDelta() override;
	};
}