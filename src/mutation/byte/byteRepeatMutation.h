#pragma once
#include "mutation/mutation.h"

namespace Radamsa {
	class ByteRepeatMutation : public Radamsa::Mutation {
	private:
		struct radamsa_constants constants;
		Random* random;
		int delta;
	public:
		ByteRepeatMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input) override;
		int nextDelta() override;
		int repeatLen();
	};
}