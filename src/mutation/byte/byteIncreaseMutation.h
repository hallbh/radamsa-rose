#pragma once
#include "mutation/mutation.h"

namespace Radamsa {
	class ByteIncreaseMutation : public Radamsa::Mutation {
	private:
		struct radamsa_constants constants;
		Random* random;
		int delta;
	public:
		ByteIncreaseMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input) override;
		int nextDelta() override;
	};
}