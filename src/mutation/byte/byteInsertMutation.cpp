#include "byteInsertMutation.h"

/// Byte insert mutation construstor 
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

Radamsa::ByteInsertMutation::ByteInsertMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "bi") {
	this->constants = constants;
	this->random = &random;
}

/// A mutation insert byte
///
/// This mutation insert a random value at random index.
///
/// @param input an string

std::string Radamsa::ByteInsertMutation::mutate(std::string input) {
	int index = this->random->nextInteger(input.length());
	this->delta = this->random->randomDelta();
	int amount = this->random->nextInteger(256);
    if (!input.empty()) { // Blame edit-byte-vector
	   input.insert(index, 1, amount);
    }
	return input;
}

int Radamsa::ByteInsertMutation::nextDelta() {
	return this->delta; // return delta
}

