#pragma once
#include <string>
#include <deque>
#include "radamsa-rose/constants.h"
#include "random/random.h"

namespace Radamsa {
	class Mutation {
	public:
		int score;
		int priority;

		Mutation(int score, int pri, std::string name);
		virtual ~Mutation();
		virtual void mutate(std::deque<std::string>& input);
		virtual std::string mutate(std::string input) = 0;
		virtual int nextDelta() = 0;
		std::string getName();
	private:
		std::string name;
	};
}