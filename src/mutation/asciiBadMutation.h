#pragma once
#include "mutation.h"
#include "asciiBadUtils.h"

namespace Radamsa {
	class AsciiBadMutation : public Radamsa::Mutation {
	private:
		struct radamsa_constants constants;
		Random* random;
		int delta;

		void stringMutate(std::vector<AsciiNode>& chunks, int l);
		void mutateTextData(std::string& data);
		std::string randomBadness();
		std::string randomSilly();
		int randAsCount();
	public: 
		std::string sillyStrings[32] {"%n", "%n", "%s", "%d", "%p", "%#x", 
               "\\0", "aaaa%d%n", 
               "`xcalc`", ";xcalc", "$(xcalc)", "!xcalc", "\"xcalc", "'xcalc", 
               "\\x00", "\\r\\n", "\\r", "\\n", "\\x0a", "\\x0d", 
               "NaN", "+inf", 
               "$PATH", 
               "$!!", "!!", "&#000;", "\\u0000", 
			   "$&", "$+", "$`", "$'", "$1" };

		AsciiBadMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input) override;
		int nextDelta();
	};
}