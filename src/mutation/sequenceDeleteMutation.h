#pragma once
#include "mutation.h"

namespace Radamsa {
	class SequenceDeleteMutation : public Radamsa::Mutation {
	private:
		int delta;
		Random* random;
		struct radamsa_constants constants;
	public:
		SequenceDeleteMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input) override;
		void mutate(std::deque<std::string>& input) override;
		int nextDelta() override;
	};
}