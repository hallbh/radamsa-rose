#pragma once
#include "mutation.h"
#include <locale>
#include <codecvt>

namespace Radamsa {
	class utf8InsertMutation : public Radamsa::Mutation {
	private:
		int del;
		struct radamsa_constants constants;
		Random* random;
		std::wstring_convert<std::codecvt_utf8<char32_t>,char32_t> conversion;
	public: 
		utf8InsertMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input) override;
		int nextDelta() override;
	};
}