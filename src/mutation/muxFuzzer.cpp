#include "muxFuzzer.h"
#include <algorithm>
#include <functional>
#include <queue>

bool Radamsa::compareMutaPriorities(std::pair<int, Radamsa::Mutation*> p1, std::pair<int, Radamsa::Mutation*> p2) {
	return p1.first < p2.first;
}

/// General muxFuzzer constructer
///
/// @param priority int
/// @param constants radamsa_constant
/// @param random a random object

Radamsa::muxFuzzer::muxFuzzer(int pri, radamsa_constants constants, Random& random) : Mutation(0, pri, "muxFuzzer") {
	this->constants = constants;
	this->random = &random;
}

void Radamsa::muxFuzzer::weightedPermutation() {
	int i = this->mutationList.size();
	for (auto rit = this->mutationList.rbegin(); rit != this->mutationList.rend(); ++rit) {
		int ppri = this->random->nextInteger((*rit)->priority * (*rit)->score);
		std::tuple<int, int, Radamsa::Mutation*> elem(ppri, --i, (*rit));
		this->mutationQueue.push(elem);
	}
	this->mutationList.clear();
}

int Radamsa::muxFuzzer::adjustPriority(int pri, int delta) {
	if (delta == 0) {
		return pri;
	}
	int sum = delta + pri;
	return std::max(this->constants.minScore, std::min(this->constants.maxScore, sum));
}

/// A Mutation picks a mutation from all mutations
/// by sorting mutation list and comparing mutation by pripority.
///
/// @param input string 

void Radamsa::muxFuzzer::mutate(std::deque<std::string>& input) {
	if (!input.empty()) {
		std::deque<std::string> intialInput(input);
		weightedPermutation();

		while (!this->mutationQueue.empty()) {
			Mutation* m = std::get<2>(this->mutationQueue.top());
			this->mutationQueue.pop();
			m->mutate(input);
			m->score = adjustPriority(m->score, m->nextDelta());
			if (input.empty() || intialInput.front() != input.front()) {
				this->mutationList.push_back(m);
				this->emptyPriorityQueue();
				return;
			}
			this->mutationList.push_back(m);

			input.swap(intialInput);
		}
	}
}

std::string Radamsa::muxFuzzer::mutate(std::string input) {
	// This function should not be used
	return input;
}

int Radamsa::muxFuzzer::nextDelta() {
	// Won't be used
	return 0;
}

/// Take in mutation pointer, and add it to mutation list.
///
/// @param mut mutation pointer

void Radamsa::muxFuzzer::addMutation(Radamsa::Mutation* mut) {
	this->mutationList.push_back(mut);
}

void Radamsa::muxFuzzer::emptyPriorityQueue() {
	while (!this->mutationQueue.empty()) {
		this->mutationList.push_front(std::get<2>(this->mutationQueue.top()));
		this->mutationQueue.pop();
	}
}