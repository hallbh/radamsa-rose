#include "mutation_adapter.h"
#include "chunk_queue_interface.h"
#include <stdexcept>

/// Mutation Adapter Constructer
///
/// By using adapter, incompatible mutation can collaborate.
///
/// @param innerMutation mutation object

Radamsa::MutationAdapter::MutationAdapter(int (*mutateFunction)(struct input_chunk*, struct radamsa_constants, radamsa_random), std::string name, int priority, radamsa_constants constants, Radamsa::Random& random) : Mutation(0, priority, name) {
	this->mutateFunction = mutateFunction;
}

void Radamsa::MutationAdapter::mutate(std::deque<std::string>& input) {
    if (!input.empty()) {
        struct input_chunk* inputHead = Radamsa::toLinkedStruct(input);
        this-> delta = this->mutateFunction(inputHead, this->constants, this->random);
        std::deque<std::string> newInput = Radamsa::fromLinkedStruct(inputHead);
        input.swap(newInput);
    }
}

std::string Radamsa::MutationAdapter::mutate(std::string input) {
	throw std::logic_error("In mutate std::string in adapter");
}

int Radamsa::MutationAdapter::nextDelta() {
	return this->delta;
}
