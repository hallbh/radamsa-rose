#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include "../../include/radamsa-rose/constants.h"

namespace Radamsa {
	class AsciiNode {
	public:
		std::string type;
		std::string value;

		AsciiNode(std::string type, std::string value);

		bool operator == (AsciiNode const& otherNode);
	};

	class AsciiBadUtils {
	private:
		struct radamsa_constants constants;

	public:
		AsciiBadUtils(struct radamsa_constants constants);
		
		char delimiterOf(char c);
		bool texty(char c);
		bool textyEnough(std::string input);
		int stringyLength(std::vector<Radamsa::AsciiNode> chunks);

		void flush(std::string type, std::string bytes, std::vector<Radamsa::AsciiNode>& chunks);
		
		void step(std::string& input, std::string& rawr, std::vector<Radamsa::AsciiNode>& chunks);
		void stepText(std::string& input, std::string& seenr, std::vector<Radamsa::AsciiNode>& chunks);
		void stepDelimited(std::string& input, char delim, std::string afterr, std::string prevr, std::vector<Radamsa::AsciiNode>& chunks);

		std::vector<Radamsa::AsciiNode> stringLex(std::string input);
		std::string stringUnlex(std::vector<Radamsa::AsciiNode> chunks);
	};
}