#include "asciiBadMutation.h"

Radamsa::AsciiBadMutation::AsciiBadMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "ab") {
	this->constants = constants;
	this->random = &random;
	this->delta = 0;
}

std::string Radamsa::AsciiBadMutation::mutate(std::string input) {
	std::string output = "";
	Radamsa::AsciiBadUtils utils = AsciiBadUtils(constants);

	std::vector<Radamsa::AsciiNode> chunks = utils.stringLex(input);
	int l = utils.stringyLength(chunks);
	if (l > 0) {
		stringMutate(chunks, l);
		delta = random->randomDeltaUp(constants.pWeakUsually);
		output = utils.stringUnlex(chunks);
	}
	else {
		delta = -1;
		return input;
	}
	return output;
}

void Radamsa::AsciiBadMutation::stringMutate(std::vector<AsciiNode>& chunks, int l)
{
	int p = random->nextInteger(l);
	AsciiNode which = chunks[p];

	if (which.type == "delimited") {
		char delim = which.value[0];
		std::string delimited = which.value.substr(1, which.value.length() - 2);
		mutateTextData(delimited);
		which.value = delim + delimited + delim;
	}
	else if (which.type == "byte") {
		stringMutate(chunks, l);
	}
	else if (which.type == "text") {
		mutateTextData(which.value);
	}

	chunks[p] = which;
}

void Radamsa::AsciiBadMutation::mutateTextData(std::string& data)
{
	std::string bad;
	std::string::iterator it;
	int p = 0;
	int pos = 0;
	int n = 0;
	int x = random->nextInteger(4);
	switch (x) {
	case 0: // insert badness
		p = random->nextInteger(data.size());
		bad = randomBadness();
		data.insert(p, bad);
		break;
	case 1: // replace badness
		p = random->nextInteger(data.size());
		bad = randomBadness();
		it = data.begin();
		for (; p > 0; p--) it++;
		data.replace(it, data.end(), bad);
		break;
	case 2: // insert as (beep character '\a')
		n = randAsCount();
		pos = random->nextInteger(data.size());
		for (; n > 0; n--) data.insert(pos, "\a");
		break;
	case 3:
	default:
		data.push_back('\0');
		break;
	}
}

std::string Radamsa::AsciiBadMutation::randomBadness()
{
	int n = random->nextInteger(20);
	std::string out = "";
	for (n + 1; n >= 0; n--) {
		std::string x = randomSilly();
		out = x + out;
	}
	return out;
}

std::string Radamsa::AsciiBadMutation::randomSilly()
{
	int p = random->nextInteger(32);// sillyStrings->size());
	return sillyStrings[p];
}

int Radamsa::AsciiBadMutation::randAsCount()
{
	int type = random->nextInteger(12);
	switch(type) {
	case 0:
		return 127;
		break;
	case 1:
		return 128;
		break;
	case 2:
		return 255;
		break;
	case 3:
		return 256;
		break;
	case 4:
		return 16383;
		break;
	case 5:
		return 16384;
		break;
	case 6:
		return 32767;
		break;
	case 7:
		return 32768;
		break;
	case 8:
		return 65535;
		break;
	case 9:
		return 65536;
		break;
	default:
		return random->nextInteger(1024);
		break;
	}
}

int Radamsa::AsciiBadMutation::nextDelta() {
	return delta;
}