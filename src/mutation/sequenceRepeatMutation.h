#pragma once

#include "mutation.h"

namespace Radamsa {
	class SequenceRepeatMutation : public Radamsa::Mutation {
	private:
		int delta;
		Random* random;
		struct radamsa_constants constants;
	public:
		SequenceRepeatMutation(int pri, struct radamsa_constants constants, Random& random);
		void mutate(std::deque<std::string> &input) override;
		std::string mutate(std::string input) override;
		int nextDelta();
	};
}