#include "fuse_next_mutation.h"
#include <list>

#define toVec(s) std::vector<char>(s.begin(), s.end())
#define toStr(s) std::string(s.begin(), s.end())

Radamsa::FuseNextMutation::FuseNextMutation(int priotity, const struct radamsa_constants constants, Random& random) : AbstractFuseMutation(priotity, constants, random, "fn") {}

void Radamsa::FuseNextMutation::mutate(std::deque<std::string>& input) {
    std::string firstBlock = input.front();
    input.pop_front();

    std::string fbHalfA = firstBlock.substr(0, (firstBlock.size() + 1) / 2);
    std::string fbHalfB = firstBlock.substr((firstBlock.size() + 1) / 2);

    std::string secondBlock;
    if (!input.empty()) {
        secondBlock = input.front();
        input.pop_front();
    } else {
        secondBlock = firstBlock;
    }

    std::vector<char> abl = this->fuse(toVec(fbHalfA), toVec(secondBlock));
    std::vector<char> abal = this->fuse(abl, toVec(fbHalfB));

    // Put the input back together
    input.push_front(std::string(abal.begin(), abal.end()));
}

std::string Radamsa::FuseNextMutation::mutate(std::string input) {
    // This function is not needed by fuse-next
    return input;
}