#pragma once
#include "mutation/mutation.h"
#include <vector>
#include <utility>
#include <map>
#include <list>

namespace Radamsa {
    class AbstractFuseMutation : public Mutation {
    public:
        AbstractFuseMutation(int priotity, const struct radamsa_constants constants, Random& random, std::string name);
        int nextDelta() override;
    protected:
        Random* random;
        struct radamsa_constants constants;

        std::vector<char> fuse(std::vector<char> listOne, std::vector<char> listTwo);
    // private:
    //     std::map<char, std::list<std::vector<char>>> createCharSuffixes(std::vector<std::vector<char>> suffixes);
    //     std::vector<std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>>> split(std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>> node);
    //     std::pair<std::vector<char>, std::vector<char>> getAnyPositionPair(std::vector<std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>>> nodes);
    //     std::pair<std::vector<char>, std::vector<char>> findJumpPoints(std::vector<char> listOne, std::vector<char> listTwo);
    //     std::vector<char> jump(std::vector<char> list, std::pair<std::vector<char>, std::vector<char>> jumpPoints);
    //     std::vector<std::vector<char>> createSuffixes(std::vector<char> list);
    //     std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>> createAlternatingSuffixes(std::vector<char> listOne);
    //     std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>> createInitialSuffixes(std::vector<char> listOne, std::vector<char> listTwo);
    };
}