#pragma once
#include "abstract_fuse_mutation.h"

namespace Radamsa {
    class FuseThisMutation : public AbstractFuseMutation {
    public:
        FuseThisMutation(int priotity, const struct radamsa_constants constants, Random& random);
        std::string mutate(std::string input) override;
    };
}