#include "fuse_this_mutation.h"

Radamsa::FuseThisMutation::FuseThisMutation(int priotity, const struct radamsa_constants constants, Random& random) : AbstractFuseMutation(priotity, constants, random, "ft") {}

std::string Radamsa::FuseThisMutation::mutate(std::string input) {
    std::vector<char> bytes(input.begin(), input.end());
    bytes = this->fuse(bytes, bytes);
    return std::string(bytes.begin(), bytes.end());
}