#include "sequenceDeleteMutation.h"

Radamsa::SequenceDeleteMutation::SequenceDeleteMutation(int pri, struct radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "sd")
{
	this->delta = 0;
	this->random = &random;
	this->constants = constants;
};

std::string Radamsa::SequenceDeleteMutation::mutate(std::string input) {
	std::string output(input);
	int startIndex = random->nextInteger(output.size());
	int length = random->nextInteger(output.size() - startIndex);
	auto startIterator = output.begin() + startIndex;
	output.erase(startIterator, startIterator + length);
	this->delta = random->randomDelta();
	return output;
}
void Radamsa::SequenceDeleteMutation::mutate(std::deque<std::string>& input) {
	auto toMutate = input.front();
	input.pop_front();
	auto out = mutate(toMutate);
	if (!out.empty()) input.push_front(out); // Blame edit-byte-vector
}

int Radamsa::SequenceDeleteMutation::nextDelta() {
	return delta;
}