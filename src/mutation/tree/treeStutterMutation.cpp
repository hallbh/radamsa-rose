#include "treeStutterMutation.h"
#include "treeUtils.h"
#include <regex>
#include <vector>

Radamsa::TreeStutterMutation::TreeStutterMutation(int pri, struct radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "tr")
{
	this->delta = 0; 
	this->random = &random;
	this->constants = constants;
};

std::string Radamsa::TreeStutterMutation::mutate(std::string input) {
	TreeUtils utils = TreeUtils();
	delta = -1;

	std::map<std::string, std::string> sublists = utils.sublists(input);
	

	std::vector<int> permuteHack;
	for (int i = 0; i < sublists.size(); i++) permuteHack.push_back(i);

	random->permute(permuteHack, 0, permuteHack.size());
	if (permuteHack.size() == 0) {
		random->generateNext();
		random->generateNext();
		return input;
	}

	for (int i = 0; i < permuteHack.size(); i++) {
		int index = permuteHack[i];
		auto it = sublists.begin();
		for (index; index > 0; index--, it++);

		// see if there is a sub-sublist.
		std::vector<std::string> matches = std::vector<std::string>();
		std::smatch m;
		std::regex sub("#[0-9]*#");
		std::string parentTemp = it->second;

		while (std::regex_search(parentTemp, m, sub)) {
			matches.push_back(m.str());
			parentTemp = m.suffix();
		}
		if (matches.size() > 0) {
			delta = 1;

			int matchIndex = random->nextInteger(matches.size());
			unsigned long long repeats = random->nextLogInteger(10);

			int pos = it->second.find(matches[matchIndex]);
			std::string prefix = it->second.substr(0, pos);
			std::string suffix = it->second.substr(pos + matches[matchIndex].size());

			if (repeats == 0) { // Radamsa does it at least once
				it->second = prefix + it->second + suffix;
			}
			for (unsigned long long i = 0ULL; i < repeats; i++) {
				it->second = prefix + it->second + suffix;
			}

			utils.flatten(input, sublists);
			return input;
		}

	}

	utils.flatten(input, sublists);
	return input;
}

int Radamsa::TreeStutterMutation::nextDelta() {
	return delta;
}