#pragma once
#include "../mutation.h"

namespace Radamsa {
	class TreeDeleteMutation : public Radamsa::Mutation {
	private:
		Random* random;
		struct radamsa_constants constants;
		int delta;
	public:
		TreeDeleteMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input);
		int nextDelta();
	};
}