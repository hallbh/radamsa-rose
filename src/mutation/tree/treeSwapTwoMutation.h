#pragma once
#include "../mutation.h"

namespace Radamsa {
	class TreeSwapTwoMutation : public Radamsa::Mutation {
	private:
		Random* random;
		struct radamsa_constants constants;
		int delta;
	public:
		TreeSwapTwoMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input);
		int nextDelta();
	};
}