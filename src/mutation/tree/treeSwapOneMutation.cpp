#include "treeSwapOneMutation.h"
#include "treeUtils.h"

Radamsa::TreeSwapOneMutation::TreeSwapOneMutation(int pri, struct radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "ts1")
{
	this->delta = -1; 
	this->random = &random;
	this->constants = constants;
};

std::string Radamsa::TreeSwapOneMutation::mutate(std::string input) {
	TreeUtils utils = TreeUtils();
	delta = -1;

	std::map<std::string, std::string> sublists = utils.sublists(input);
	
	if (sublists.size() > 1) {
		delta = 1;
		std::vector<std::string> keys = std::vector<std::string>();
		for (auto it = sublists.begin(); it != sublists.end(); it++) keys.push_back(it->first);

		std::vector<std::string> toSwap = random->reservoirSample(keys, 2);
		random->permute(toSwap, 0, toSwap.size());

		// replace a with b
		utils.flatten(toSwap[1], sublists); // Get rid of any x = x infinite recursion
		sublists[toSwap[0]] = toSwap[1];
	}

	utils.flatten(input, sublists);
	return input;
}

int Radamsa::TreeSwapOneMutation::nextDelta() {
	return delta;
}