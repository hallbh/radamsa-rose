#include "treeUtils.h"
#include <vector>

Radamsa::TreeUtils::TreeUtils() {
	usual_delims = {
		   {'(', ')'},
		   {'[',']'},
		   {'<', '>'},
		   {'{', '}'},
		   {'\"', '\"'},
		   {'\'', '\''} };
}

std::map<std::string, std::string> Radamsa::TreeUtils::sublists(std::string &input) {
	std::map<std::string, std::string> out;
	int substr = 0;

	// once you find an opening, look for the closing delimiter - but first, look for sublists nested inside this one
	// imagine the case of "((()))" - we don't want "((()" as the top level sublist. 
	// We want "((()))", "(())", and "()".
	// So if you work backwards, like a stack until you find the first opening, then go forwards it should work
	for (int i = input.length() - 1; i >= 0; i--) {
		// map.find returns map.end() if it fails to find the key
		if (Radamsa::TreeUtils::usual_delims.find(input[i]) != Radamsa::TreeUtils::usual_delims.end()) {
			char close = Radamsa::TreeUtils::usual_delims[input[i]];
			for (int j = i + 1; j < input.length(); j++) {
				if (input[j] == close) { // if we find the closing 
					std::string node = input.substr(i, (j + 1) - i);

					std::string keyNum = std::to_string(substr);
					for (int i = keyNum.length(); i < 4; i++) {
						keyNum = "0" + keyNum; // Hack to get the ordering in the keys of the map correct
					}
					std::string key = "#" + keyNum + "#";

					input.replace(i, node.length(), key);

					out[key] = node;
					substr++;
					break;
				}
			}
		}
	}

	return out;
}

// Expand all the garbage we parsed out.
// We don't know the depth of each sublist, so we have to replace in all the sublists too which sucks.
void Radamsa::TreeUtils::flatten(std::string& input, std::map<std::string, std::string> sublists) {
	for (std::map<std::string, std::string>::iterator it = sublists.begin(); it != sublists.end(); ++it) {
		// replace in sublists first, so we don't end up putting ### in the main string.
		// Can appear many times after mutations
		for (std::map<std::string, std::string>::iterator it2 = sublists.begin(); it2 != sublists.end(); ++it2) {
			while (it2->second.find(it->first) != std::string::npos)
				it2->second.replace(it2->second.find(it->first), it->first.length(), it->second);
		}
		if (input.find(it->first) != std::string::npos)
			input.replace(input.find(it->first), it->first.length(), it->second);
	}
}