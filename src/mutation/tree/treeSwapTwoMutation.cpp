#include "treeSwapTwoMutation.h"
#include "treeUtils.h"

Radamsa::TreeSwapTwoMutation::TreeSwapTwoMutation(int pri, struct radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "ts2")
{
	this->delta = -1;
	this->random = &random;
	this->constants = constants;
};

std::string Radamsa::TreeSwapTwoMutation::mutate(std::string input) {
	TreeUtils utils = TreeUtils();
	delta = -1;

	std::map<std::string, std::string> sublists = utils.sublists(input);

	if (sublists.size() > 1) {
		delta = 1;
		std::vector<std::string> keys = std::vector<std::string>();
		for (auto it = sublists.begin(); it != sublists.end(); it++) keys.push_back(it->first);

		std::vector<std::string> toSwap = random->reservoirSample(keys, 2);

		// swap a with b
		std::string flattened0 = sublists[toSwap[0]];
		std::string flattened1 = sublists[toSwap[1]];
		// Partially flatten now to avoid infinite loops later
		utils.flatten(flattened0, sublists);
		utils.flatten(flattened1, sublists);
		sublists[toSwap[0]] = flattened1;
		sublists[toSwap[1]] = flattened0;

		delta = 1;
	}

	utils.flatten(input, sublists);
	return input;
}

int Radamsa::TreeSwapTwoMutation::nextDelta() {
	return delta;
}