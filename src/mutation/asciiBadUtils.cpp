#include "asciiBadUtils.h"

Radamsa::AsciiNode::AsciiNode(std::string type, std::string value) {
	this->type = type;
	this->value = value;
}

bool Radamsa::AsciiNode::operator == (AsciiNode const& otherNode) {
	return (this->type == otherNode.type) && (this->value == otherNode.value);
}

Radamsa::AsciiBadUtils::AsciiBadUtils(radamsa_constants constants)
{
	this->constants = constants;
}

char Radamsa::AsciiBadUtils::delimiterOf(char c)
{
	if (c == '\'' || c == '\"') return c;
	return -1;
}

bool Radamsa::AsciiBadUtils::texty(char c)
{
	if (c < 9 || c > 126) return false;
	if (c > 31 || c == 9 || c == 10 || c == 13) return true;
	return false;
}

bool Radamsa::AsciiBadUtils::textyEnough(std::string input)
{
	for (int i = 0; i < constants.minTexty; i++) {
		if (i > input.size()) return true; // still mutate small strings
		if (!texty(input[i])) return false;
	}
	return true;
}

int Radamsa::AsciiBadUtils::stringyLength(std::vector<AsciiNode> chunks)
{
	int l = chunks.size();
	if (l == 0) return -1;
	if (chunks[0].type == "byte") return -1;

	return l;
}

void Radamsa::AsciiBadUtils::flush(std::string type, std::string bytes, std::vector<AsciiNode>& chunks)
{
	chunks.emplace(chunks.begin(), AsciiNode(type, bytes));
}

void Radamsa::AsciiBadUtils::step(std::string& input, std::string& rawr, std::vector<AsciiNode>& chunks)
{
	if (input.empty()) {
		if (!rawr.empty()) flush("byte", rawr, chunks);
		std::reverse(chunks.begin(), chunks.end()); 
		return;
	}
	else if (textyEnough(input)) {
		if (!rawr.empty()) flush("byte", rawr, chunks);
		std::string seenr = "";
		stepText(input, seenr, chunks);
		return;
	}
	else {
		char front = input[0];
		input = input.substr(1);
		rawr = front + rawr;
		step(input, rawr, chunks);
		return;
	}
}

void Radamsa::AsciiBadUtils::stepText(std::string& input, std::string& seenr, std::vector<AsciiNode>& chunks)
{
	if (input.empty()) {
		std::reverse(seenr.begin(), seenr.end());
		flush("text", seenr, chunks);
		std::reverse(chunks.begin(), chunks.end()); 
		return;
	}

	if (delimiterOf(input[0]) != -1) {
		char delim = delimiterOf(input[0]);

		input = input.substr(1, input.size());
		seenr = delim + seenr;
		std::string afterr = "";
		stepDelimited(input, delim, afterr, seenr, chunks);
		return;
	}
	else if (texty(input[0])) {
		seenr = input[0] + seenr;
		input = input.substr(1, input.size());
		stepText(input, seenr, chunks);
		return;
	}
	else {
		flush("text", seenr, chunks);
		std::string rawr = "";
		step(input, rawr, chunks);
		return;
	}
	
}

void Radamsa::AsciiBadUtils::stepDelimited(std::string& input, char delim, std::string afterr, std::string prevr, std::vector<AsciiNode>& chunks)
{
	if (input.empty()) {
		afterr = afterr + prevr;
		std::reverse(afterr.begin(), afterr.end());
		flush("text", afterr, chunks);
		std::reverse(chunks.begin(), chunks.end()); // return
		return;
	}
	char front = input[0];
	if (front == delim) { // finish this text chunk
		prevr = prevr.substr(1, prevr.size()); // drop the start symbol
		std::reverse(afterr.begin(), afterr.end());
		
		if (!prevr.empty()) {
			std::reverse(prevr.begin(), prevr.end());
			flush("text", prevr, chunks);
		}

		flush("delimited", delim + afterr + delim, chunks);
		std::string rawr = "";
		input = input.substr(1, input.size());
		step(input, rawr, chunks);
		return;
	}
	else if (front == '\\') { // skip the byte after quotation if it seems texty
		input = input.substr(1, input.size());

		if (input.empty()) {
			afterr = '\\' + afterr;
		}
		else {
			char second = input[0];
			if (texty(second)) {
				afterr = '\\' + afterr;
			}
			afterr = second + afterr;
			input = input.substr(1, input.size());
		}
	}
	else if (texty(front)) {
		input = input.substr(1, input.size());
		afterr = front + afterr;
	}
	else {
		flush("text", afterr + prevr, chunks);
		std::string rawr = "";
		step(input, rawr, chunks);
		return;
	}

	stepDelimited(input, delim, afterr, prevr, chunks);
}

std::vector<Radamsa::AsciiNode> Radamsa::AsciiBadUtils::stringLex(std::string input)
{
	std::vector<AsciiNode> chunks = std::vector<AsciiNode>();
	std::string rawr = "";

	step(input, rawr, chunks);

	return chunks;
}

std::string Radamsa::AsciiBadUtils::stringUnlex(std::vector<AsciiNode> chunks)
{
	std::string out;

	for (AsciiNode c : chunks) {
		out = out + c.value;
	}

	return out;
}
