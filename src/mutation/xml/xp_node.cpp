#include "xp_node.h"

unsigned long long nodeCount = 0;

Radamsa::XpNode::XpNode(Radamsa::NodeType type) : creationNumber(++nodeCount), type(type) {}
Radamsa::XpNode::~XpNode() {}

Radamsa::NodeType Radamsa::XpNode::getType() { return this->type; }

bool Radamsa::operator<(const XpNode& lhs, const XpNode& rhs) { return lhs.creationNumber < rhs.creationNumber; }
bool Radamsa::operator>(const XpNode& lhs, const XpNode& rhs) { return rhs < lhs; }
bool Radamsa::operator<=(const XpNode& lhs, const XpNode& rhs) { return !(lhs > rhs); }
bool Radamsa::operator>=(const XpNode& lhs, const XpNode& rhs) { return !(lhs < rhs); }
bool Radamsa::XpNode::operator==(XpNode& other) const { return this->creationNumber == other.creationNumber; }
bool Radamsa::operator==(const XpNode& lhs, const XpNode& rhs) { return const_cast<XpNode&>(lhs) == const_cast<XpNode&>(rhs); }
bool Radamsa::operator!=(const XpNode& lhs, const XpNode& rhs) { return !(const_cast<XpNode&>(lhs) == const_cast<XpNode&>(rhs)); }


Radamsa::ByteNode::ByteNode(std::string bytes) : bytes(bytes), XpNode(NodeType::Byte) {}

std::shared_ptr<Radamsa::XpNode> Radamsa::ByteNode::clone()
{
    auto out = std::make_shared<ByteNode>(this->bytes);
    out->creationNumber = this->creationNumber;
    return out;
}

std::string Radamsa::ByteNode::toString() { return bytes; }

std::string attrToString(std::map<std::string, std::string> attrs) {
    std::string toReturn;
    for (auto attr : attrs) {
        toReturn.append(" ").append(attr.first).append("=").append(attr.second);
    }
    return toReturn;
}

Radamsa::OpenNode::OpenNode(std::string name, std::map<std::string, std::string> attributes) : name(name), attributes(attributes), XpNode(NodeType::Open) {}

std::shared_ptr<Radamsa::XpNode> Radamsa::OpenNode::clone()
{
    auto out = std::make_shared<OpenNode>(this->name, this->attributes);
    out->creationNumber = this->creationNumber;
    return out;
}

std::string Radamsa::OpenNode::toString() { 
    return "<" + name + attrToString(this->attributes) + ">";
}

Radamsa::OpenSingleNode::OpenSingleNode(std::string name, std::map<std::string, std::string> attributes) : name(name), attributes(attributes), XpNode(NodeType::OpenSingle) {}

std::shared_ptr<Radamsa::XpNode> Radamsa::OpenSingleNode::clone()
{
    auto out = std::make_shared<OpenSingleNode>(this->name, this->attributes);
    out->creationNumber = this->creationNumber;
    return out;
}

std::string Radamsa::OpenSingleNode::toString() { 
    return "<" + name + attrToString(this->attributes) + " />";
}

Radamsa::CloseNode::CloseNode(std::string name) : name(name), XpNode(NodeType::Close) {}

std::shared_ptr<Radamsa::XpNode> Radamsa::CloseNode::clone()
{
    auto out = std::make_shared<CloseNode>(this->name);
    out->creationNumber = this->creationNumber;
    return out;
}

std::string Radamsa::CloseNode::toString() { return "</" + name + ">"; }

Radamsa::TagNode::TagNode(std::string name, std::map<std::string, std::string> attributes, std::vector<std::shared_ptr<XpNode>> xp) : name(name), attributes(attributes), xp(xp), XpNode(NodeType::Tag) {}

std::shared_ptr<Radamsa::XpNode> Radamsa::TagNode::clone()
{
    std::vector<std::shared_ptr<XpNode>> newXp = std::vector<std::shared_ptr<XpNode>>();
    for (auto x : this->xp) {
        newXp.push_back(x->clone());
    }

    auto out = std::make_shared<TagNode>(this->name, this->attributes, newXp);

    out->creationNumber = this->creationNumber;
    return out;
}

std::string Radamsa::TagNode::toString() {
    std::string toReturn = "<" + name + attrToString(this->attributes) + ">";
    for (auto x : this->xp) {
        toReturn.append(x->toString());
    }
    return toReturn.append("</" + name + ">");
}

bool Radamsa::TagNode::operator==(XpNode& other) const { 
    if (XpNode::operator==(other)) {
        TagNode* otherTag = (TagNode*) &other;
        if (otherTag->xp.size() == this->xp.size()) {
            bool innerEqual = true;
            for (int i = 0; i < this->xp.size(); ++i) {
                innerEqual &= (*(this->xp[i]) == *(otherTag->xp[i]));
                if (!innerEqual) break;
            }
            return innerEqual;
        }
    }
    return false;
}

Radamsa::PlusNode::PlusNode(std::vector<std::shared_ptr<XpNode>> xp1, std::vector<std::shared_ptr<XpNode>> xp2) : xp1(xp1), xp2(xp2), XpNode(NodeType::Plus) {}

Radamsa::PlusNode::PlusNode(std::shared_ptr<XpNode> x1, std::shared_ptr<XpNode> x2) : XpNode(NodeType::Plus) {
    this->xp1.push_back(x1);
    this->xp2.push_back(x2);
}

std::string Radamsa::PlusNode::toString() {
    std::string toReturn;
    for (auto x : xp1) {
        toReturn.append(x->toString());
    }
    for (auto x : xp2) {
        toReturn.append(x->toString());
    }

    return toReturn;
}

std::shared_ptr<Radamsa::XpNode> Radamsa::PlusNode::clone() {
    std::vector<std::shared_ptr<XpNode>> newXp1 = std::vector<std::shared_ptr<XpNode>>();
    for (auto x : this->xp1) {
        newXp1.push_back(x->clone());
    }
    std::vector<std::shared_ptr<XpNode>> newXp2 = std::vector<std::shared_ptr<XpNode>>();
    for (auto x : this->xp2) {
        newXp2.push_back(x->clone());
    }

    auto out = std::make_shared<PlusNode>(newXp1, newXp2);

    out->creationNumber = this->creationNumber;
    return out;
}

bool Radamsa::PlusNode::operator==(XpNode& other) const { 
    if (XpNode::operator==(other)) {
        PlusNode* otherTag = (PlusNode*) &other;
        if (otherTag->xp1.size() == this->xp1.size() && otherTag->xp2.size() == this->xp2.size()) {
            bool innerEqual = true;
            for (int i = 0; i < this->xp1.size(); ++i) {
                innerEqual &= (*(this->xp1[i]) == *(otherTag->xp1[i]));
                if (!innerEqual) break;
            }

            for (int i = 0; i < this->xp2.size(); ++i) {
                innerEqual &= (*(this->xp2[i]) == *(otherTag->xp2[i]));
                if (!innerEqual) break;
            }
            return innerEqual;
        }
    }
    return false;
}