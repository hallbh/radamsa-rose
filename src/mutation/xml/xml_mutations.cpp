#include "xml_mutations.h"
#include <algorithm>
#include <stdexcept>

using namespace Radamsa;

void Radamsa::xml_mutate(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants) {
    XpUtils utils;
    std::string original = utils.nodesToString(nodes);
    do {
        auto mutation = selectMutation(rand);
        mutation(rand, tags, nodes, constants);
    } while (original == utils.nodesToString(nodes));   // Keep trying random mutations until something works.
}

std::function<void(Random&, std::vector<Tag>&, std::vector<std::shared_ptr<XpNode>>&, const radamsa_constants)> Radamsa::selectMutation(Random& rand) {
    int selected = rand.nextInteger(6);
    switch(selected) {
        case 0:
            return xp_swap;
        case 1:
            return xp_dup;
        case 2:
            return xp_pump;
        case 3:
            return xp_repeat;
        default:    // Xp insert is weighted higher
            return xp_insert;
    }
}

std::vector<std::vector<std::shared_ptr<XpNode>>::iterator> findNodes(std::vector<std::shared_ptr<XpNode>>& nodes, std::function<bool(Radamsa::NodeType)> pred) {
    std::vector<std::vector<std::shared_ptr<XpNode>>::iterator> positions;
    for (auto it = nodes.begin(); it != nodes.end(); ++it) {
        Radamsa::NodeType type = (*it)->getType();
        if (pred(type))
            positions.insert(positions.begin(), it);

        // Deep search tag and plus nodes
        if (type == Radamsa::NodeType::Plus) {
            PlusNode* pnode = (PlusNode*)(*it).get();
            auto x1Positions = findNodes(pnode->xp1, pred);
            auto x2Positions = findNodes(pnode->xp2, pred);
            positions.insert(positions.begin(), x1Positions.begin(), x1Positions.end());
            positions.insert(positions.begin(), x2Positions.begin(), x2Positions.end());
        } else if (type == Radamsa::NodeType::Tag) {
            TagNode* tnode = (TagNode*)(*it).get();
            auto xPositions = findNodes(tnode->xp, pred);
            positions.insert(positions.begin(), xPositions.begin(), xPositions.end());
        }
    }
    return positions;
}

bool isTagged(Radamsa::NodeType type) {
    return type == Radamsa::NodeType::Tag;
}

bool isByteTaggedNode(Radamsa::NodeType type) {
    return isTagged(type) || type == Radamsa::NodeType::Byte;
}

bool isOpenTaggedNode(Radamsa::NodeType type) {
    return type == Radamsa::NodeType::Open || type == Radamsa::NodeType::OpenSingle || isTagged(type);
}

bool anyTag(Radamsa::NodeType type) {
    return true;
}

void Radamsa::xp_swap(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants) {
    auto positions = findNodes(nodes, isOpenTaggedNode);
    if (positions.size() > 1) {
        rand.permute(positions, 0, positions.size());
        std::iter_swap(positions.front(), positions[1]);
    }
}

void Radamsa::xp_dup(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants) {
    auto positions = findNodes(nodes, isOpenTaggedNode);
    if (!positions.empty()) {
        auto pos = positions[rand.nextInteger(positions.size())];
        auto left = (*pos)->clone();
        auto right = (*pos)->clone();
        (*pos) = std::make_shared<PlusNode>(left, right);
    }
}

unsigned int determineNumberOfPumps(Random& rand, unsigned int maxPumps) {
    uint32_t finished = rand.generateNext();
    int max = 2;
    while ((finished & 1) != 0 && max < maxPumps) {
        max = max << 1;
        finished = rand.generateNext();
    }
    return rand.nextInteger(max) + 2;
}

void Radamsa::xp_repeat(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants) {
    auto positions = findNodes(nodes, isOpenTaggedNode);
    if (!positions.empty()) {
        auto pos = positions[rand.nextInteger(positions.size())];
        unsigned int numberOfPumps = determineNumberOfPumps(rand, constants.maxPumpLimit);
        auto left = (*pos)->clone();
        auto right = (*pos)->clone();
        std::shared_ptr<XpNode> plus = std::make_shared<PlusNode>(left, right); 
        while (--numberOfPumps > 0) {
            auto oldPlus = plus->clone();
            plus = std::make_shared<PlusNode>((*pos), oldPlus);
        }
        (*pos) = plus;
    }
}

TagNode* pickSomeTaggedNode(Random& rand, std::vector<std::vector<std::shared_ptr<XpNode>>::iterator> taggedNodes) {
    for (auto it : taggedNodes) {
        if (!(rand.generateNext() & 1)) {
            return (TagNode*)(*it).get();
        }
    }
    return (TagNode*)(*taggedNodes.back()).get();
}

bool comparePositionValues(std::vector<std::shared_ptr<XpNode>>::iterator posOne, std::vector<std::shared_ptr<XpNode>>::iterator posTwo) {
    return (*(*posTwo)) < (*(*posOne));
}

bool findAndReplace(std::vector<std::shared_ptr<XpNode>> &innerXp, std::shared_ptr<XpNode> pathEnd, std::shared_ptr<XpNode> replacement) {
    for (int i = 0; i < innerXp.size(); i++) {
        auto x = innerXp[i];
        // If the nodes themselves are equal, replace and return true;
        if (*x == *pathEnd) {
            innerXp[i] = replacement->clone();
            return true;
        }
        // Go down inner xml on nodes that have that
        if (x->getType() == NodeType::Tag) {
            auto maybeFound = findAndReplace(((TagNode*)x.get())->xp, pathEnd, replacement);
            if (maybeFound) return maybeFound;
        }
        else if (x->getType() == NodeType::Plus) {
            auto maybeFound1 = findAndReplace(((PlusNode*)x.get())->xp1, pathEnd, replacement);
            if (maybeFound1) return maybeFound1;
            auto maybeFound2 = findAndReplace(((PlusNode*)x.get())->xp2, pathEnd, replacement);
            if (maybeFound2) return maybeFound2;
        }
    }

    return false;
}

void Radamsa::xp_pump(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants) {
    auto utils = XpUtils();
    auto positions = findNodes(nodes, isTagged);
    std::sort(positions.begin(), positions.end(), comparePositionValues);
    if (!positions.empty()) {
        TagNode* start = pickSomeTaggedNode(rand, positions);
        std::shared_ptr<XpNode> pumpCopy = start->clone();
        auto targetPositions = findNodes(start->xp, isByteTaggedNode);
        if (!targetPositions.empty()) {
            int pumpIndex = rand.nextInteger(targetPositions.size());
            std::shared_ptr<XpNode> pathEnd = (*targetPositions[pumpIndex])->clone();
            unsigned int numberOfPumps = determineNumberOfPumps(rand, constants.maxPumpLimit);

            while (numberOfPumps-- > 0) {
                // Locate and replace the end
                bool actualEnd = findAndReplace(start->xp, pathEnd, pumpCopy);
                if (!actualEnd) throw std::logic_error("Child node not contained in parent in xp-pump");
            }
        }
    }
}

std::shared_ptr<XpNode> generateNewNode(Random& rand, std::vector<Tag> tags, std::shared_ptr<XpNode> node) {
    Tag newTag = tags[rand.nextInteger(tags.size())];

    // Select Attributes to be copied
    std::map<std::string, std::string> attr;
    for (auto it = newTag.attributes.begin(); it != newTag.attributes.end(); ++it) {
        if (rand.nextInteger(3) == 0)
            attr.insert(*it);
    }
    if (newTag.type == "open-only") {
        std::shared_ptr<XpNode> open = std::make_shared<OpenNode>(newTag.name, attr);
        std::shared_ptr<XpNode> out = std::make_shared<PlusNode>(open, node);
        return out;
    } else if (newTag.type == "open-single") {
        std::shared_ptr<XpNode> open = std::make_shared<OpenSingleNode>(newTag.name, attr);
        std::shared_ptr<XpNode> out = std::make_shared<PlusNode>(open, node);
        return out;
    } else if (newTag.type == "open-full") {
        std::shared_ptr<XpNode> out = std::make_shared<TagNode>(newTag.name, attr, std::vector<std::shared_ptr<XpNode>>(1, node));
        return out;
    }

    throw std::runtime_error("Found an unexpected node type while generating a new node");
}

void Radamsa::xp_insert(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants) {
    auto positions = findNodes(nodes, isByteTaggedNode);
    if (positions.empty())
        positions = findNodes(nodes, anyTag);
    int pos_index = rand.nextInteger(positions.size());
    std::shared_ptr<XpNode> target = (*positions[pos_index]);
    (*positions[pos_index]) = generateNewNode(rand, tags, target);
}