#pragma once
#include "xp_node.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <map>

namespace Radamsa {
	class Tag {
	public:
		std::string name;
		std::string type;
		std::map<std::string, std::string> attributes;

		Tag(std::string name, std::string type, std::map<std::string, std::string> attributes) {
			this->name = name;
			this->type = type;
			this->attributes = attributes;
		}

		bool equals(Tag other) {
			bool out = true;
			out = out && this->name == other.name;
			out = out && this->attributes == other.attributes;
			return out;
		}
	};

	class XpUtils {
	public: // public for testing ofc
		std::string extractName(std::string tag);
		std::map<std::string, std::string> extractAttributes(std::string tag);
		std::shared_ptr<XpNode> constructTagNode(std::vector<std::string> matches, int openIndex, int closeIndex, std::string& modifiable);

		XpUtils();
		
		void XpProcess(std::string input, std::vector<std::shared_ptr<XpNode>>& nodes, std::vector<Tag>& tags);
		bool isTaggy(std::vector<std::shared_ptr<XpNode>> nodes);
		std::string nodesToString(std::vector<std::shared_ptr<XpNode>> nodes);
	};
}