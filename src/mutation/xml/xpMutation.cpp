#include "xpMutation.h"
#include "xpUtils.h"
#include "xml_mutations.h"
#include <algorithm>

Radamsa::XpMutation::XpMutation(int pri, struct radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "xp")
{
	this->delta = 0;
	this->random = &random;
	this->constants = constants;
};

std::string Radamsa::XpMutation::mutate(std::string input) {
	std::vector<Tag> tags;
	std::vector<std::shared_ptr<XpNode>> nodes;

	XpUtils utils = XpUtils();
	utils.XpProcess(input, nodes, tags);
	if (utils.isTaggy(nodes)) {
		xml_mutate(*(this->random), tags, nodes, constants);
		std::string out = utils.nodesToString(nodes);
		delta = 1;
		return out;
	}
	else {
		delta = -1;
		return input;
	}
}

int Radamsa::XpMutation::nextDelta() {
	return delta;
}