#include "line_mutation.h"
#include <sstream>

/// AbstractLineMutation Constructer
///
/// @param priotity an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer
/// @param name name of the line mutation


Radamsa::AbstractLineMutation::AbstractLineMutation(int priotity, const radamsa_constants constants, Random& random, std::string name) : Mutation(constants.maxScore, priotity, name) {
	this->random = &random;
	this->constants = constants;
	this->canMutate = false;
}

/// This mutation first splits the input into lines.
/// If input is empty or first item of lines is binarish, 
/// it will not mutate and return the input.
/// Otherwise, it will perform mutation. 
/// It will perform the line operation and then return
/// the mergeline result.
///
/// @param input string


std::string Radamsa::AbstractLineMutation::mutate(std::string input) {
	auto lines = this->splitLines(input);
	if (input.empty() || isBinarish(lines[0])) {
		this->canMutate = false;
		return input;
	}
	this->canMutate = true;
	this->performOperation(lines);
	return this->mergeLines(lines);
}

// These delta's possibly constants?
int Radamsa::AbstractLineMutation::nextDelta() {
	if (this->canMutate) {
		this->canMutate = false;
		return 1;
	}

	return -1;
}

/// This function takes string input,
/// and then splits it into vector of string
///
/// @param input std:string


std::vector<std::string> Radamsa::AbstractLineMutation::splitLines(std::string input) {
	std::vector<std::string> lines;
	std::string line;

	for (auto iterator = input.cbegin(); iterator != input.cend(); ++iterator) {
		const char c = *iterator;
		line.push_back(c);
		if (c == '\n') {
			lines.push_back(line);
			line.clear();
		}
	}
	if (!line.empty()) {
		lines.push_back(line);
	}
	return lines;
}

/// a function to define if a string is binarish
///
/// @param line string input


bool Radamsa::AbstractLineMutation::isBinarish(std::string line) {
	// length of 8 may be a constant
	for (int i = 0; i < line.size() && i < 8; i++) {
		const char c = line.at(i);
		if (isNullByte(c) || hasHighBit(c)) {
			return true;
		}
	}

	return false;
}

/// This function merges line 
///
/// @param lines vector of string


std::string Radamsa::AbstractLineMutation::mergeLines(std::vector<std::string> lines) {
	std::string str;
	for (auto it = lines.cbegin(); it != lines.cend(); ++it) {
		str.append(*it);
	}
	return str;
}

/// return if the input char is null byte
///
/// @param c const chat


bool Radamsa::AbstractLineMutation::isNullByte(const char c) {
	return c == '\x00';
}

/// return if the input char has high bit
///
/// @param c const chat


bool Radamsa::AbstractLineMutation::hasHighBit(const char c) {
	return (c & 128) != 0;
}

Radamsa::StatelessLineMutation::StatelessLineMutation(int priotity, const radamsa_constants constants, Random& random, std::function<void(std::vector<std::string>&, Random*)> operation, std::string name) : AbstractLineMutation(priotity, constants, random, name) {
	this->operation = operation;
}

void Radamsa::StatelessLineMutation::performOperation(std::vector<std::string>& lines) {
	this->operation(lines, this->random);
}

Radamsa::StatefulLineMutation::StatefulLineMutation(int priotity, const radamsa_constants constants, Random& random, std::function<void(std::vector<std::string>&, LineState&, Random*)> operation, std::string name) : 
	AbstractLineMutation(priotity, constants, random, name), state(constants.storedElements, constants.updateProbability, random) {
	this->operation = operation;
}

void Radamsa::StatefulLineMutation::performOperation(std::vector<std::string>& lines) {
	this->operation(lines, this->state, this->random);
}
