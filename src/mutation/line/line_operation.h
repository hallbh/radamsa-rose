#pragma once
#include "radamsa-rose/constants.h"
#include "random/random.h"
#include "line_state.h"
#include <string>

namespace Radamsa {
	void deleteLineOperation(std::vector<std::string>& lines, Random* rand);
	void deleteLineSequenceOperation(std::vector<std::string>& lines, Random* rand);
	void duplicateLineOperation(std::vector<std::string>& lines, Random* rand);
	void cloneLineOperation(std::vector<std::string>& lines, Random* rand);
	void repeatLineOperation(std::vector<std::string>& lines, Random* rand);
	void swapLineOperation(std::vector<std::string>& lines, Random* rand);
	void permuteLineOperation(std::vector<std::string>& lines, Random* rand);
	void insertLineOperation(std::vector<std::string>& lines, LineState& state, Random* rand);
	void replaceLineOperation(std::vector<std::string>& lines, LineState& state, Random* rand);
}