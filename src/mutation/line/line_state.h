#pragma once
#include "random/random.h"
#include <vector>
#include <string>

namespace Radamsa {
	class LineState {
	public:
		LineState(const unsigned int minimumElements, const int updateProbability, Random& rand);
		void step(const std::vector<std::string> lines);
		std::string pickLine();
	private:
		std::vector<std::string> elements;
		unsigned int minElem;
		Random* rand;
		int updateProbability;

		void addElement(const std::vector<std::string> lines);
		void updateElement(const std::vector<std::string> lines);
	};
}