#include "line_state.h"

/// LineState Constructer
///
/// @param minimumElements const unsigned int
/// @param updateProbability int
/// @param rand random object


Radamsa::LineState::LineState(const unsigned int minimumElements, const int updateProbability, Random& rand) {
	this->rand = &rand;
	this->minElem = minimumElements;
	this->updateProbability = updateProbability;
}

void Radamsa::LineState::step(const std::vector<std::string> lines) {
	while (this->elements.size() < this->minElem) {
		addElement(lines);
	}
	updateElement(lines);
}

std::string Radamsa::LineState::pickLine() {
	return this->elements.at(this->rand->nextInteger(this->elements.size()));
}

void Radamsa::LineState::addElement(const std::vector<std::string> lines) {
	int index = this->rand->nextInteger(lines.size());
	this->elements.insert(this->elements.begin(), lines.at(index));
}

void Radamsa::LineState::updateElement(const std::vector<std::string> lines) {
	int updateIndex = this->rand->nextInteger(this->updateProbability);
	if (updateIndex < this->elements.size()) {
		this->elements[updateIndex] = lines.at(this->rand->nextInteger(lines.size()));
	}
}
