#include "radamsa_algorithm.h"
#include <algorithm>

#include "generator/muxGenerator.h"
#include "pattern/muxPattern.h"
#include "mutation/muxFuzzer.h"

#include "mutation/line/line_mutation.h"
#include "mutation/line/line_operation.h"
#include "mutation/byte/byteDropMutation.h"
#include "mutation/byte/byteFlipMutation.h"
#include "mutation/byte/byteRepeatMutation.h"
#include "mutation/byte/byteInsertMutation.h"
#include "mutation/byte/bytePermuteMutation.h"
#include "mutation/byte/byteIncreaseMutation.h"
#include "mutation/byte/byteDecreaseMutation.h"
#include "mutation/byte/byteRandomMutation.h"

#include "mutation/sequenceDeleteMutation.h"
#include "mutation/sequenceRepeatMutation.h"

#include "mutation/tree/treeDeleteMutation.h"
#include "mutation/tree/treeDuplicateMutation.h"
#include "mutation/tree/treeStutterMutation.h"
#include "mutation/tree/treeSwapOneMutation.h"
#include "mutation/tree/treeSwapTwoMutation.h"

#include "mutation/numMutation.h"
#include "mutation/asciiBadMutation.h"
#include "mutation/utf8InsertMutation.h"
#include "mutation/utf8WidenMutation.h"
#include "mutation/fuse/fuse_this_mutation.h"
#include "mutation/fuse/fuse_next_mutation.h"
#include "mutation/fuse/fuse_old_mutation.h"
#include "mutation/xml/xpMutation.h"

#include "mutation/mutation_adapter.h"
#include "pattern/pattern_adapter.h"
#include "generator/generator_adapter.h"

#include "pattern/pat_once.h"
#include "pattern/pat_many.h"
#include "pattern/pat_burst.h"

#include "generator/stdingenerator.h"
#include "generator/randomgenerator.h"
#include "generator/filegenerator.h"
#include "generator/jumpgenerator.h"

Radamsa::RadamsaAlgorithm::RadamsaAlgorithm(int seed, const struct radamsa_constants constants) : random(seed) {
	this->constants = constants;
	this->muxMutaCalled = false;

	this->addAllMutations();
	this->addAllPatterns();
	this->addAllGenerators();
}

void Radamsa::RadamsaAlgorithm::addAllMutations() {
	// Order of mutations in default-mutations in mutations.scm
	this->mutationMap.insert<Radamsa::FuseThisMutation>(2, this->constants, this->random);
	this->mutationMap.insert<Radamsa::FuseOldMutation>(2, this->constants, this->random);
	this->mutationMap.insert<Radamsa::FuseNextMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::NumMutation>(5, this->constants, this->random);
	this->mutationMap.insert<Radamsa::TreeDeleteMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::TreeDuplicateMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::TreeSwapOneMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::TreeStutterMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::TreeSwapTwoMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::StatelessLineMutation>(1, this->constants, this->random, Radamsa::deleteLineOperation, "ld");
	this->mutationMap.insert<Radamsa::StatelessLineMutation>(1, this->constants, this->random, Radamsa::deleteLineSequenceOperation, "lds");
	this->mutationMap.insert<Radamsa::StatelessLineMutation>(1, this->constants, this->random, Radamsa::duplicateLineOperation, "lr2");
	this->mutationMap.insert<Radamsa::StatelessLineMutation>(1, this->constants, this->random, Radamsa::cloneLineOperation, "li");
	this->mutationMap.insert<Radamsa::StatelessLineMutation>(1, this->constants, this->random, Radamsa::swapLineOperation, "ls");
	this->mutationMap.insert<Radamsa::StatelessLineMutation>(1, this->constants, this->random, Radamsa::permuteLineOperation, "lp");
	this->mutationMap.insert<Radamsa::StatelessLineMutation>(1, this->constants, this->random, Radamsa::repeatLineOperation, "lr");
	this->mutationMap.insert<Radamsa::StatefulLineMutation>(1, this->constants, this->random, Radamsa::insertLineOperation, "lis");
	this->mutationMap.insert<Radamsa::StatefulLineMutation>(1, this->constants, this->random, Radamsa::replaceLineOperation, "lrs");
	this->mutationMap.insert<Radamsa::SequenceRepeatMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::SequenceDeleteMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::ByteDropMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::ByteFlipMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::ByteInsertMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::ByteRepeatMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::BytePermuteMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::ByteIncreaseMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::ByteDecreaseMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::ByteRandomMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::utf8WidenMutation>(1, this->constants, this->random);
	this->mutationMap.insert<Radamsa::utf8InsertMutation>(2, this->constants, this->random);
	this->mutationMap.insert<Radamsa::XpMutation>(9, this->constants, this->random);
	this->mutationMap.insert<Radamsa::AsciiBadMutation>(1, this->constants, this->random);
}

void Radamsa::RadamsaAlgorithm::addAllPatterns() {
	this->patternMap.insert<Radamsa::MutateOncePattern>(1, this->constants, this->random);
	this->patternMap.insert<Radamsa::ManyMutationsPattern>(2, this->constants, this->random);
	this->patternMap.insert<Radamsa::BurstMutationsPattern>(1, this->constants, this->random);
}

void Radamsa::RadamsaAlgorithm::addAllGenerators() {
	this->generatorMap.insert<Radamsa::RandomGenerator>(1, this->constants, this->random);
	this->generatorMap.insert<Radamsa::FileGenerator>(1000, this->constants, this->random);
	this->generatorMap.insert<Radamsa::JumpGenerator>(200, this->constants, this->random);
	this->generatorMap.insert<Radamsa::StdinGenerator>(100000, this->constants, this->random);
}

void Radamsa::RadamsaAlgorithm::setAllMutationsEnabled(bool enabled) {
	this->mutationMap.setAllEnabled(enabled);
}

void Radamsa::RadamsaAlgorithm::setMutationEnabled(std::string name, bool enabled) {
	this->mutationMap.setEnabled(name, enabled);
}

bool Radamsa::RadamsaAlgorithm::isMutationEnabled(std::string name) {
	return this->mutationMap.isEnabled(name);
}

void Radamsa::RadamsaAlgorithm::setMutationPriority(std::string name, int pri) {
	this->mutationMap.setPriorityOf(name, pri);
}

void Radamsa::RadamsaAlgorithm::setAllPatternsEnabled(bool enabled) {
	this->patternMap.setAllEnabled(enabled);
}

void Radamsa::RadamsaAlgorithm::setPatternEnabled(std::string name, bool enabled) {
	this->patternMap.setEnabled(name, enabled);
}

bool Radamsa::RadamsaAlgorithm::isPatternEnabled(std::string name) {
	return this->patternMap.isEnabled(name);
}

void Radamsa::RadamsaAlgorithm::setPatternPriority(std::string name, int pri) {
	return this->patternMap.setPriorityOf(name, pri);
}

void Radamsa::RadamsaAlgorithm::setGeneratorEnabled(std::string name, bool enabled) {
	this->generatorMap.setEnabled(name, enabled);
}

void Radamsa::RadamsaAlgorithm::setAllGeneratorsEnabled(bool enabled) {
	this->generatorMap.setAllEnabled(enabled);
}

bool Radamsa::RadamsaAlgorithm::isGeneratorEnabled(std::string name) {
	return this->generatorMap.isEnabled(name);
}

void Radamsa::RadamsaAlgorithm::setGeneratorPriority(std::string name, int pri) {
	return this->generatorMap.setPriorityOf(name, pri);
}

std::unique_ptr<Radamsa::Generator> Radamsa::RadamsaAlgorithm::muxAndGetGenerators() {
	auto muxGenerator = std::make_unique<Radamsa::muxGenerator>(0, this->constants, this->random);
	auto genList = this->generatorMap.getOrderedList();
	for (auto it = genList.begin(); it != genList.end(); it++) {
		muxGenerator->addGenerator(*it);
	}
	return muxGenerator;
}

std::unique_ptr<Radamsa::Pattern> Radamsa::RadamsaAlgorithm::muxAndGetPatterns() {
	auto muxPattern = std::make_unique<Radamsa::muxPattern>(0, this->constants, this->random);
	auto patList = this->patternMap.getOrderedList();
	for (auto it = patList.begin(); it != patList.end(); it++) {
		muxPattern->addPattern(*it);
	}
	return muxPattern;

}

std::unique_ptr<Radamsa::Mutation> Radamsa::RadamsaAlgorithm::muxAndGetMutations() {
	auto muxFuzzer = std::make_unique<Radamsa::muxFuzzer>(0, this->constants, this->random);
	auto mutaList = this->mutationMap.getOrderedList();
	for (auto it = mutaList.begin(); it != mutaList.end(); it++) {
			Radamsa::Mutation* muta = *it;
			if (!muxMutaCalled) {
				muta->score = std::max(2, (int)this->random.nextInteger(this->constants.maxScore));
			muxFuzzer->addMutation(muta);
		}
	}
	muxMutaCalled = true;
	return muxFuzzer;
}

uint32_t Radamsa::RadamsaAlgorithm::peekRandomState() {
	return this->random.peekState();
}

void Radamsa::RadamsaAlgorithm::addCustomMutation(int (*mutate)(struct input_chunk*, struct radamsa_constants, radamsa_random), std::string name, int priority) {
	this->mutationMap.insert<Radamsa::MutationAdapter>(mutate, name, priority, this->constants, this->random);
}

void Radamsa::RadamsaAlgorithm::addCustomPattern(struct input_chunk* (*runFunc)(struct input_chunk*, radamsa_mutation, struct radamsa_constants, radamsa_random), std::string name, int priority) {
	this->patternMap.insert<Radamsa::PatternAdapter>(runFunc, priority, this->constants, this->random, name);
}

void Radamsa::RadamsaAlgorithm::addCustomGenerator(struct input_chunk* (*genFunc)(const char*, size_t size, struct radamsa_constants, radamsa_random), std::string name, int priority) {
	this->generatorMap.insert<Radamsa::GeneratorAdapter>(genFunc, name, priority, this->constants, this->random);
}

