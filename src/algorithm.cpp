#include "algorithm.h"
#include <stdexcept>

std::string Radamsa::runAlgorithm(std::string input, Radamsa::AlgorithmParameters params) {
	if (params.generator == NULL) {
		throw std::logic_error("Generator pointer is null");
	} else if (params.pattern == NULL) {
		throw std::logic_error("Generator pointer is null");
	}

	std::deque<std::string> inputChunks = params.generator->generate(input);
	std::deque<std::string> outputChunks = params.pattern->run(std::move(inputChunks), params.mutation);
	std::string output;
	while (!outputChunks.empty()) {
		output.append(outputChunks.front());
		outputChunks.pop_front();
	}
	return output;
}
