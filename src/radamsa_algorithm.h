#pragma once
#include "radamsa-rose/constants.h"
#include "radamsa-rose/radamsa_definitions.h"
#include "mutation/mutation.h"
#include "pattern/pattern.h"
#include "generator/generator.h"
#include "random/random.h"
#include "enable_ordered_map.h"
#include <string>
#include <unordered_map>
#include <vector>
#include <memory>
#include <stdexcept>

namespace Radamsa {
	class RadamsaAlgorithm {
	public:
		RadamsaAlgorithm(int seed, const struct radamsa_constants constants);

		// TODO change priorities for all of them

		void setMutationEnabled(std::string name, bool enabled);
		void setAllMutationsEnabled(bool enabled);
		bool isMutationEnabled(std::string name);
		void setMutationPriority(std::string name, int pri);

		void setPatternEnabled(std::string name, bool enabled);
		void setAllPatternsEnabled(bool enabled);
		bool isPatternEnabled(std::string name);
		void setPatternPriority(std::string name, int pri);

		void setGeneratorEnabled(std::string name, bool enabled);
		void setAllGeneratorsEnabled(bool enabled);
		bool isGeneratorEnabled(std::string name);
		void setGeneratorPriority(std::string name, int pri);

		std::unique_ptr<Radamsa::Generator> muxAndGetGenerators();
		std::unique_ptr<Radamsa::Pattern> muxAndGetPatterns();
		std::unique_ptr<Radamsa::Mutation> muxAndGetMutations();

		void addCustomMutation(int (*mutate)(struct input_chunk*, struct radamsa_constants, radamsa_random), std::string name, int priority);
		void addCustomPattern(struct input_chunk* (*runFunc)(struct input_chunk*, radamsa_mutation, struct radamsa_constants, radamsa_random), std::string name, int priority);
		void addCustomGenerator(struct input_chunk* (*genFunc)(const char*, size_t size, struct radamsa_constants, radamsa_random), std::string name, int priority);

		uint32_t peekRandomState();

	private:
		EnableOrderedMap<Mutation> mutationMap;
		EnableOrderedMap<Pattern> patternMap;
		EnableOrderedMap<Generator> generatorMap;
		struct radamsa_constants constants;
		Random random;

		bool muxMutaCalled;

		void addAllMutations();
		void addAllPatterns();
		void addAllGenerators();
	};
}
