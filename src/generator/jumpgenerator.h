#pragma once
#include "generator.h"
#include <list>

namespace Radamsa {
	class JumpGenerator : public Generator {
	private:
		struct radamsa_constants constants;
		Random* random;

		void checkForErrorInConstants();
		std::vector<std::string> splitPaths(std::string input);
		std::deque<std::string> readFromFile(std::ifstream& file);
		std::deque<std::string> jumpSomewhere(Radamsa::Random&, std::deque<std::string> chunkA, std::deque<std::string> chunkB);
		std::deque<std::string> walkToJump(Radamsa::Random&, int ip, std::string strA, std::deque<std::string> chunkA, std::string strB, std::deque<std::string> chunkB);
		std::deque<std::string> finish(Radamsa::Random, std::string strA, std::deque<std::string> chunkA, std::string strB, std::deque<std::string> chunkB);

	public:
		JumpGenerator(int priority, struct radamsa_constants constants, Random& random);
		std::deque<std::string> generate(std::string) override;
	};
}