#include "randomgenerator.h"
#include <stdexcept>

Radamsa::RandomGenerator::RandomGenerator(int priority, const struct radamsa_constants constants, Random& random) : Generator(priority, "random") {
    this->constants = constants;
    this->random = &random;
}

Radamsa::Random Radamsa::RandomGenerator::createRandomFromRandom() {
	Radamsa::Bignum seed = this->random->nextInteger(Radamsa::Bignum("4722366482869645213696"));
	return Radamsa::Random(seed);
}

std::deque<std::string> Radamsa::RandomGenerator::generate(std::string input) {
	this->checkForErrorInConstants();
	Radamsa::Random privateRandom = this->createRandomFromRandom();
    std::deque<std::string> outstream;
    int newBlockProbability = 0;
    
    // Generator random number of blocks
    do {
        outstream.push_back(this->nextRandomBlock(privateRandom));
        newBlockProbability = privateRandom.nextIntegerRange(1, 100);
    } while (privateRandom.nextInteger(newBlockProbability));

    return outstream;
}

void Radamsa::RandomGenerator::checkForErrorInConstants() {
	if (this->constants.maxBlockSize <= 0) {
		throw std::logic_error("Maximum block size cannot be negative or zero");
	}
}

std::string Radamsa::RandomGenerator::nextRandomBlock(Random& rand) {
    int blockSize = rand.nextIntegerRange(32, this->constants.maxBlockSize);
    std::string block(blockSize, '\0');
    for(int i = blockSize - 1; i >= 0; --i) {
        uint32_t digit = rand.generateNext();
        char val = digit & 255;
        block[i] = val;
    }
    return block;
}