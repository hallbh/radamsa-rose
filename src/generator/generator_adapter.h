#include "generator.h"
#include "radamsa-rose/radamsa_definitions.h"

namespace Radamsa {
    class GeneratorAdapter : public Generator {
    private:
        struct input_chunk* (*genFunc)(const char*, size_t size, struct radamsa_constants, radamsa_random);
        struct radamsa_constants constants;
        Random* random;
    public:
        GeneratorAdapter(struct input_chunk* (*genFunc)(const char*, size_t size, struct radamsa_constants, radamsa_random), std::string name, int priority, radamsa_constants constants, Random& random);
        std::deque<std::string> generate(std::string input);
    };
}