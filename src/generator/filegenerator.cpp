#include "filegenerator.h"
#include <stdexcept>
#include <fstream>

Radamsa::FileGenerator::FileGenerator(int priority, const struct radamsa_constants constants, Random& random) : Generator(priority, "file") {
	this->random = &random;
	this->constants = constants;
}

void Radamsa::FileGenerator::checkForErrorInConstants() {
	if (this->constants.minBlockSize < 1) {
		throw std::logic_error("Minimum block size cannot be zero");
	}
	else if (this->constants.maxBlockSize == 0) {
		throw std::logic_error("Maximum block size cannot be zero");
	}
	else if (this->constants.maxBlockSize < this->constants.minBlockSize) {
		throw std::logic_error("Maximum block size must be greater than the minimum block size");
	}
}

//trim from begin
static inline void ltrim(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
		return !std::isspace(ch);
		}));
}

// trim from end (in place)
static inline void rtrim(std::string& s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
		return !std::isspace(ch);
		}).base(), s.end());
}

// trim from both ends (in place)
static inline std::string trim(std::string s) {
	ltrim(s);
	rtrim(s);
	return s;
}

std::vector<std::string> Radamsa::FileGenerator::splitPaths(std::string input) {
	std::vector<std::string> paths;
	std::string path;

	for (auto iterator = input.cbegin(); iterator != input.cend(); ++iterator) {
		const char c = *iterator;
		if (c == ',') {
			paths.push_back(path);
			path.clear();
			continue;
		}
		path.push_back(c);
	}
	if (!path.empty()) {
		paths.push_back(path);
	}
	return paths;
}

std::deque<std::string> Radamsa::FileGenerator::readFromFile(std::ifstream& file) {
	Radamsa::Bignum seed = this->random->nextInteger(Radamsa::Bignum("100000000000000000000")); // should be 100000000000000000000 but that is currently not supported
	Radamsa::Random privateRandom(seed);

	std::deque<std::string> chunks;

	int blockLength = privateRandom.nextBlockSize(constants.minBlockSize, constants.maxBlockSize);
	std::string last;
	std::vector<char> buffer(constants.maxBlockSize + 1);


	while (file.get(buffer.data(), blockLength + 1)) {
		int readsize = file.gcount();
		if (readsize == blockLength) {
			last.append(buffer.data());
			blockLength = privateRandom.nextBlockSize(constants.minBlockSize, constants.maxBlockSize);
			chunks.push_back(last);
			last.clear();
		}
		else {
			last.append(buffer.data());
			blockLength -= readsize;
		}
	}

	if (last.size() != 0) chunks.push_back(last);

	if (file.eof()) file.close();

	//delete[] buffer;
	return chunks;
}

std::deque<std::string> Radamsa::FileGenerator::generate(std::string input) {
	this->checkForErrorInConstants();
	std::deque<std::string> outputChunks;

	if (input.size() == 0) return outputChunks;

	auto paths = this->splitPaths(input);

	int length = paths.size();

	std::string path;

	int index = this->random->nextInteger(length);
	if (0 <= index && index < length) { // valid path index
		path = paths[index];
	}
	else {
		path.clear();
	}

	std::string trimmedPath = trim(path);

	std::ifstream file;

	file.open(trimmedPath, std::ios::in);
	if (file.fail()) {
		outputChunks.push_back(path);
		return outputChunks;
	}

	outputChunks = this->readFromFile(file);
	return outputChunks;
}
