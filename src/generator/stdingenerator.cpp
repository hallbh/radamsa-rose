#include "stdingenerator.h"
#include <stdexcept>

/// StdinGenerator
///
///

Radamsa::StdinGenerator::StdinGenerator(int priority, const struct radamsa_constants constants, Random& random) : Generator(priority, "stdin") {
	this->constants = constants;
	this->random = &random;
}

/*
generators.scm port->stream for stdin.
*/
Radamsa::Random Radamsa::StdinGenerator::createRandomFromRandom() {
	Radamsa::Random copy(*(this->random));
	Radamsa::Bignum seed = copy.nextInteger(Radamsa::Bignum("100000000000000000000")); // should be 100000000000000000000 but that is currently not supported
	return Radamsa::Random(seed);
}

void Radamsa::StdinGenerator::checkForErrorInConstants() {
	if (this->constants.minBlockSize < 1) {
		throw std::logic_error("Minimum block size cannot be zero");
	} else if (this->constants.maxBlockSize == 0) {
		throw std::logic_error("Maximum block size cannot be zero");
	} else if (this->constants.maxBlockSize < this->constants.minBlockSize) {
		throw std::logic_error("Maximum block size must be greater than the minimum block size");
	}
}


/// StdinGenerator break the string input to queue of chunks.
/// The length of each chunk is defined by random.
///
/// @param input string

std::deque<std::string> Radamsa::StdinGenerator::generate(std::string input) {
	// Once Stdin Generator has generated once, its output stays the same
	if (!this->memoizedOutput.empty())
		return this->memoizedOutput;

	this->checkForErrorInConstants();
	Radamsa::Random privateRandom = this->createRandomFromRandom();
	std::deque<std::string> outputChunks;
	int chunkStart = 0;
	do {
		int blockLength = privateRandom.nextBlockSize(constants.minBlockSize, constants.maxBlockSize);
		outputChunks.push_back(input.substr(chunkStart, blockLength));
		chunkStart += blockLength;
	} while (chunkStart < input.size());
	this->memoizedOutput = outputChunks;
	return outputChunks;
}
