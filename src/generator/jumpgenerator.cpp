#include "jumpgenerator.h"
#include <stdexcept>
#include <fstream>
#include <list>
#include "fuse.h"

Radamsa::JumpGenerator::JumpGenerator(int priority, const struct radamsa_constants constants, Random& random) : Generator(priority, "jump") {
	this->random = &random;
	this->constants = constants;
}

//trim from begin
static inline void ltrim(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
		return !std::isspace(ch);
		}));
}

// trim from end (in place)
static inline void rtrim(std::string& s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
		return !std::isspace(ch);
		}).base(), s.end());
}

// trim from both ends (in place)
static inline std::string trim(std::string s) {
	ltrim(s);
	rtrim(s);
	return s;
}

std::vector<std::string> Radamsa::JumpGenerator::splitPaths(std::string input) {
	std::vector<std::string> paths;
	std::string path;

	for (auto iterator = input.cbegin(); iterator != input.cend(); ++iterator) {
		const char c = *iterator;
		if (c == ',') {
			paths.push_back(path);
			path.clear();
			continue;
		}
		path.push_back(c);
	}
	if (!path.empty()) {
		paths.push_back(path);
	}
	return paths;
}

void Radamsa::JumpGenerator::checkForErrorInConstants() {
	if (this->constants.minBlockSize < 1) {
		throw std::logic_error("Minimum block size cannot be zero");
	}
	else if (this->constants.maxBlockSize == 0) {
		throw std::logic_error("Maximum block size cannot be zero");
	}
	else if (this->constants.maxBlockSize < this->constants.minBlockSize) {
		throw std::logic_error("Maximum block size must be greater than the minimum block size");
	}
}

std::deque<std::string> Radamsa::JumpGenerator::readFromFile(std::ifstream& file) {
	Radamsa::Bignum seed = this->random->nextInteger(Radamsa::Bignum("100000000000000000000"));
	Radamsa::Random privateRandom(seed);

	std::deque<std::string> chunks;

	int blockLength = privateRandom.nextBlockSize(constants.minBlockSize, constants.maxBlockSize);
	std::string last;
	std::vector<char> buffer(constants.maxBlockSize + 1);


	while (file.get(buffer.data(), blockLength + 1)) {
		int readsize = file.gcount();
		if (readsize == blockLength) {
			last.append(buffer.data());
			blockLength = privateRandom.nextBlockSize(constants.minBlockSize, constants.maxBlockSize);
			chunks.push_back(last);
			last.clear();
		}
		else {
			last.append(buffer.data());
			blockLength -= readsize;
		}
	}

	if (last.size() != 0) chunks.push_back(last);

	if (file.eof()) file.close();

	return chunks;
}

std::deque<std::string> Radamsa::JumpGenerator::generate(std::string input) {
	this->checkForErrorInConstants();

	std::deque<std::string> outputChunks;
	if (input.size() == 0) return outputChunks;

	auto paths = this->splitPaths(input);
	int length = paths.size();

	std::string pathA;
	std::string pathB;

	int indexA = this->random->nextInteger(length);
	int indexB = this->random->nextInteger(length);

	if (0 <= indexA && indexA < length) { // valid path index
		pathA = paths[indexA];
	}
	if (0 <= indexB && indexB < length) {
		pathB = paths[indexB];
	}

	std::string trimPathA = trim(pathA);
	std::string trimPathB = trim(pathB);


	std::ifstream fileA;
	std::ifstream fileB;

	fileA.open(trimPathA, std::ios::in);
	fileB.open(trimPathB, std::ios::in);

	if (fileA.fail() || fileB.fail()) {
		if (pathA == pathB) {
			outputChunks.push_back(pathA);
			return outputChunks;
		}
		outputChunks.push_back(pathA);
		outputChunks.push_back(pathB);
		return outputChunks;
	}

	std::deque<std::string> chunksA = this->readFromFile(fileA);
	std::deque<std::string> chunksB = this->readFromFile(fileB);

	Radamsa::Bignum seed = (this->random)->nextInteger(Radamsa::Bignum("68719476735"));
	Radamsa::Random jumpRand(seed);

	outputChunks = jumpSomewhere(jumpRand,chunksA,chunksB);
	return outputChunks;
}

std::deque<std::string> Radamsa::JumpGenerator::jumpSomewhere(Radamsa::Random& privateRand, std::deque<std::string> chunkA, std::deque<std::string> chunkB) {
	
	if (chunkA.empty()) return chunkB;
	if (chunkB.empty()) return chunkA;

	std::string topA = chunkA.front();
	std::string topB = chunkB.front();
	chunkA.pop_front();
	chunkB.pop_front();

	std::deque<std::string> output = walkToJump(privateRand, 3, topA, chunkA, topB, chunkB);
	
	return output;
	
}

std::deque<std::string> Radamsa::JumpGenerator::walkToJump(Radamsa::Random& privateRand, int ip, std::string strA, std::deque<std::string> chunkA, std::string strB, std::deque<std::string> chunkB) {
	
	Radamsa::Random tempRand = privateRand;
	std::string tempstrA = strA;
	std::string tempstrB = strB;
	std::deque<std::string> tempchunkA = chunkA;
	std::deque<std::string> tempchunkB = chunkB;

	int n = privateRand.nextInteger(ip++);
	//if (n == 0 || chunkA.empty()) return finish(privateRand, strA, chunkA, strB, chunkB);
	if (n == 0 || chunkA.empty()) return finish(tempRand,tempstrA,tempchunkA,tempstrB,tempchunkB);
	
	std::string aa = chunkA.front();
	chunkA.pop_front();

	n = privateRand.nextInteger(3);


	if (n == 0) {
		std::deque<std::string> out = walkToJump(privateRand, ip, aa, chunkA, strB, chunkB);
		out.push_front(strA);
		return out;
	}
	//if (chunkB.empty()) return finish(privateRand, strA, chunkA, strB, chunkB);
	if (chunkB.empty()) return finish(tempRand, tempstrA, tempchunkA, tempstrB, tempchunkB);

	std::string bb = chunkB.front();
	chunkB.pop_front();

	std::deque<std::string> out = walkToJump(privateRand, ip, aa, chunkA, bb, chunkB);
	out.push_front(strA);
	return out;
}

std::deque<std::string> Radamsa::JumpGenerator::finish(Radamsa::Random privateRand, std::string strA, std::deque<std::string> chunkA, std::string strB, std::deque<std::string> chunkB) {
	std::vector<char> a(strA.begin(), strA.end());
	std::vector<char> b(strB.begin(), strB.end());

	std::vector<char> res = fuse(a, b,privateRand, this->constants);
	chunkA = std::deque<std::string>();


	std::string ab(res.begin(), res.end());
	chunkB.push_front(ab);
	return chunkB;
}