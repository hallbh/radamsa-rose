#pragma once
#include <string>
#include <deque>
#include "random/random.h"
#include "radamsa-rose/constants.h"

namespace Radamsa {
	class Generator {
	private:
		std::string name;
	public:
		int priority;
		Generator(int priority, std::string name);
		virtual ~Generator();
		virtual std::deque<std::string> generate(std::string input) = 0;
		std::string getName();
	};

}