#pragma once
#include "generator.h"

namespace Radamsa {
	class FileGenerator : public Generator {
	private:
		struct radamsa_constants constants;
		Random* random;

		void checkForErrorInConstants();
		std::vector<std::string> splitPaths(std::string input);
		std::deque<std::string> readFromFile(std::ifstream& file);
	public:
		FileGenerator(int priority, struct radamsa_constants constants, Random& random);
		std::deque<std::string> generate(std::string) override;
	};
}