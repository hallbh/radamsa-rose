#include "generator_adapter.h"
#include "chunk_queue_interface.h"

/// Generator Adapter Constructer
///
/// By using adapter, incompatible generator can collaborate.
///
/// @param gen generator object

Radamsa::GeneratorAdapter::GeneratorAdapter(struct input_chunk* (*genFunc)(const char*, size_t size, struct radamsa_constants, radamsa_random), std::string name, int priority, radamsa_constants constants, Random& random) : Generator(priority, name) {
    this->genFunc = genFunc;
    this->random = &random;
    this->constants = constants;
}

std::deque<std::string> Radamsa::GeneratorAdapter::generate(std::string input) {
    struct input_chunk* chunks = genFunc(input.c_str(), input.size(), this->constants, this->random);
    return Radamsa::fromLinkedStruct(chunks);
}
