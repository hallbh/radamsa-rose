#pragma once
#include "radamsa-rose/constants.h"
#include "bignum.h"
#include <stack>
#include <vector>
#include <cstdint>
#include <algorithm>
#include <string>

#define PROMOTER_LEN 74
#define FIXNUM 16777216 // 2^24 // 1 greater than the largest fixnum 

namespace Radamsa {

	template <class T>
	struct Label {
		uint32_t label;
		T element;
	};

	template <class E>
	bool sortLabel(struct Label<E> a, struct Label<E> b) {
		return a.label > b.label;
	}

	class Random {
	public:
		Random(unsigned long long seed);
		Random(Bignum seed);

		uint32_t peekState();

		virtual unsigned long long nextInteger(unsigned long long max);
		virtual Bignum nextInteger(Bignum max);
		virtual bool doesOccur(struct rational probability);
		virtual unsigned int nextBlockSize(unsigned int minSize, unsigned int maxSize);
		virtual int randomDelta();
		virtual int randomDeltaUp(rational probability);
		virtual Bignum nextNBit(unsigned long long n);
		virtual Bignum nextLogInteger(unsigned long long max);
		virtual unsigned long long nextIntegerRange(unsigned long long low, unsigned long long high);

		// End is exclusive
		template <class E>
		void permute(std::vector<E>& list, size_t start, size_t end) {
			if (!list.empty()) {
				std::vector< struct Label<E> > labels;
				for (auto it = list.cbegin() + start; it != ((end < list.size()) ? list.cbegin() + end : list.cend()); ++it) {
					struct Label<E> elem = { generateNext(), *it };
					labels.push_back(elem);
				}
				shuffle(labels, 0, labels.size() - 1);
				for (auto it = labels.cbegin(); it != labels.cend(); ++it) {
					list[start++] = (*it).element;
				}
			}
		}
		
		/*
			Aki's shuffle essentially sorts the list into groups based on a random label.
			The groups are sorted from highest to lowest label number.
			The groups are then treated as new lists to then label and shuffle again.
			|............|
			|....|....|..|
			|..|.|..|.|..|
			This shuffle will be doing things in place.
		*/
		template <class E>
		void shuffle(std::vector< struct Label<E> >& labels, int startIndex, int endIndex) {
			if (endIndex - startIndex > 0) {
				std::sort(labels.begin() + startIndex, labels.begin() + endIndex + 1, Radamsa::sortLabel<E>);
				// Go in reverse up the list
				for (int index = endIndex - 1; index >= 0; index--) {
					if (labels.at(index).label < labels.at(endIndex).label) {
						int subListStart = index + 1;
						for (int i = subListStart; i <= endIndex; i++) {
							labels.at(i).label = this->generateNext();
						}
						shuffle(labels, index + 1, endIndex);
						endIndex = index;
					}
				}
			}
		}

		virtual std::vector<std::string> reservoirSample(std::vector<std::string> list, int n);
		std::vector<std::string> reservoirInit(std::vector<std::string> &list, int n, int p, std::vector<std::string> &reservoir);
		std::vector<std::string> reservoirSampler(std::vector<std::string> &list, int n, int p, std::vector<std::string> &reservoir);


		// public for testing purposes
	//private:
		// Random has a state boolean which lets it know which of the streams to walk on the next call to successors
		// Stream a is the one that is actually used for random stuff
		// Stream b is the "seed" that is carried around
		// Both are stored as stacks of 24-bit unsigned "fixnums" as they are represented in Radamsa
		bool state; // Do we walk the seed or not
		Bignum stream_a; // Random stream - Hare
		Bignum stream_b; // Seed - Tortoise

		// Both are 24-bit fixnums used to modify the state of random when getting successors
		static const uint32_t accumulator = 8388617;
		static const uint32_t multiplier = 3133337;

		// This is a magic number from Radamsa equal to decimal 111,111,111,111,111,111,111,111
		// represented as a sequence of bits for bignum mathematics.
		static uint8_t promoter[PROMOTER_LEN];
		
		void initialize(Bignum seed);

		// Random state manipulation functions
		void walk(Bignum *seed, Bignum *out, uint32_t acc);
		void generateSuccessors();
		uint32_t generateNext();

		// Random helper functions
		int rand_fixnum(int max);
		bool is_lesser(uint32_t a, uint32_t b);
		uint32_t rand_bignum_topdigit(uint32_t digit);
		Bignum rand_bignum(Bignum max);
	};
}