#include "random.h"
#include "random.h"
#include "random.h"
#include <stack>
#include <vector>
#include <math.h>
#include <exception>
#include <cstdint>
#include <stdexcept>
#include <stdio.h>

using namespace Radamsa;

uint8_t Random::promoter[] = { 1,0,0,1,0,1,1,0,1,0,0,1,0,1,0,1,0,1,1,0,1,0,0,1,0,0,0,1,1,0,1,1,1,0,0,1,0,1,1,1,0,1,1,0,1,0,1,0,0,1,1,0,0,1,1,1,0,0,0,1,1,1,0,0,0,1,1,1,0,0,0,1,1,1 }; // help;

/// Random Constructer
///
/// @param seed a Bignum object

void Random::initialize(Bignum seed) {
	stream_a = Bignum();
	stream_b = Bignum();
	state = true;

	if (seed < Bignum((unsigned long long) FIXNUM)) {
		// Promotes the seed to a bignum and stores the result in the seed stack.
		// 
		// stream_b = (seed + 1) * 11111111111111111111111
		seed = seed + Bignum(1);

		std::vector<uint8_t> bits = std::vector<uint8_t>();
		for (uint8_t bit : promoter) bits.push_back(bit);
		std::reverse(bits.begin(), bits.end()); // Convert to [lsd ... msd]
		Bignum promoted = Bignum(bits);
		
		promoted = seed * promoted;

		stream_b = promoted;
	}
	else {
		stream_b = seed;
	}

	Bignum clone_b = Bignum(stream_b);
	walk(&clone_b, &stream_a, accumulator);
}

void Random::walk(Bignum *seed, Bignum *out, uint32_t acc)
{
	// Recursive base case.
	if (seed->empty()) return;

	uint64_t mult = (uint64_t) seed->pop() * multiplier;
	uint32_t lo = mult % FIXNUM;
	uint32_t hi = mult / FIXNUM;

	uint32_t accumulation = (lo + acc) % FIXNUM;
	out->push(accumulation);

	walk(seed, out, hi);
}

void Random::generateSuccessors()
{
	Bignum a_out = Bignum();
	// if (a == b) - Friends meet, we're going to need a bigger track
	if (stream_a == stream_b) { // std::stack has a == operator which works like a java a.equals(b) would
		state ? stream_a.push(accumulator) : stream_a.push(multiplier);
		stream_b = stream_a;
		state = true;
	}
	// if (st) - Hare and tortoise
	else if (state) {
		state = false;
		Bignum b_out = Bignum();
		walk(&stream_b, &b_out, accumulator);
		stream_b = b_out;
	}
	// else just Hare
	else {
		state = true;
	}

	// Always walk stream_a
	walk(&stream_a, &a_out, accumulator);
	stream_a = a_out;
}

uint32_t Random::peekState() {
	return stream_a.peek();
}
/// Generate a another random number

uint32_t Random::generateNext()
{
	// In owl lisp, the random state is returned as this value "next"
	uint32_t next = stream_a.peek();
	// And a delayed evaluation of this on the current random state:
	generateSuccessors();
	// We manually call this every time we want to get a random number from the stream.
	// Which is any time we see an (uncons rs _) in (owl random) or radamsa.
	// Actually a fixnum (24 bits)
	return next;
}

/// basic nextInteger function to generate random number
///
/// @param max int

int Random::rand_fixnum(int max)
{
	int maxFixnum = FIXNUM - 1;
	uint32_t nextInt = generateNext();

	if (nextInt == maxFixnum) return 0;
	
	uint64_t product = ((uint64_t) max * (uint64_t) nextInt);
	uint32_t quotient = product / maxFixnum;

	return quotient;
}
/// a function to define whether a number is lesser than another number
///
/// @param a first uint32_t number
/// @param b second uint32_t number

bool Random::is_lesser(uint32_t a, uint32_t b) {
	return a < b;
}

/// grabs the "top" fixnum for a bignum returned by nextInteger
///
/// @param digit uint32_t object

uint32_t Random::rand_bignum_topdigit(uint32_t digit) {
	if (digit == FIXNUM - 1) { // if we don't have a limit
		return generateNext();
	}
	else {
		return rand_fixnum(digit + 1);
	}
}

/// basic nextInteger function for bignum object to generate random number
///
/// @param max Bignum

Bignum Random::rand_bignum(Bignum max)
{
	Bignum left = Bignum(max);
	Bignum out = Bignum();
	bool lower = false;
	uint32_t digit = 0;
	uint32_t top = 0;

	digit = left.pop();
	while (left.fixVector.size() > 0) {
		uint32_t next = generateNext();

		out.push(next);
		lower = (next < digit);
		digit = left.pop();
	}

	if (lower) {
		top = rand_bignum_topdigit(digit);
	}
	else {
		top = rand_fixnum(digit);
	}

	if (top > 0) out.push(top);
	// If you're paying attention, you'll realize we just pushed the top to 
	// the least significant position SO:
	// YOU SHOULDN'T DO THIS TO THE POOR BIGNUM OK - Do as I say not as I do.
	std::reverse(out.fixVector.begin(), out.fixVector.end());
	out.refreshBitVector();

	return out;
}

/// Random constructer for unsigned long long seed
///
/// @param seed unsigned long long

Random::Random(unsigned long long seed) {
	// Initialize the first successors when the Random is constructed
	initialize(Bignum(seed));
}

/// Random constructer for bignum seed
///
/// @param seed unsigned long long

Random::Random(Bignum seed) {
	initialize(seed);
}

/// Return a random number for unsigned long long input
///
/// @param max unsigned long long 

unsigned long long Radamsa::Random::nextInteger(unsigned long long max)
{
	return (unsigned long long) nextInteger(Bignum(max));
}

/// Return a random number for bignum input
///
/// @param max unsigned long long 

Bignum Random::nextInteger(Bignum max)
{
	// Bignum can't be negative so :p
	// Also none of our inputs can be negative by typing so we're golden
	// if (max < Bignum(0ULL)) return Bignum(0ULL); // Bug ? deviates from how owl lisp does it, but I'm too lazy to throw an error here.
	if (max == Bignum(0ULL)) return Bignum(0ULL);
	if (max < Bignum((unsigned long long) FIXNUM)) { // Only one fixnum in fixnum vector
		uint32_t fix = max.peek();
		return Bignum(rand_fixnum(fix));
	}
	else { 
		return rand_bignum(max);
	}

	return Bignum(0ULL);
}
/// Base on the probability define that whether occur
///
/// @param probability struct rational

bool Random::doesOccur(struct rational probability)
{
	// 0 probability = false
	if (probability.numerator == 0) return false;

	// Number greater than 1 - greater than 100% chance, therefore true
	if (probability.numerator > probability.denominator) return true;
	
	// If the argument is negative return false - not a problem with unsigned ints in the struct

	// Maybe bug - I condensed some redundant logic here.
	/*if (probability.numerator == 1) {
		return rand(probability.denominator) == 0;
	}
	else {*/
	auto next = nextInteger(probability.denominator);
	auto out = next < probability.numerator;

	return out;
	//}
}

/// base on the minSize and maxSize, return the next block size
///
/// @param minSize unsigned int
/// @param maxSize unsigned int

unsigned int Random::nextBlockSize(unsigned int minSize, unsigned int maxSize)
{
	return std::max((unsigned int) nextInteger(maxSize), minSize);
}

/// random delta for brownian steppers

int Random::randomDelta()
{
	uint32_t next = generateNext();
	if ((next & 1) == 0) {
		return 1;
	}
	return -1;
}

/// random delta up to take probability into account
///
/// @param probability rational object

int Random::randomDeltaUp(rational probability) {
	bool occ = doesOccur(probability);
	return occ ? 1 : -1;
}

Bignum Radamsa::Random::nextNBit(unsigned long long n)
{
	if (n == 0)
		return 0;
	Bignum high = Bignum(1ULL);
	high << (n - 1);

	Bignum val = this->nextInteger(high);
	Bignum out = val | high;

	return out;
}

Bignum Radamsa::Random::nextLogInteger(unsigned long long max)
{
	if (max == 0)
		return 0;
	unsigned long long n = this->nextInteger(max);
	return this->nextNBit(n);
}

/// return a random number between low and high
///
/// @param low unsigned long long
/// @param high unsigned long long 

unsigned long long Radamsa::Random::nextIntegerRange(unsigned long long low, unsigned long long high)
{
	if (high == low) {
		throw std::logic_error("Low cannot be higher than high for a random range");
	}
	unsigned long long val = this->nextInteger(high - low);
	return val + low;
}

std::vector<std::string> Radamsa::Random::reservoirSample(std::vector<std::string> list, int n) {
	std::vector<std::string> reservoir = std::vector<std::string>();
	return reservoirInit(list, n, 0, reservoir);
}

std::vector<std::string> Radamsa::Random::reservoirInit(std::vector<std::string> &list, int n, int p, std::vector<std::string> &reservoir) {
	if (list.empty()) {
		return reservoir;
	}
	else if (n == p) {
		return reservoirSampler(list, n, n + 1, reservoir);
	}
	else {
		reservoir.emplace(reservoir.begin(), list[0]);
		list.erase(list.begin(), list.begin() + 1);
		return reservoirInit(list, n, p + 1, reservoir);
	}
}

std::vector<std::string> Radamsa::Random::reservoirSampler(std::vector<std::string> &list, int n, int p, std::vector<std::string> &reservoir) {
	if (list.empty()) {
		return reservoir;
	}
	else {
		int x = nextInteger(p);
		if (x < n) {
			reservoir[x] = list[0];
		}
		list.erase(list.begin(), list.begin() + 1);
		return reservoirSampler(list, n, p + 1, reservoir);
	}
}
