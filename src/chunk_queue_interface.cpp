#include "chunk_queue_interface.h"

struct input_chunk* Radamsa::toChunk(std::string input) {
    struct input_chunk* chunk = new input_chunk();
    chunk->chunkSize = input.size();
    chunk->chunkPointer = new char[chunk->chunkSize];
    input.copy(chunk->chunkPointer, chunk->chunkSize);
    return chunk;
}

struct input_chunk* Radamsa::toLinkedStruct(std::deque<std::string>& input) {
    struct input_chunk* inputHead = toChunk(input.front());
	input.pop_front();

    struct input_chunk* prevChunk = inputHead;
    while (!input.empty()) {
        struct input_chunk* chunk = toChunk(input.front());
        prevChunk->next = chunk;
        prevChunk = chunk;
        input.pop_front();
    }
    return inputHead;
}

std::deque<std::string> Radamsa::fromLinkedStruct(struct input_chunk* chunk) {
    std::deque<std::string> input;
    struct input_chunk* prev;
    while (chunk) {
        input.push_back(std::string(chunk->chunkPointer, chunk->chunkSize));
        delete[] chunk->chunkPointer;
        prev = chunk;
        chunk = chunk->next;
        delete prev;
    }
    return input;
}