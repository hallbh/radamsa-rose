extern "C" {
	#include "radamsa-rose/rose-fuzzer.h"
}
#include "radamsa_algorithm.h"
#include "algorithm.h" // Not a mistake, this is our algorithm that runs radamsa's components
#include <string>
#include <algorithm>

radamsa_rose newRadamsaRoseFuzzer(int randomSeed, const struct radamsa_constants constants) {
	return new Radamsa::RadamsaAlgorithm(randomSeed, constants);
}

void deleteRadamsaRoseFuzzer(radamsa_rose radamsa) {
	delete (Radamsa::RadamsaAlgorithm*) radamsa;
}

size_t runRadamsa(radamsa_rose radamsa, const char* input, size_t inputSize, char* target, size_t max_size) {
	Radamsa::RadamsaAlgorithm* algorithm = (Radamsa::RadamsaAlgorithm*) radamsa;

	std::unique_ptr<Radamsa::Generator> gen = algorithm->muxAndGetGenerators();
	std::unique_ptr<Radamsa::Pattern> pat = algorithm->muxAndGetPatterns();
	std::unique_ptr<Radamsa::Mutation> muta = algorithm->muxAndGetMutations();

	Radamsa::AlgorithmParameters params = { muta.get(), gen.get(), pat.get() };
	std::string in(input, inputSize);
	std::string out = Radamsa::runAlgorithm(in, params);
	out.copy(target, max_size, 0);
	return std::min(out.size(), max_size);
}

void initDefaultConstants(radamsa_constants* constants) {
	constants->maxHashCount = 1024 * 1024; //digests.scm line 21
	constants->searchFuel = 100000; // fuse.scm line 16
	constants->searchStopIp = 8; // fuse.scm line 18
	constants->storedElements = 10; // generic.scm line 126
	constants->updateProbability = constants->storedElements << 1;
	constants->minScore = 2; // mutations.scm line 27
	constants->maxScore = 10; //mutations.scm line 28
	constants->pWeakUsually.numerator = 11; // mutations.scm line 30
	constants->pWeakUsually.denominator = 20;
	// constants->randDeltaDecrease = -1; // mutations.scm line 41, 48
	// Sed-num constants? mutations.scm line 150, 153
	// Repeat-len limit constants? mutations.scm 207
	// sed-byte-perm rand-range constants? mutations.scm 247
	// sed-seq-repeat constants? rand-log base mutations.scm line 334, 702
	constants->minTexty = 6; // mutations.scm line 738
	//rand-lex-string constants? mutations.scm line 837
	constants->maxMutations = 16; //patterns.scm line 21
	//pat-burst min constant? patterns.scm line 71
	constants->averageBlockSize = 2048; // shared.scm line 34
	constants->minBlockSize = 256; // shared.scm line 35
	constants->initialIp = 24; // shared.scm line 36
	constants->remutateProbability.numerator = 4; // shared.scm line 37
	constants->remutateProbability.denominator = 5;
	constants->maxBlockSize = 2 * constants->averageBlockSize; // shared.scm line 53
	constants->maxPumpLimit = 0b100000000000000; // xp.scm line 633
}

void enableMutation(radamsa_rose radamsa, char* name) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setMutationEnabled(name, true);
}

void disableMutation(radamsa_rose radamsa, char* name) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setMutationEnabled(name, false);
}

void enableAllMutations(radamsa_rose radamsa) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setAllMutationsEnabled(true);
}

void disableAllMutations(radamsa_rose radamsa) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setAllMutationsEnabled(false);
}

void setMutationPriority(radamsa_rose radamsa, char* name, int priority) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setMutationPriority(name, priority);
}

void enablePattern(radamsa_rose radamsa, char* name) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setPatternEnabled(name, true);
}

void disablePattern(radamsa_rose radamsa, char* name) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setPatternEnabled(name, false);
}

void enableAllPatterns(radamsa_rose radamsa) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setAllPatternsEnabled(true);
}
void disableAllPatterns(radamsa_rose radamsa) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setAllPatternsEnabled(false);
}

void setPatternPriority(radamsa_rose radamsa, char* name, int priority) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setPatternPriority(name, priority);
}

void enableGenerator(radamsa_rose radamsa, char* name) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setGeneratorEnabled(name, true);
}

void disableGenerator(radamsa_rose radamsa, char* name) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setGeneratorEnabled(name, false);
}

void enableAllGenerators(radamsa_rose radamsa) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setAllGeneratorsEnabled(true);
}

void disableAllGenerators(radamsa_rose radamsa) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setAllGeneratorsEnabled(false);
}

void setGeneratorPriority(radamsa_rose radamsa, char* name, int priority) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->setGeneratorPriority(name, priority);
}

void addNewMutation(radamsa_rose radamsa, int (*mutate)(struct input_chunk*, radamsa_constants, radamsa_random), char* name, int priority) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->addCustomMutation(mutate, std::string(name), priority);
}

void addNewPattern(radamsa_rose radamsa, struct input_chunk* (*runFunc)(struct input_chunk*, radamsa_mutation, struct radamsa_constants, radamsa_random), char* name, int priority) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->addCustomPattern(runFunc, std::string(name), priority);
}

void addNewGenerator(radamsa_rose radamsa, struct input_chunk* (*genFunc)(const char*, size_t size, struct radamsa_constants, radamsa_random), char* name, int priority) {
	((Radamsa::RadamsaAlgorithm*) radamsa)->addCustomGenerator(genFunc, std::string(name), priority);
}