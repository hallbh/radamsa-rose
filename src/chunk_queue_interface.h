#pragma once
#include "radamsa-rose/radamsa_definitions.h"
#include <string>
#include <queue>

namespace Radamsa {
    struct input_chunk* toChunk(std::string input);
    struct input_chunk* toLinkedStruct(std::deque<std::string>& input);
    std::deque<std::string> fromLinkedStruct(struct input_chunk* chunk);
}