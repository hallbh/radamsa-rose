#include <string>
#include <unordered_map>
#include <list>
#include <memory>
#include <stdexcept>
#include <algorithm>
#include <vector>

namespace Radamsa {
    template <class V>
    class EnableOrderedMap {
    private:
        std::unordered_map<std::string, std::unique_ptr<V> > elementMap;
        std::list<std::string> elementNames;
        std::list<std::string> insertionOrderedNames;
    public:
        template <class T, class... Args>
        void insert(Args&&... args) {
            auto ptr = std::make_unique<T>(std::forward<Args>(args)...);
            std::string name = ptr->getName();
            if (this->elementMap.count(name)) {
                this->remove(name);
                this->elementMap[name] = std::move(ptr);
            } else {
                this->elementMap.emplace(ptr->getName(), std::move(ptr));
            }
            this->elementNames.push_back(name);
            this->insertionOrderedNames.push_back(name);
        }

        void setEnabled(std::string name, bool enabled) {
            this->remove(name);
            if (enabled && this->elementMap.count(name)) {
                this->elementNames.push_back(name);
            }
        }

        void setAllEnabled(bool enabled) {
            if (enabled) {
                elementNames = insertionOrderedNames;
            } else {
                elementNames.clear();
            }
        }

        void remove(std::string name) {
            auto ind = std::find(this->elementNames.begin(), this->elementNames.end(), name);
            if (ind != this->elementNames.end())
                this->elementNames.erase(ind);
        }

        bool isEnabled(std::string name) {
            return std::find(this->elementNames.begin(), this->elementNames.end(), name) != this->elementNames.end();
        }

        void setPriorityOf(std::string name, int pri) {
            if (this->elementMap.count(name)) {
                this->elementMap.at(name)->priority = pri;
            }
        }

        std::vector<V*> getOrderedList() {
            std::vector<V*> list;
            for (std::string name : this->elementNames) {
                list.push_back(this->elementMap[name].get());
            }
            return list;
        }
    };
}
