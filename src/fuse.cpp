#include "fuse.h"
#include <utility>
#include <map>
#include <list>

using namespace Radamsa;

std::vector<std::vector<char>> createSuffixes(std::vector<char> list) {
    std::vector<std::vector<char>> suffixes;
    for (auto it = list.begin(); it != list.end(); it++) {
        suffixes.push_back(std::vector<char>(it, list.end()));
    }
    return suffixes;
}

std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>> createAlternatingSuffixes(std::vector<char> list, Random& random) {
    std::vector<std::vector<char>> suffixesOne;
    suffixesOne.push_back(list);
    
    std::vector<std::vector<char>> suffixesTwo;
    suffixesTwo.push_back(list);

    for (auto it = list.begin() + 1; it != list.end(); it++) {
        if (random.generateNext() & 1) {
            suffixesOne.insert(suffixesOne.begin(), std::vector<char>(it, list.end()));
        } else {
            suffixesTwo.insert(suffixesTwo.begin(), std::vector<char>(it, list.end()));
        }
    }

    return std::make_pair(suffixesOne, suffixesTwo);
}

std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>> createInitialSuffixes(std::vector<char> listOne, std::vector<char> listTwo, Random& random) {
    if (listOne == listTwo) {
        return createAlternatingSuffixes(listOne, random);
    }
    return std::make_pair(createSuffixes(listOne), createSuffixes(listTwo));
}

std::pair<std::vector<char>, std::vector<char>> getAnyPositionPair(std::vector<std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>>> nodes, Random& random) {
    //std::reverse(nodes.begin(), nodes.end());   // this reversal works for my first test case
    auto node = (nodes.at(random.nextInteger(nodes.size())));
    std::vector<char> fromSuffix = node.first.at(random.nextInteger(node.first.size()));
    std::vector<char> toSuffix = node.second.at(random.nextInteger(node.second.size()));
    return std::make_pair(fromSuffix, toSuffix);
}

std::map<char, std::list<std::vector<char>>> createCharSuffixes(std::vector<std::vector<char>> suffixes) {
    std::map<char, std::list<std::vector<char>>> charSuffixes;
    for (auto it = suffixes.begin(); it != suffixes.end(); it++) {
        if (!it->empty()) {
            char character = it->front();
            it->erase(it->begin());
            charSuffixes[character].push_front(*it);
        }
    }
    return charSuffixes;
}

std::vector<std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>>> split(std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>> node) {
    std::map<char, std::list<std::vector<char>>> charSuffixesOne = createCharSuffixes(node.first);
    std::map<char, std::list<std::vector<char>>> charSuffixesTwo = createCharSuffixes(node.second);
    std::vector<std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>>> newNodes;
    for (auto it = charSuffixesOne.begin(); it != charSuffixesOne.end(); it++) {
        // The two suffix chains share the starting byte
        if (!charSuffixesTwo[it->first].empty()) {
            std::vector<std::vector<char>> newSuffixesOne(it->second.begin(), it->second.end());
            std::vector<std::vector<char>> newSuffixesTwo(charSuffixesTwo[it->first].begin(), charSuffixesTwo[it->first].end());
            newNodes.insert(newNodes.begin(), std::make_pair(newSuffixesOne, newSuffixesTwo));
        }
    }
    return newNodes;
}

std::pair<std::vector<char>, std::vector<char>> findJumpPoints(std::vector<char> listOne, std::vector<char> listTwo, Random& random, const radamsa_constants constants) {
    auto node = createInitialSuffixes(listOne, listTwo, random);
    std::vector<std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>>> nodes;
    nodes.push_back(node);
    int fuel = constants.searchFuel;
    while (fuel > 0) {
        if (random.nextInteger(constants.searchStopIp) == 0)
            break;
        std::vector<std::pair<std::vector<std::vector<char>>, std::vector<std::vector<char>>>> newNodes;
        for (auto it = nodes.begin(); it != nodes.end(); it++) {
            auto subNewNodes = split(*it);
            newNodes.insert(newNodes.begin(), subNewNodes.begin(), subNewNodes.end());
        }
        if (newNodes.empty()) {
            break;
        }
        fuel -= newNodes.size();
        nodes = std::move(newNodes);
    }
    return getAnyPositionPair(nodes, random);
}

std::vector<char> jump(std::vector<char> list, std::pair<std::vector<char>, std::vector<char>> jumpPoints) {
    if (!jumpPoints.first.empty()) {
        for (auto it = list.begin(); it != list.end(); it++) {
            if (std::vector<char>(it, list.end()) == jumpPoints.first) {
                list.erase(it, list.end());
                break;
            }
        }
    }
    list.insert(list.end(), jumpPoints.second.begin(), jumpPoints.second.end());
    return list;
}

std::vector<char> Radamsa::fuse(std::vector<char> listOne, std::vector<char> listTwo, Random& random, const radamsa_constants constants) {
    if (listOne.empty()) {
        return listTwo;
    } else if (listTwo.empty()) {
        return listOne;
    }
    return jump(listOne, findJumpPoints(listOne, listTwo, random, constants));
}