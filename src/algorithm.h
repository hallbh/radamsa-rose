#pragma once
#include <string>
#include "pattern/pattern.h"
#include "generator/generator.h"
#include "mutation/mutation.h"
#include "radamsa-rose/constants.h"
#include "random/random.h"

namespace Radamsa {
	struct AlgorithmParameters {
		Radamsa::Mutation* mutation;
		Radamsa::Generator* generator;
		Radamsa::Pattern* pattern;
	};

	std::string runAlgorithm(std::string input, Radamsa::AlgorithmParameters params);
}