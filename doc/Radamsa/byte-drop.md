## sed-byte-drop
```scheme
(define (sed-byte-drop rs ll meta) ;; drop byte
    (lets 
        ((rs p (rand rs (sizeb (car ll))))
         (rs d (rand-delta rs)))
        (values sed-byte-drop rs 
            (cons (edit-byte-vector (car ll) p (λ (old tl) tl)) (cdr ll))
            (inc meta 'byte-drop) d)))
```
__Input:__
* rs: random seed (defined in owl lisp in random.scm)
* ll: linked list of bytes
* meta: the operation tracker

__Output:__
?

### Description
---

Uses a random seed defined in `rs` that then selects a random byte in `ll` before dropping it. The operation is recorded in `meta`.

### Line Breakdown
---
*In progress*
