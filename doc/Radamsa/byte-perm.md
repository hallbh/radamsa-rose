## sed-byte-perm
``` scheme
 (define (sed-byte-perm rs ll meta) ;; permute a few bytes
         (lets 
            ((rs p (rand rs (sizeb (car ll))))
             (rs d (rand-delta rs)))
            (values sed-byte-perm rs 
               (cons 
                  (edit-byte-vector (car ll) p 
                     (λ (old tl)
                        (lets
                           ((lst (cons old tl))
                            (rs n (rand-range rs 2 20)) ;; fixme: magic constant, should use something like repeat-len
                            (head (take lst n))
                            (tail (drop lst n))
                            (rs head (random-permutation rs head)))
                           (append head tail))))
                  (cdr ll))
               (inc meta 'byte-perm) d)))
```
__Input:__
* rs: random seed
* ll: ?
* meta: operation tracker
  
__Output:__

?

### Description
---
Permutes bytes in `ll` using a random-seed defined by `rs` and saves ther operation to `meta`

### Line Breakdown
---
*In progress*