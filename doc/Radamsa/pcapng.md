# Documentation of pcapng.scm

## Exports

### pcapng-input?
Returns true if the passed in dictionary contains an entry for "pcapng" under generators, otherwise false

### pcapng-block-to-mutate?
Checks the input against a constant and returns true if they match, false otherwise.

### pcapng-instrument-mutations?
only mutates Enhanced Packet Blocks - I'm pretty tired and out of it, so this function is a little greek to me.

### uint32->bytes
Converts a list of uint32's to bytes

### bytes->uint32
Converts a list of bytes to uint32's