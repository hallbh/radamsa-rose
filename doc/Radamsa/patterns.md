# Documentation of `patterns.scm`

Patterns defines the core of where and how Radamsa decides how to mutate the input.
There are three defined patterns that are enumerated in `*patterns*`, an option list
that is displayed to the user.

There are three patterns in `*patterns` that can be specified in a comma separated list, 
and if the option is not specified on the command line, it defaults to `default-patterns`. 
The patterns are "od" - Mutate once, "nd" - Mutate [1-many] times, and "bu" - Make several
mutations closeby once.

"od" maps to `pat-once-dec`, "nd" maps to `pat-many-dec`, and "bu" maps to `pat-burst`

The shape of these functions is: `pat :: rs ll muta meta ? ll' ++ (list (tuple rs mutator meta))`
These are functions that each take four arguments:
- `rs`: The random seed being used for this run
- `ll`: The list being mutated
- `mutator`: The mutation function being run
- `meta`: A dictionary tracking the execution of Radamsa

And make a function call to `mutate-once`, adding the pattern name to the meta, and a 
continuation in continuation passing style. The continuation is the only thing that differs
between these three functions.

- `pat-once-dec` creates a continuation that returns the mutated input and a list containing a tuple of information (see above)
- `pat-many-dec` recurs until it fails a probability check against `remutate-probability`, defined in shared.scm, and returns the same basic continuation once it terminates
- `pat-burst` runs the same mutation function against the list at least once, potentially many times on the same spot, because mutation behavior is deterministic given a seed, returning the basic continuation on termination.

`mutate-once` takes five arguments:
- `rs`: the random seed being used for this run
- `ll`: The list being mutated
- `mutator`: The mutation function being run
- `meta`: The tracking dictionary
- `cont`: The continuation function passed in by one of the pattern functions

It looks like `ll` could also be a data stream of some sort as well, that returns bytes to
be modified.

The function interates to a point in the list and performs a mutation on it, then returns the 
application of the continuation on the modified list, the random seed, the mutator, and the meta.




## Exported Functions:

- `*patterns*`
- `default-patterns`
- `string->patterns`

## Constants defined:

- `max-burst-mutations`, 16 - this constant is never used by Radamsa.
- `default-patterns`, `"od,nd=2,bu"`

## Details
- `define (mutate-once rs ll mutator meta cont)`
- `define (pat-once-dec rs ll mutator meta)` <br />
    50 run mutate-once <br />
    51 update `meta` dictionary tracking information <br />
    53 lappend `ll` and `(list (tuple rs mutator meta))` to run the mutation <br />
- `define (pat-many-dec rs ll mutator meta)` <br />
    57 run mutate-once <br />
    58 update `meta` dictionary tracking information <br />
    60 define `remutate-probability`, defined in shared.scm
    61 check `remutate-probability`, recursively run `pat-many-dec` <br />
        lappend `ll` and `(list (tuple rs mutator meta))` <br />
        until it fails check `remutate-probability` <br />
    
- `define (pat-burst rs ll mutator meta)` <br />
    66 run mutate-once <br />
    67 update `meta` dictionary tracking information <br />
    69 re-define variables <br />
    70 define `remutate-probability`, defined in shared.scm <br />
    71 check if p <br />
- `define (priority->pattern pri)` <br />
    86 take a random element from `*pattern*` assign to `func` <br />
    88 cons cdr of `pri` list and `func` <br />
    90 print out cdr of `pri` is an unknown pattern <br />
- `define (mux-patterns ps)` <br />
    96 mergesort `ps` <br />
    97 walk over a `(map car ps)` list from left and compute `n` magic number <br />
    100 choose-pri in `ps` using number <br />
- `define (string->patterns str)`