# Program Flow

This document describes the overall function of Radamsa as a whole.

## Running Radamsa

Radamsa is run as a tool similar to the `cat` UNIX tool, where data is fed into Radamsa with a list of arguments and the fuzzed output(s) are printed to whatever the output is piped to.

Radamsa can be invoked with a large variety of command line rules. 
Of these rules, `version`, `help`, `about`, and `list` print some information and terminate succesfully. The rules `count`, `seed`, `mutations`, `patterns`, and `generators` all have default values which are used unless different values are supplied. The rest of the rules require a value be passed in to modify how radamsa is running.

When Radamsa is run, it initializes an instance of radamsa with the configuration information it is passed from the command line, which then kicks off processing on the input stream it is passed (stdin by default).

Once everything is initialized, Radamsa enters a mutation loop.

TODO: Verify what gen does. Almost certain that it is how data to be mutated is pulled in to the program

<details>
	<summary>Click to expand for loop variable breakdown - development purposes</summary>

	* `rs` - random seed
	* `muta` - a continuation that returns a continuation which executes a mutation and messes with some internals
	* `pat` - the mutation pattern being followed
	* `out` - something output related
	* `offset` - specified by the user or 1
	* `p` - 1 (appears to be a counter in the meta for the "nth" loop)
	* `cs` - checksums (checked to see if the same mutation has been performed before)
	* `left` - the loop variable, starts at the count specified by the command line arguments
</details>

During the main mutation loop, if we have reached the desired amount of outputs, the program terminates successfully. (If `left` is equal to 0)

Otherwise, the generator is called - this will be fleshed out later, but generator is how we get our input string `ll`.

Then, the pattern is executed on the linked list of bytes with the mutation continuation function, returning a mutated list.
	* There are three mutation patterns in radamsa, at the start of each execution, one of the three (or fewer if specified) is selected and run on the input. 

The mutated list is passed into the checksummer to verify uniqueness, and forces it to be a linked list if it is not already.
If the checksummer returns false, radamsa returns to the top of the main loop with no changes.
If the checksummer does not return false, if the offest is 1, then the mutated list is written to the output (stdout if none is specified), radamsa sleeps for a certain amount of time if it is specified in command line arguments, and then loops, incrementing `p` and decrementing `left`. Otherwise decrement the offset, increment p and loop.