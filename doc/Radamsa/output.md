# Documentation of `output.scm`

# Exported Functions
## output
Arguments:
- `ll`: a linked list of bytes to output
- `x`: a tuple describing the udp connection

If x is the shape of `(udp ip port socket)`, write packets to the socket until it's all sent
Else output `ll` to a file descriptor (`x`)

# output-to-fd
output :: (ll' ++ (#(rs mutator meta))) fd ? rs mutator meta (n-written || #f), handles port closing &/| flushing


Takes the mutated list and meta information and prints it to the file descriptor, and returns the meta information along with how many bytes were written or false.

## checksummer, dummy-checksummer, byte-list-checksummer
All of these check the checksum of bytes in a stream.

## stream-chunk
Given a buffer, a position, and a tail, if the position is 0, append the 0th element of buff to tail, otherwise get more data to put on the front

## dummy-output
Constructs the output, but does not write it.

## string->outputs
`string num -> linkedlist of output functions || #false`
Very complicated
