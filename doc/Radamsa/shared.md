# Documentation of `shared.scm`

# Exported Functions / Values
All of the defined constants below

## `selection->priority`
Arguments:
- `lst` a list that is either 1 or 2 elements long
Returns:
- false if the list is not 1 or 2 elements, the priority is not a number, or below zero. Otherwise converts priority from a string to a number and returns the list. If no priority is supplied, the priority is set to 1

## `choose`
Retrieves a reference to a function mapped to a command line argument

Arguments:
- `options`: a list with elements in the form of `(tuple "name" function-name "description")`
- `name`: a string that should match a name in `options`
Returns:
- A reference to the function mapped to `name`

## `choose-pri`
`((p . a) ...) n ? x`


Arguments:
- `l`: a list of the form `((priority . function) ...)`
- `n`: a priority value
Returns:
- If `n < priority` the function, otherwise it recurses on the tail of `l` and subtracts `priority` from `n`

## `car>`
Arguments: `a` and `b`
Returns true if the car of `a` is greater than the car of `b`, otherwise false

## `length<`
Arguments: `l`, a list, `n` a number
Returns true if the length of `l` is smaller than n, otherwise false.

## `stderr-probe`
"Simple runtime event tracing" - currently disabled in Radamsa, appears to be a diagnostic tool.

## `flush-bvecs`
- See README.md for my documentation of this function

## `inc`
Arguments: 
- `ff`: a dictionary of {key : count} pairs
- `key`: the key of the count to increment
Returns a dictionary with that key incremented, used to track mutations done in meta.

## `mutate-num`
Currently marked as a simple placeholder in Radamsa

Arguments:
-`rs`: the random seed being used this run
-`num`: the number being mutated
Returns, based on a random number between 0 and 11
- 0: `num + 1`
- 1: `num - 1`
- 2: `0`
- 3: `1`
- [4-6]: a random element from `interesting-numbers`
- 7: a random element from `interesting-numbers` plus `num`
- 8: a random element from `interesting-numbers` minus `num`
- 9: `num` minus a random number in `[0, num*2)`
- else: Add (or sometimes subtract) garbage to/from `num` 

# Defined Constants
- `avg-block-size` - 2048 - average block size when streaming data
- `min-block-size` - 256 - minimum preferred mutation block size
- `initial-ip` - initial max 1/n for basic patterns - used in `patterns.scm`
- `remutate-probability` - probability of each mutation being followed by a new one in nd
- `max-block-size` - twice the average block size
- `interesting-numbers` - a list of interesting numbers TODO: break this down more
- `exit-write-error` 1
- `exit-read-error` 2
