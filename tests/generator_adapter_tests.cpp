#include "catch.hpp"
#include "generator/generator_adapter.h"

struct input_chunk* oneCharChunkGenerator(const char* input, size_t size, struct radamsa_constants constants, radamsa_random rand) {
    struct input_chunk* prev = NULL;
    struct input_chunk* chunk;

    for (int i = size - 1; i >= 0; --i) {
        chunk = new input_chunk();
        chunk->chunkSize = 1;
        chunk->chunkPointer = new char[1];
        chunk->chunkPointer[0] = input[i];
        chunk->next = prev;
        prev = chunk;
    }

    return chunk;
}

TEST_CASE("Test that the adapter properly adapts a generating function", "[gen-adapter]") {
    Radamsa::Random rand(0);
    struct radamsa_constants c { 0 };
    Radamsa::GeneratorAdapter gen(oneCharChunkGenerator, "occg", 0, c, rand);

    std::deque<std::string> out = gen.generate("abcde");
    REQUIRE(out.size() == 5);
    REQUIRE(out.front() == "a");
    REQUIRE(out.back() == "e");
}