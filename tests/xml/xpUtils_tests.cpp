#include "catch.hpp"
#include "mutation/xml/xpUtils.h"

using namespace Radamsa;

TEST_CASE("Inividual Nodes") {
	std::map<std::string, std::string> attributes = std::map<std::string, std::string>();
	attributes.insert(std::pair<std::string const, std::string>("thing1", "\"melon\""));
	attributes.insert(std::pair<std::string const, std::string>("thing2", "\"water\""));

	SECTION("byte") {
		std::shared_ptr<XpNode> bytes = std::make_shared<ByteNode>("This is just text the parser runs into");

		REQUIRE(bytes->getType() == Radamsa::NodeType::Byte);
		REQUIRE(bytes->toString() == "This is just text the parser runs into");
	}

	SECTION("open") {
		std::shared_ptr<XpNode> open = std::make_shared<OpenNode>("fruits", attributes);

		REQUIRE(open->getType() == Radamsa::NodeType::Open);
		REQUIRE(open->toString() == "<fruits thing1=\"melon\" thing2=\"water\">");
	}

	SECTION("open-single") {
		std::shared_ptr<XpNode> openSingle = std::make_shared<OpenSingleNode>("fruits", attributes);

		REQUIRE(openSingle->getType() == Radamsa::NodeType::OpenSingle);
		REQUIRE(openSingle->toString() == "<fruits thing1=\"melon\" thing2=\"water\" />");
	}

	SECTION("close") {
		std::shared_ptr<XpNode> close = std::make_shared<CloseNode>("fruits");

		REQUIRE(close->getType() == Radamsa::NodeType::Close);
		REQUIRE(close->toString() == "</fruits>");
	}
	
	SECTION("tag") {
		std::vector<std::shared_ptr<XpNode>> xp;
		std::shared_ptr<XpNode> nl = std::make_shared<ByteNode>("\n");
		xp.push_back(nl);
		std::shared_ptr<XpNode> tag = std::make_shared<TagNode>("fruits", attributes, xp);

		REQUIRE(tag->getType() == Radamsa::NodeType::Tag);
		REQUIRE(tag->toString() == "<fruits thing1=\"melon\" thing2=\"water\">\n</fruits>");
	}
	
	SECTION("plus") {
		std::vector<std::shared_ptr<XpNode>> xp1;
		std::vector<std::shared_ptr<XpNode>> xp2;

		std::shared_ptr<XpNode> text = std::make_shared <ByteNode>("fruits are good fruits\n");
		xp1.push_back(text);

		std::shared_ptr<XpNode> tag = std::make_shared <TagNode>("fruits", attributes, xp1);
		xp2.push_back(tag);

		std::shared_ptr<XpNode> plus = std::make_shared<PlusNode>(xp1, xp2);

		REQUIRE(plus->getType() == Radamsa::NodeType::Plus);
		REQUIRE(plus->toString() == "fruits are good fruits\n<fruits thing1=\"melon\" thing2=\"water\">fruits are good fruits\n</fruits>");
	}

	SECTION("open-close") {
		std::string in = "<emptyWithAttribute thing=\"boxcar\" />";

		XpUtils utils = XpUtils();

		std::vector<std::shared_ptr<XpNode>> nodes = std::vector<std::shared_ptr<XpNode>>();
		std::vector<Tag> tags = std::vector<Tag>();

		utils.XpProcess(in, nodes, tags);

		REQUIRE(utils.nodesToString(nodes) == in);
	}
}

TEST_CASE("Basic full tree") {
	std::string full = "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2=\"melon\">Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>";
	std::map<std::string, std::string> empty;

	std::vector<std::shared_ptr<XpNode>> inner1;
	std::vector<std::shared_ptr<XpNode>> text;
	std::vector<std::shared_ptr<XpNode>> nestedNodes1;
	std::vector<std::shared_ptr<XpNode>> nestedNodes2;
	std::vector<std::shared_ptr<XpNode>> nestedText;

	text.push_back(std::make_shared<ByteNode>("this is some text"));

	nestedText.push_back(std::make_shared<ByteNode>("this is some very nested text"));
	nestedNodes2.push_back(std::make_shared<ByteNode>("\n"));
	nestedNodes2.push_back(std::make_shared<TagNode>("nested3", empty, nestedText));
	nestedNodes2.push_back(std::make_shared<ByteNode>("\n"));

	nestedNodes1.push_back(std::make_shared<ByteNode>("\n"));
	nestedNodes1.push_back(std::make_shared<TagNode>("nested2", empty, nestedNodes2));
	nestedNodes1.push_back(std::make_shared<ByteNode>("\n"));

	inner1.push_back(std::make_shared<ByteNode>("\n"));
	inner1.push_back(std::make_shared<TagNode>("text", empty, text));
	inner1.push_back(std::make_shared<ByteNode>("\n"));
	inner1.push_back(std::make_shared<TagNode>("nested1", empty, nestedNodes1));
	inner1.push_back(std::make_shared<ByteNode>("\n"));

	std::vector<std::shared_ptr<XpNode>> inner2;

	std::map<std::string, std::string> boxcar;
	boxcar.insert(std::pair<std::string const, std::string>("thing", "\"boxcar\""));
	std::map<std::string, std::string> watermelon;
	watermelon.insert(std::pair<std::string const, std::string>("thing1", "\"water\""));
	watermelon.insert(std::pair<std::string const, std::string>("thing2", "\"melon\""));
	std::vector<std::shared_ptr<XpNode>> niceOut;
	niceOut.push_back(std::make_shared<ByteNode>("Boy it sure is nice out today"));

	inner2.push_back(std::make_shared<ByteNode>("\n"));
	inner2.push_back(std::make_shared<OpenSingleNode>("empty", empty));
	inner2.push_back(std::make_shared<ByteNode>("\n"));
	inner2.push_back(std::make_shared<OpenSingleNode>("emptyWithAttribute", boxcar));
	inner2.push_back(std::make_shared<ByteNode>("\n"));
	inner2.push_back(std::make_shared<TagNode>("tagWithAttribute", watermelon, niceOut));
	inner2.push_back(std::make_shared<ByteNode>("\n"));

	std::shared_ptr<XpNode> normalTag1 = std::make_shared<TagNode>("normalTag", empty, inner1);
	std::shared_ptr<XpNode> normalTag2 = std::make_shared<TagNode>("normalTag", empty, inner2);
	std::shared_ptr<XpNode> abnormalTag = std::make_shared<OpenNode>("abnormalTag", empty);

	std::vector<std::shared_ptr<XpNode>> depth1;
	depth1.push_back(std::make_shared<ByteNode>("\n"));
	depth1.push_back(normalTag1);
	depth1.push_back(std::make_shared<ByteNode>("\n"));
	depth1.push_back(normalTag2);
	depth1.push_back(std::make_shared<ByteNode>("\n"));
	depth1.push_back(abnormalTag);
	depth1.push_back(std::make_shared<ByteNode>("\n"));

	std::shared_ptr<XpNode> enclosingTag = std::make_shared<TagNode>("enclosingTag", std::map<std::string, std::string>(), depth1);

	REQUIRE(enclosingTag->toString() == full);
	XpUtils utils = XpUtils();	

	SECTION("TEST PARSE") {
		std::vector<std::shared_ptr<XpNode>> nodes = std::vector<std::shared_ptr<XpNode>>();
		std::vector<Tag> tags = std::vector<Tag>();
		utils.XpProcess(full, nodes, tags);

		std::vector<Tag> expectedTags = std::vector<Tag>();
		expectedTags.push_back(Tag("enclosingTag", "open-full", empty));
		expectedTags.push_back(Tag("normalTag", "open-full", empty));
		expectedTags.push_back(Tag("text", "open-full", empty));
		expectedTags.push_back(Tag("nested1", "open-full", empty));
		expectedTags.push_back(Tag("nested2", "open-full", empty));
		expectedTags.push_back(Tag("nested3", "open-full", empty));
		expectedTags.push_back(Tag("empty", "open-single", empty));
		expectedTags.push_back(Tag("emptyWithAttribute", "open-single", boxcar));
		expectedTags.push_back(Tag("tagWithAttribute", "open-full", watermelon));
		expectedTags.push_back(Tag("abnormalTag", "open-only", empty));

		//bool check = nodes[0]->equals(enclosingTag);
		//REQUIRE(check);
		std::string back = nodes[0]->toString();
		if (expectedTags.size() == tags.size()) {
			for (int i = 0; i < tags.size(); i++) {
				Tag a = expectedTags[i];
				const Tag b = tags[i];
				REQUIRE(a.equals(b));
			}
		}
		else {
			REQUIRE(false);
		}
		REQUIRE(back == full);
	}
}