#include "catch.hpp"
#include "../mock/mock_random.h"
#include "mutation/tree/treeDeleteMutation.h"

TEST_CASE("No sublists") {
	std::string input = "spaghetti sauce";
	MockRandom randy = MockRandom();
	Radamsa::TreeDeleteMutation m = Radamsa::TreeDeleteMutation(10, {}, randy);

	std::string out = m.mutate(input);
	REQUIRE(out == input);
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("One sublist nuked") {
	std::string input = "spaghetti (marinara) sauce";
	MockRandom randy = MockRandom();
	Radamsa::TreeDeleteMutation m = Radamsa::TreeDeleteMutation(10, {}, randy);
	randy.expectedNext(0);

	std::string out = m.mutate(input);
	REQUIRE(out == "spaghetti  sauce");
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("Nested sublists nuked") {
	std::string input = "spaghetti (mari<nara>) sauce";
	MockRandom randy = MockRandom();
	Radamsa::TreeDeleteMutation m = Radamsa::TreeDeleteMutation(10, {}, randy);
	randy.expectedNext(0);
	randy.expectedNext(1);

	std::string out = m.mutate(input);
	REQUIRE(out == "spaghetti (mari) sauce");

	out = m.mutate(input);
	REQUIRE(out == "spaghetti  sauce");
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("Random sublist nuked") {
	std::string input = "((<{}>)[]\"\"\'\')";
	MockRandom randy = MockRandom();
	Radamsa::TreeDeleteMutation m = Radamsa::TreeDeleteMutation(10, {}, randy);
	randy.expectedNext(0);
	randy.expectedNext(5);

	std::string out = m.mutate(input);
	REQUIRE(out == "((<{}>)[]\"\")");

	out = m.mutate(input);
	REQUIRE(out == "([]\"\"\'\')");
	REQUIRE(randy.allExpectedCallsMade());
}