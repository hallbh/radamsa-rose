#include "catch.hpp"
#include "../mock/mock_random.h"
#include "mutation/tree/treeSwapOneMutation.h"
#include "mutation/tree/treeSwapTwoMutation.h"

TEST_CASE("Swap overwrites swapee") {
	std::string input = "This is a <sentence> with many (sublists)";
	MockRandom randy = MockRandom();
	Radamsa::TreeSwapOneMutation m = Radamsa::TreeSwapOneMutation(10, {}, randy);

	std::string out = m.mutate(input);
	REQUIRE(out == "This is a (sublists) with many (sublists)");
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("Swap overwrites swapee 2") {
	std::string input = "This is a <sentence> with {} many (sublists)";
	MockRandom randy = MockRandom();
	Radamsa::TreeSwapOneMutation m = Radamsa::TreeSwapOneMutation(10, {}, randy);

	std::string out = m.mutate(input);
	REQUIRE(out == "This is a <sentence> with (sublists) many (sublists)");
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("Swap swap") {
	std::string input = "This is a <sentence> with many (sublists)";
	MockRandom randy = MockRandom();
	Radamsa::TreeSwapTwoMutation m = Radamsa::TreeSwapTwoMutation(10, {}, randy);

	std::string out = m.mutate(input);
	REQUIRE(out == "This is a (sublists) with many <sentence>");
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("Swap swap 2") {
	std::string input = "This is a <sentence> with {} many (sublists)";
	MockRandom randy = MockRandom();
	Radamsa::TreeSwapTwoMutation m = Radamsa::TreeSwapTwoMutation(10, {}, randy);

	std::string out = m.mutate(input);
	REQUIRE(out == "This is a <sentence> with (sublists) many {}");
	REQUIRE(randy.allExpectedCallsMade());
}