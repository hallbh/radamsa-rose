#include "catch.hpp"
#include "mock/mock_random.h"
#include "mutation/numMutation.h"

using namespace Radamsa;

TEST_CASE("Num Mutation Construction") {
	struct radamsa_constants constants = { 0 };
	constants.maxScore = 10;
	Random mock = MockRandom();

	NumMutation m  = NumMutation( 12, constants, mock );

	REQUIRE(m.getName() == "num");
	REQUIRE(m.score == 10);
	REQUIRE(m.priority == 12);
}

TEST_CASE("Copy Range") {
	struct radamsa_constants constants = { 0 };
	Random mock = MockRandom();
	NumMutation m = NumMutation(12, constants, mock);

	std::string a = "hello I am a string";
	std::string b = "am a string";
	std::string c = "but I am not";

	std::string expected = "hello I but I am not";
	std::string actual = m.copyRange(a, b, c);

	REQUIRE(actual == expected);
}

TEST_CASE("Digit Val") {
	struct radamsa_constants constants = { 0 };
	Random mock = MockRandom();
	NumMutation m = NumMutation(12, constants, mock);

	REQUIRE(m.digitVal('0') == 0);
	REQUIRE(m.digitVal('1') == 1);
	REQUIRE(m.digitVal('2') == 2);
	REQUIRE(m.digitVal('3') == 3);
	REQUIRE(m.digitVal('4') == 4);
	REQUIRE(m.digitVal('5') == 5);
	REQUIRE(m.digitVal('6') == 6);
	REQUIRE(m.digitVal('7') == 7);
	REQUIRE(m.digitVal('8') == 8);
	REQUIRE(m.digitVal('9') == 9);
}

TEST_CASE("Get Num") {
	struct radamsa_constants constants = { 0 };
	Random mock = MockRandom();
	NumMutation m = NumMutation(12, constants, mock);

	SECTION("num") {
		std::string a = "12345 I am a string";
		
		int out = m.getNum(a);
		int expected = 12345;

		REQUIRE(out == expected);
		REQUIRE(a == " I am a string");
	}

	SECTION("1") {
		std::string a = "1";
		REQUIRE(m.getNum(a) == 1);
		REQUIRE(a == "");
	}

	SECTION("not") {
		std::string a = "I am a string";
		int out = m.getNum(a);
		REQUIRE(out == -1);
	}
}

TEST_CASE("Mutate Num") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	NumMutation m = NumMutation(12, constants, mock);

	SECTION("0 - 3") {
		mock.expectedNext(0);
		mock.expectedNext(1);
		mock.expectedNext(2); 
		mock.expectedNext(3);

		REQUIRE(m.mutateNum(1234) == "1235");
		REQUIRE(m.mutateNum(1234) == "1233");
		REQUIRE(m.mutateNum(1234) == "0");
		REQUIRE(m.mutateNum(1234) == "1");
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("4 - 8") {
		mock.expectedNext(4);
		mock.expectedNext(3);

		mock.expectedNext(5);
		mock.expectedNext(32);

		mock.expectedNext(7);
		mock.expectedNext(3);

		mock.expectedNext(8);
		mock.expectedNext(32);

		REQUIRE(m.mutateNum(1234) == "170141183460469231731687303715884105727");
		REQUIRE(m.mutateNum(1234) == "3");
		REQUIRE(m.mutateNum(1234) == "170141183460469231731687303715884106961");
		REQUIRE(m.mutateNum(1234) == "-1231");
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("9") {
		mock.expectedNext(9);
		mock.expectedNext(832);

		REQUIRE(m.mutateNum(1234) == "402");
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("10 - 11") {
		mock.expectedNext(10); // I don't care about these values -- just making sure the right # of calls
		mock.expectedNext(10);
		mock.expectedNext(10);
		mock.expectedNext(1);

		mock.expectedNext(11);
		mock.expectedNext(11);
		mock.expectedNext(11);
		mock.expectedNext(0);

		std::string actual1 = m.mutateNum(1234);
		std::string actual2 = m.mutateNum(1234);

		REQUIRE(actual1 == "1244");
		REQUIRE(actual2 == "-1223");
		REQUIRE(mock.allExpectedCallsMade());
	}
}

// > echo "spaghetti 123456" | ./radamsa.exe -p od -m num
TEST_CASE("Mutate a Num") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	NumMutation m = NumMutation(10, constants, mock);

	std::string input = "spaghetti 123456";

	// Given random values expect results:
	SECTION("spaghetti -123455") {
		mock.expectedNext(0);
		mock.expectedNext(8);
		mock.expectedNext(30);
		
		m.mutateANum(input, 0);

		REQUIRE(mock.allExpectedCallsMade());
		REQUIRE(input == "spaghetti -123455");
	}

	SECTION("spaghetti 123944") {
		mock.expectedNext(0);
		mock.expectedNext(11);
		mock.expectedNext(67);
		mock.expectedNext(488);
		mock.expectedNext(1);

		m.mutateANum(input, 0);

		REQUIRE(mock.allExpectedCallsMade());
		REQUIRE(input == "spaghetti 123944");
	}

	SECTION("spaghetti 127") {
		mock.expectedNext(0);
		mock.expectedNext(6);
		mock.expectedNext(27);

		m.mutateANum(input, 0);

		REQUIRE(mock.allExpectedCallsMade());
		REQUIRE(input == "spaghetti 127");
	}

	SECTION("spaghetti 1") {
		mock.expectedNext(0);
		mock.expectedNext(3);

		m.mutateANum(input, 0);

		REQUIRE(mock.allExpectedCallsMade());
		REQUIRE(input == "spaghetti 1");
	}

	SECTION("spaghetti -15081812954628934920") {
		mock.expectedNext(0);
		mock.expectedNext(11);
		mock.expectedNext(52);
		mock.expectedNext(80420263);
		mock.expectedNext(0);

		m.mutateANum(input, 0);

		REQUIRE(mock.allExpectedCallsMade());
		REQUIRE(input == "spaghetti -80296807");
	}

	SECTION("spaghetti 170141183460469231731687303715884105729") {
		mock.expectedNext(0);
		mock.expectedNext(4);
		mock.expectedNext(5);

		m.mutateANum(input, 0);

		REQUIRE(mock.allExpectedCallsMade());
		REQUIRE(input == "spaghetti 170141183460469231731687303715884105729");
	}
}

TEST_CASE("Mutation Full Run") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	NumMutation m = NumMutation(10, constants, mock);

	std::string input = "spaghetti 123456";
	std::string sauce = "spaghetti sauce";

	SECTION("spaghetti 170141183460469231731687303715884105729") {
		mock.expectedNext(0);
		mock.expectedNext(4);
		mock.expectedNext(5);

		std::string out = m.mutate(input);

		REQUIRE(mock.allExpectedCallsMade());
		REQUIRE(out == "spaghetti 170141183460469231731687303715884105729");
		REQUIRE(m.nextDelta() == 2);
	}

	SECTION("spaghetti sauce") {
		mock.expectedNext(0);
		mock.expectedNext(7);

		std::string out = m.mutate(sauce);

		REQUIRE(mock.allExpectedCallsMade());
		REQUIRE(out == "spaghetti sauce");
		REQUIRE(m.nextDelta() == 0);
	}

	SECTION("spaghetti sauce 2") {
		mock.expectedNext(0);
		mock.expectedNext(0);

		std::string out = m.mutate(sauce);

		REQUIRE(mock.allExpectedCallsMade());
		REQUIRE(out == "spaghetti sauce");
		REQUIRE(m.nextDelta() == -1);
	}
}