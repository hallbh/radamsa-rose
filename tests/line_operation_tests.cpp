#include "catch.hpp"
#include "mutation/line/line_operation.h"
#include "mock/mock_random.h"

using namespace Radamsa;

/*
 * DELETE LINE
 */
TEST_CASE("Test line deletion works on one line", "[mutation, line, delete]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	SECTION("Test deletion of valid line") {
		lines.push_back("abcde");
		CHECK(lines.size() == 1);

		deleteLineOperation(lines, rand);
		REQUIRE(lines.size() == 0);
	}

	SECTION("Test deletion of empty line") {
		lines.push_back("");
		CHECK(lines.size() == 1);

		deleteLineOperation(lines, rand);
		REQUIRE(lines.size() == 0);
	}

	delete rand;
}

TEST_CASE("Test line deletion does not throw error on empty lines", "[mutation, line, delete]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	deleteLineOperation(lines, rand);
	REQUIRE(lines.size() == 0);

	delete rand;
}

TEST_CASE("Test line deletion replicates radamsa behavior with 5 valid lines", "[mutation, line, delete]") {
	std::vector<std::string> lines;
	MockRandom rand;
	lines.push_back("a\n");
	lines.push_back("b\n");
	lines.push_back("c\n");
	lines.push_back("d\n");
	lines.push_back("e");
	CHECK(lines.size() == 5);

	// radamsa -p od -m ld -s 0 in.txt (in.txt has the lines described above)
	// Should delete the e line
	SECTION("Deletion with seed 0") {
		rand.expectedNext(4); // Random index of 4 when this is called with the above line
		deleteLineOperation(lines, &rand);
		REQUIRE(lines.size() == 4);
		REQUIRE(lines.back() == "d\n");
	}

	// radamsa -p od -m ld -s 232323 in.txt (in.txt has the lines described above)
	// Should delete the a line
	SECTION("Deletion with seed 232323") {
		rand.expectedNext(0); // Random index of 0 when this is called with the above line
		deleteLineOperation(lines, &rand);
		REQUIRE(lines.size() == 4);
		REQUIRE(lines.back() == "e");
		REQUIRE(lines.front() == "b\n");
	}

	REQUIRE(rand.allExpectedCallsMade());
}

/*
 * DELETE LINE SEQUENCE
 */
// Delete sequence should not delete with one line
TEST_CASE("Test line sequence deletion works on one line", "[mutation, line, sequence_delete]") {
	std::vector<std::string> lines;

	SECTION("Test deletion of valid line") {
		Random* rand = new Random(69);
		lines.push_back("abcde");
		CHECK(lines.size() == 1);

		deleteLineSequenceOperation(lines, rand);
		REQUIRE(lines.size() == 1);
		delete rand;

		rand = new Random(0);
		deleteLineSequenceOperation(lines, rand);
		REQUIRE(lines.size() == 1);
		delete rand;

		rand = new Random(42096);
		deleteLineSequenceOperation(lines, rand);
		REQUIRE(lines.size() == 1);
		delete rand;
	}

	SECTION("Test deletion of empty line") {
		Random* rand = new Random(69);
		lines.push_back("");
		CHECK(lines.size() == 1);

		deleteLineSequenceOperation(lines, rand);
		REQUIRE(lines.size() == 1);
		delete rand;
	}

}

TEST_CASE("Test line sequence deletion does not throw error on empty lines", "[mutation, line, sequence_delete]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	deleteLineSequenceOperation(lines, rand);
	REQUIRE(lines.size() == 0);

	delete rand;
}

TEST_CASE("Test line sequence deletion replicates radamsa behavior with 5 valid lines", "[mutation, line, sequence_delete]") {
	std::vector<std::string> lines;
	MockRandom rand;
	lines.push_back("a\n");
	lines.push_back("b\n");
	lines.push_back("c\n");
	lines.push_back("d\n");
	lines.push_back("e");
	CHECK(lines.size() == 5);

	// radamsa -p od -m lds -s 1 in.txt (in.txt has the lines described above)
	// Should delete the lines b and c
	SECTION("Deletion with seed 1") {
		rand.expectedNext(1); // Random index of 1 when this is called twice with the above line
		rand.expectedNext(2); // Random length of 2 when this is called twice with the above line
		deleteLineSequenceOperation(lines, &rand);
		REQUIRE(lines.size() == 3);
		REQUIRE(lines[0] == "a\n");
		REQUIRE(lines[1] == "d\n");
		REQUIRE(lines[2] == "e");
	}

	// radamsa -p od -m lds -s 42 in.txt (in.txt has the lines described above)
	// Should not delete a line
	SECTION("Deletion with seed 42") {
		rand.expectedNext(4); // Random index of 4 when this is called with the above line
		rand.expectedNext(0); // Length of 0
		deleteLineSequenceOperation(lines, &rand);
		REQUIRE(lines.size() == 5);
		REQUIRE(lines.front() == "a\n");
		REQUIRE(lines.back() == "e");
	}

	REQUIRE(rand.allExpectedCallsMade());
}

/*
 * DUPLICATE LINE
 */
TEST_CASE("Test line duplication works on one line", "[mutation, line, duplicate]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	SECTION("Test duplication of valid line") {
		lines.push_back("abcde");
		CHECK(lines.size() == 1);

		duplicateLineOperation(lines, rand);
		REQUIRE(lines.size() == 2);
		REQUIRE(lines[0] == "abcde");
		REQUIRE(lines[1] == "abcde");
	}

	SECTION("Test duplication of empty line") {
		lines.push_back("");
		CHECK(lines.size() == 1);

		duplicateLineOperation(lines, rand);
		REQUIRE(lines.size() == 2);
		REQUIRE(lines[0].empty());
		REQUIRE(lines[1].empty());
	}

	delete rand;
}

TEST_CASE("Test line duplication does not throw error on empty lines", "[mutation, line, duplicate]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	duplicateLineOperation(lines, rand);
	REQUIRE(lines.size() == 0);

	delete rand;
}

TEST_CASE("Test line duplication replicates radamsa behavior with 5 valid lines", "[mutation, line, duplicate]") {
	std::vector<std::string> lines;
	lines.push_back("a\n");
	lines.push_back("b\n");
	lines.push_back("c\n");
	lines.push_back("d\n");
	lines.push_back("e");
	CHECK(lines.size() == 5);

	// radamsa -p od -m ld -s 0 in.txt (in.txt has the lines described above)
	// Should duplicate the e line
	SECTION("Duplication with seed 0") {
		MockRandom rand;
		rand.expectedNext(4); // Random index of 4 when this is called with the above line
		duplicateLineOperation(lines, &rand);
		REQUIRE(lines.size() == 6);
		REQUIRE(lines[4] == lines[5]);
	}

	// radamsa -p od -m ld -s 232323 in.txt (in.txt has the lines described above)
	// Should duplicate the c line
	SECTION("Duplicate with seed 909090") {
		MockRandom rand;
		rand.expectedNext(2); // Random index of 0 when this is called with the above line
		duplicateLineOperation(lines, &rand);
		REQUIRE(lines.size() == 6);
		REQUIRE(lines.front() == "a\n");
		REQUIRE(lines[2] == lines[3]);
		REQUIRE(lines.back() == "e");
	}
}

/*
 * CLONE LINE
 */
TEST_CASE("Test line cloning works on one line", "[mutation, line, clone]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	SECTION("Test clone of valid line") {
		lines.push_back("abcde");
		CHECK(lines.size() == 1);

		cloneLineOperation(lines, rand);
		REQUIRE(lines.size() == 2);
		REQUIRE(lines[0] == "abcde");
		REQUIRE(lines[1] == "abcde");
	}

	SECTION("Test cloning of empty line") {
		lines.push_back("");
		CHECK(lines.size() == 1);

		cloneLineOperation(lines, rand);
		REQUIRE(lines.size() == 2);
		REQUIRE(lines[0].empty());
		REQUIRE(lines[1].empty());
	}

	delete rand;
}

TEST_CASE("Test line cloning does not throw error on empty lines", "[mutation, line, clone]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	cloneLineOperation(lines, rand);
	REQUIRE(lines.size() == 0);

	delete rand;
}

TEST_CASE("Test line cloning replicates radamsa behavior with 5 valid lines", "[mutation, line, clone]") {
	std::vector<std::string> lines;
	MockRandom rand;
	lines.push_back("a\n");
	lines.push_back("b\n");
	lines.push_back("c\n");
	lines.push_back("d\n");
	lines.push_back("e");
	CHECK(lines.size() == 5);

	// radamsa -p od -m li -s 0 in.txt (in.txt has the lines described above)
	// Should clone e and place the clone before d
	SECTION("Cloning with seed 0") {
		rand.expectedNext(4); // Random index of 4 when this is called with the above line
		rand.expectedNext(3); // Random index of 3 when this is called with the above line
		cloneLineOperation(lines, &rand);
		REQUIRE(lines.size() == 6);
		REQUIRE(lines[3] == lines[5]);
	}

	// radamsa -p od -m li -s 42069 in.txt (in.txt has the lines described above)
	// Should clone d in a manner similar to duplication
	SECTION("Cloning with seed 42069") {
		rand.expectedNext(3); // Random index of 0 when this is called with the above line
		rand.expectedNext(3);
		cloneLineOperation(lines, &rand);
		REQUIRE(lines.size() == 6);
		REQUIRE(lines.front() == "a\n");
		REQUIRE(lines[3] == lines[4]);
		REQUIRE(lines.back() == "e");
	}

	// radamsa -p od -m li -s 4 in.txt (in.txt has the lines described above)
	// THIS IS WEIRD. REPLACES d WITH c

	// radamsa -p od -m li -s 9 in.txt (in.txt has the lines described above)
	// Should clone the b to be first
	SECTION("Cloning with seed 9") {
		rand.expectedNext(1); // Random index of 0 when this is called with the above line
		rand.expectedNext(0);
		cloneLineOperation(lines, &rand);
		REQUIRE(lines.size() == 6);
		REQUIRE(lines.front() == "b\n");
		REQUIRE(lines[0] == lines[2]);
		REQUIRE(lines[1] == "a\n");
		REQUIRE(lines.back() == "e");
	}

	REQUIRE(rand.allExpectedCallsMade());
}

/*
 * REPEAT LINE
 */
TEST_CASE("Test line repetition works on one line", "[mutation, line, repeat]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	SECTION("Test repetition of valid line") {
		lines.push_back("abcde");
		CHECK(lines.size() == 1);

		repeatLineOperation(lines, rand);
		REQUIRE(lines.size() > 2);
	}

	SECTION("Test cloning of empty line") {
		lines.push_back("");
		CHECK(lines.size() == 1);

		repeatLineOperation(lines, rand);
		REQUIRE(lines.size() > 2);
	}

	delete rand;
}

TEST_CASE("Test line repetion does not throw error on empty lines", "[mutation, line, repeat]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	repeatLineOperation(lines, rand);
	REQUIRE(lines.size() == 0);

	delete rand;
}


TEST_CASE("Test line repetition replicates radamsa behavior with 5 valid lines", "[mutation, line, repeat]") {
	std::vector<std::string> lines;
	MockRandom rand;
	lines.push_back("a\n");
	lines.push_back("b\n");
	lines.push_back("c\n");
	lines.push_back("d\n");
	lines.push_back("e");
	CHECK(lines.size() == 5);

	// radamsa -p od -m lr -s 0 in.txt (in.txt has the lines described above)
	// Should repeat e 63 times (resulting in 64 total e's)
	SECTION("Cloning with seed 0") {
		rand.expectedNext(4); // Random index of 4 when this is called with the above line
		rand.expectedNext(63); // Random repetitions of 63 when this is called with the above line
		repeatLineOperation(lines, &rand);
		REQUIRE(lines.size() == 68);
		for (int i = 0; i < 64; i++) {
			REQUIRE(lines.at(i + 4) == "e");
		}
	}

	// radamsa -p od -m li -s 102030 in.txt (in.txt has the lines described above)
	// Should clone d in a manner similar to duplication
	SECTION("Repeat with seed 102030") {
		rand.expectedNext(2); // Random index of 2 when this is called with the above line
		rand.expectedNext(106); // 106 repetitions
		repeatLineOperation(lines, &rand);
		REQUIRE(lines.size() == 111);
		REQUIRE(lines.front() == "a\n");
		for (int i = 0; i < 106; i++) {
			REQUIRE(lines.at(i + 2) == "c\n");
		}
		REQUIRE(lines.back() == "e");
	}

	REQUIRE(rand.allExpectedCallsMade());
}

/*
 * SWAP LINES
 */
TEST_CASE("Test swap line will not error on one line", "[mutation, line, swap]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	lines.push_back("abcde");
	CHECK(lines.size() == 1);

	swapLineOperation(lines, rand);
	REQUIRE(lines.size() == 1);
	REQUIRE(lines.front() == "abcde");

	delete rand;
}

TEST_CASE("Test line swap does not throw error on empty lines", "[mutation, line, swap]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	swapLineOperation(lines, rand);
	REQUIRE(lines.size() == 0);

	delete rand;
}

TEST_CASE("Test line swap works with two lines", "[mutation, line, swap]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);
	lines.push_back("1\n");
	lines.push_back("");
	CHECK(lines.back().empty());

	swapLineOperation(lines, rand);
	REQUIRE(lines.size() == 2);
	REQUIRE(lines.front().empty());
	REQUIRE(lines.back() == "1\n");

	delete rand;
}

TEST_CASE("Test line swap replicates radamsa behavior with 5 valid lines", "[mutation, line, swap]") {
	std::vector<std::string> lines;
	MockRandom rand;
	lines.push_back("a\n");
	lines.push_back("b\n");
	lines.push_back("c\n");
	lines.push_back("d\n");
	lines.push_back("e");
	CHECK(lines.size() == 5);

	// radamsa -p od -m ls -s 0 in.txt (in.txt has the lines described above)
	// Should swap the e and d line
	SECTION("Swap with seed 0") {
		rand.expectedNext(3); // Random index of 4 when this is called with the above line
		swapLineOperation(lines, &rand);
		REQUIRE(lines.size() == 5);
		REQUIRE(lines[3] == "e");
		REQUIRE(lines.back() == "d\n");
	}

	// radamsa -p od -m ld -s 232323 in.txt (in.txt has the lines described above)
	// Should swap the d and c line
	SECTION("Duplicate with seed 100100") {
		rand.expectedNext(2); // Random index of 0 when this is called with the above line
		swapLineOperation(lines, &rand);
		REQUIRE(lines.size() == 5);
		REQUIRE(lines.front() == "a\n");
		REQUIRE(lines[2] == "d\n");
		REQUIRE(lines[3] == "c\n");
		REQUIRE(lines.back() == "e");
	}

	REQUIRE(rand.allExpectedCallsMade());
}

/*
 * PERMUTE LINES
 */
TEST_CASE("Test permute lines will nop on less than three lines", "[mutation, line, permute]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);

	SECTION("No lines") {
		permuteLineOperation(lines, rand);
		CHECK(lines.empty());
	}

	SECTION("1 Non-empty line") {
		lines.push_back("abcde");
		CHECK(lines.size() == 1);

		permuteLineOperation(lines, rand);
		REQUIRE(lines.size() == 1);
		REQUIRE(lines.front() == "abcde");
	}

	SECTION("2 lines") {
		lines.push_back("abcde");
		lines.push_back("");
		CHECK(lines.size() == 2);

		permuteLineOperation(lines, rand);
		REQUIRE(lines.size() == 2);
		REQUIRE(lines.front() == "abcde");
		REQUIRE(lines.back().empty());
	}

	delete rand;
}

TEST_CASE("Test line permutation works with three lines", "[mutation][line][permute-sp]") {
	std::vector<std::string> lines;
	Random* rand = new Random(3);
	lines.push_back("1\n");
	lines.push_back("");
	lines.push_back("Hey Arnold");

	permuteLineOperation(lines, rand);
	REQUIRE(lines.size() == 3);
	REQUIRE(lines.front().empty());
	REQUIRE(lines.back() == "Hey Arnold");

	delete rand;
}

TEST_CASE("Test line permutation replicates radamsa behavior with 5 valid lines", "[mutation, line, permute]") {
	// Cannot test this individually, as I do not know the state of random when random-permutation gets called and uses uncons
	// This must be tested with the whole system
}

/*
 * INSERT LINE (STATEFUL)
 */
TEST_CASE("Test stateful line insert will not error on one line", "[mutation, line, insert]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);
	LineState state(2, 4, *rand);

	lines.push_back("abcde");
	CHECK(lines.size() == 1);

	insertLineOperation(lines, state, rand);
	REQUIRE(lines.size() == 2);
	REQUIRE(lines.front() == "abcde");
	REQUIRE(lines.back() == "abcde");

	delete rand;
}

TEST_CASE("Test line insert does not throw error on empty lines", "[mutation, line, insert]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);
	LineState state(1, 1, *rand);

	insertLineOperation(lines, state, rand);
	REQUIRE(lines.size() == 0);

	delete rand;
}

TEST_CASE("Test line insert replicates radamsa behavior with 5 valid lines", "[mutation, line, insert]") {
	std::vector<std::string> lines;
	MockRandom rand;
	LineState state(10, 10 << 1, rand);
	lines.push_back("a\n");
	lines.push_back("b\n");
	lines.push_back("c\n");
	lines.push_back("d\n");
	lines.push_back("e");
	CHECK(lines.size() == 5);

	// radamsa -p od -m lis -s 0 in.txt (in.txt has the lines described above)
	// Should insert the e after the b line
	SECTION("Insert stateful with seed 0") {
		rand.expectedNext(4); // Store
		rand.expectedNext(3); // Store
		rand.expectedNext(4); // Store
		rand.expectedNext(3); // Store
		rand.expectedNext(1); // Store
		rand.expectedNext(2); // Store
		rand.expectedNext(0); // Store
		rand.expectedNext(2); // Store
		rand.expectedNext(2); // Store
		rand.expectedNext(1); // Store
		rand.expectedNext(17); // Update index (up)
		rand.expectedNext(9); // Pick line 9 from elements in state
		rand.expectedNext(2); // Insert at index 2
		insertLineOperation(lines, state, &rand);
		REQUIRE(lines.size() == 6);
		REQUIRE(lines.front() == "a\n");
		REQUIRE(lines[2] == "e");
		REQUIRE(lines[3] == "c\n");
		REQUIRE(lines.back() == "e");
	}

	// radamsa -p od -m lis -s 0 in.txt (in.txt has the lines described above)
	// Should insert the d before the d line
	SECTION("Insert stateful with seed 25") {
		rand.expectedNext(4); // Store
		rand.expectedNext(1); // Store
		rand.expectedNext(4); // Store
		rand.expectedNext(4); // Store
		rand.expectedNext(2); // Store
		rand.expectedNext(2); // Store
		rand.expectedNext(1); // Store
		rand.expectedNext(3); // Store
		rand.expectedNext(3); // Store
		rand.expectedNext(4); // Store
		rand.expectedNext(1); // Update index (up)
		rand.expectedNext(2); // New element (ep)
		rand.expectedNext(2); // Pick line 9 from elements in state
		rand.expectedNext(3); // Insert at index 2
		insertLineOperation(lines, state, &rand);
		REQUIRE(lines.size() == 6);
		REQUIRE(lines.front() == "a\n");
		REQUIRE(lines[3] == "d\n");
		REQUIRE(lines[4] == "d\n");
		REQUIRE(lines.back() == "e");
	}

	REQUIRE(rand.allExpectedCallsMade());
}

/*
 * REPLACE LINE (STATEFUL)
 */
TEST_CASE("Test stateful line replacement will not error on one line", "[mutation, line, replace]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);
	LineState state(2, 4, *rand);

	lines.push_back("abcde");
	CHECK(lines.size() == 1);

	replaceLineOperation(lines, state, rand);
	REQUIRE(lines.size() == 1);
	REQUIRE(lines.front() == "abcde");

	delete rand;
}

TEST_CASE("Test line replacement does not throw error on empty lines", "[mutation, line, replace]") {
	std::vector<std::string> lines;
	Random* rand = new Random(0);
	LineState state(1, 1, *rand);

	replaceLineOperation(lines, state, rand);
	REQUIRE(lines.size() == 0);

	delete rand;
}

TEST_CASE("Test line replacement replicates radamsa behavior with 5 valid lines", "[mutation, line, replace]") {
	std::vector<std::string> lines;
	MockRandom rand;
	LineState state(10, 10 << 1, rand);
	lines.push_back("a\n");
	lines.push_back("b\n");
	lines.push_back("c\n");
	lines.push_back("d\n");
	lines.push_back("e");
	CHECK(lines.size() == 5);

	// radamsa -p od -m lis -s 0 in.txt (in.txt has the lines described above)
	// Should insert the e after the b line
	SECTION("Replace stateful with seed 0") {
		rand.expectedNext(4); // Store
		rand.expectedNext(3); // Store
		rand.expectedNext(4); // Store
		rand.expectedNext(3); // Store
		rand.expectedNext(1); // Store
		rand.expectedNext(2); // Store
		rand.expectedNext(0); // Store
		rand.expectedNext(2); // Store
		rand.expectedNext(2); // Store
		rand.expectedNext(1); // Store
		rand.expectedNext(17); // Update index (up)
		rand.expectedNext(9); // Pick line 9 from elements in state
		rand.expectedNext(2); // Insert at index 2
		replaceLineOperation(lines, state, &rand);
		REQUIRE(lines.size() == 5);
		REQUIRE(lines.front() == "a\n");
		REQUIRE(lines[2] == "e");
		REQUIRE(lines[3] == "d\n");
		REQUIRE(lines.back() == "e");
	}

	// radamsa -p od -m lis -s 8080 in.txt (in.txt has the lines described above)
	// DOES SOMETHING WEIRD. Prints out:
	// a
	// b
	// b
	// c

	// radamsa -p od -m lis -s 0 in.txt (in.txt has the lines described above)
	// Should insert the d before the d line
	SECTION("Replace stateful with seed 9002") {
		rand.expectedNext(1); // Store
		rand.expectedNext(1); // Store
		rand.expectedNext(3); // Store
		rand.expectedNext(0); // Store
		rand.expectedNext(4); // Store
		rand.expectedNext(4); // Store
		rand.expectedNext(0); // Store
		rand.expectedNext(0); // Store
		rand.expectedNext(0); // Store
		rand.expectedNext(2); // Store
		rand.expectedNext(3); // Update index (up)
		rand.expectedNext(2); // New element (ep)
		rand.expectedNext(6); // Pick line 9 from elements in state
		rand.expectedNext(1); // Insert at index 2
		replaceLineOperation(lines, state, &rand);
		REQUIRE(lines.size() == 5);
		REQUIRE(lines.front() == "a\n");
		REQUIRE(lines[1] == lines.front());
		REQUIRE(lines.back() == "e");
	}

	REQUIRE(rand.allExpectedCallsMade());
}
