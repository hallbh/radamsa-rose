#include "mock_random.h"

MockRandom::MockRandom() : Random(0) {}

unsigned long long MockRandom::nextInteger(unsigned long long max) {
	return fromProvider();
}

bool MockRandom::doesOccur(rational probability) {
	return fromProvider();
}

unsigned int MockRandom::nextBlockSize(unsigned int minSize, unsigned int maxSize) {
	return fromProvider();
}

int MockRandom::randomDelta() {
	return fromProvider();
}

int MockRandom::randomDeltaUp(rational probability)
{
	return this->fromProvider();
}

Radamsa::Bignum MockRandom::nextNBit(unsigned int n) {
	return Radamsa::Bignum(this->fromProvider());
}

Radamsa::Bignum MockRandom::nextLogInteger(unsigned long long max) {
	return Radamsa::Bignum(this->fromProvider());
}

unsigned long long MockRandom::nextIntegerRange(unsigned long long low, unsigned long long high) {
	return this->fromProvider();
}

bool MockRandom::allExpectedCallsMade() {
	return this->provider.empty();
}

void MockRandom::expectedNext(unsigned long long nextNumber) {
	this->provider.push(nextNumber);
}

std::vector<std::string> MockRandom::reservoirSample(std::vector<std::string> list, int n)
{
	std::vector<std::string> out = std::vector<std::string>();
	for (int i = 0; i < n; i++) out.push_back(list[i]);
	return out;
}

int MockRandom::fromProvider() {
	int num = this->provider.front();
	this->provider.pop();
	return num;
}
