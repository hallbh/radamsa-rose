#include "catch.hpp"
#include "random/bignum.h"
#include "random/random.h"
#include <vector>
#include <cstdint>

using namespace Radamsa;

TEST_CASE("Test initialization") {
	SECTION("Empty") {
		Bignum num = Bignum();
		REQUIRE(num.bitVector.size() == 0);
		REQUIRE(num.fixVector.size() == 0);
	}

	SECTION("Copy") {
		Bignum foo = Bignum(63);
		Bignum num = Bignum(foo);
		REQUIRE(num.bitVector.size() == 6); // 11 1111
		REQUIRE(num.fixVector.size() == 1); // 63
	}

	SECTION("Bignum 0") {
		Bignum num = Bignum(0);
		REQUIRE(num.bitVector.size() == 0);
		REQUIRE(num.fixVector.size() == 0);
	}

	SECTION("Bignum 1") {
		Bignum num = Bignum(1);
		REQUIRE(num.bitVector.size() == 1); // 1
		REQUIRE(num.fixVector.size() == 1); // 1
	}

	SECTION("Bignum 2^24 - 1") {
		Bignum num = Bignum(16777215);
		REQUIRE(num.bitVector.size() == 24); // 1111 1111 1111 1111 1111 1111
		REQUIRE(num.fixVector.size() == 1); // 16777215
	}

	SECTION("Bignum 2^24") {
		Bignum num = Bignum(16777216);
		REQUIRE(num.bitVector.size() == 25); // 1 0000 0000 0000 0000 0000 0000
		REQUIRE(num.fixVector.size() == 2); // 1, 0
	}

	SECTION("Bignum 2^24 - 1") {
		Bignum num = Bignum(281474976710655);
		REQUIRE(num.bitVector.size() == 48); // 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111 1111
		REQUIRE(num.fixVector.size() == 2); // 16777215, 16777215
	}

	SECTION("Bignum 2^48") {
		Bignum num = Bignum(281474976710656);
		REQUIRE(num.bitVector.size() == 49); // 1 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
		REQUIRE(num.fixVector.size() == 3); // 1, 0, 0
	}

	SECTION("Bignum 2^96") { // 78228162514264336593543950336
		std::vector<uint8_t> bits = std::vector<uint8_t>();
		for (int i = 0; i < 96; i++) bits.push_back(0);
		bits.push_back(1);

		Bignum num = Bignum(bits);
		REQUIRE(num.bitVector.size() == 97); 
		REQUIRE(num.fixVector.size() == 5); // 1, 0, 0, 0, 0
	}
}

TEST_CASE("Test Bignum on string constructor", "[bignum]") {
	SECTION("Bignum of 0") {
		Bignum big("0");
		REQUIRE((int) big == 0);
	}

	SECTION("Bignum of 16") {
		Bignum big("16");
		REQUIRE((int) big == 16);
	}

	SECTION("Bignum of 10000000000000") {
		Bignum big("10000000000000");
		REQUIRE((unsigned long long)big > 9999999999999);
	}
}

TEST_CASE("Comparison Operators") {
	Bignum a = Bignum(0);
	Bignum b = Bignum(1);
	Bignum c = Bignum(1);

	SECTION("==") {
		REQUIRE(b == c);
		REQUIRE_FALSE(a == b);
	}

	SECTION("!=") {
		REQUIRE(a != b);
		REQUIRE_FALSE(b != c);
	}

	SECTION(">") {
		REQUIRE(b > a);
		REQUIRE_FALSE(b > c);
		REQUIRE_FALSE(a > b);
	}

	SECTION("<") {
		REQUIRE(a < b);
		REQUIRE_FALSE(b < c);
		REQUIRE_FALSE(b < a);
	}

	SECTION(">=") {
		REQUIRE(b >= a);
		REQUIRE(b >= c);
		REQUIRE_FALSE(a >= b);
	}

	SECTION("<=") {
		REQUIRE(a <= b);
		REQUIRE(b <= c);
		REQUIRE_FALSE(b <= a);
	}
}

TEST_CASE("Arithmetic Operators") {
	Bignum a = Bignum(0);
	Bignum b = Bignum(1);
	Bignum c = Bignum(2);
	Bignum d = Bignum(16777215); // 2^24 - 1
	Bignum result;

	SECTION("+") {
		result = a + b;
		REQUIRE(result == b);

		result = result + c;
		REQUIRE(result == Bignum(3));

		result = b + d;
		REQUIRE(result == Bignum(16777216));
	}

	SECTION("*") {
		result = a * b;
		bool check = result == a;
		REQUIRE(check);

		result = b * b;
		check = result == b;
		REQUIRE(check);

		result = c * c;
		check = result == Bignum(4);
		REQUIRE(check);

		result = d * c;
		check = result == Bignum(33554430);
		REQUIRE(check);

		std::vector<uint8_t> bits = std::vector<uint8_t>();
		for (uint8_t bit : Radamsa::Random::promoter) bits.push_back(bit);
		std::reverse(bits.begin(), bits.end()); // Convert to [lsd ... msd]
		Radamsa::Bignum promoter = Radamsa::Bignum(bits);

		bits.erase(bits.begin(), bits.end());
		uint8_t expectedBits[84] = { 1,1,0,0,0,1,0,0,1,1,0,0,0,0,1,0,0,0,1,1,1,0,0,1,1,1,0,1,0,1,1,0,1,0,0,0,1,1,0,1,0,1,0,1,1,0,0,0,1,0,0,0,0,1,1,1,1,0,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,1,0,1,1,0 };
		for (uint8_t bit : expectedBits) bits.push_back(bit);
		std::reverse(bits.begin(), bits.end()); // Convert to [lsd ... msd]
		Radamsa::Bignum expected = Radamsa::Bignum(bits);

		result = Bignum(1338) * promoter;
		check = result == expected;
		REQUIRE(check);
	}

	//SECTION("+=") {
	//	result = Bignum(0);

	//	result += b;
	//	REQUIRE(result == b);

	//	result += c;
	//	REQUIRE(result == Bignum(3));

	//	result += d;
	//	REQUIRE(result == Bignum(16777218));
	//}
}

TEST_CASE("Stack operations") {
	Bignum num = Bignum();

	//SECTION("Push") {
		num.push(1);
		REQUIRE(num == Bignum(1));
		num.push(0);
		REQUIRE(num == Bignum(16777216));
		REQUIRE(num.bitVector.size() == 25);
	//}
	//SECTION("Pop") {
		uint32_t popped = num.pop();
		REQUIRE(popped == 0);

		REQUIRE(num == Bignum(1));
		REQUIRE(num.bitVector.size() == 1);

		uint32_t peeked = num.peek();
		REQUIRE(peeked == 1);
}

TEST_CASE("Shift") {
	Bignum num = Bignum(16);
	num << 1ULL;
	REQUIRE(num == Bignum(32));
}

TEST_CASE("Powers") {
	Bignum num = Bignum(16);
	num.generatePowersTo(20);
	std::string actual = num.getNthPowerOfTwo(20);
	REQUIRE(actual == "1048576");
}

TEST_CASE("To String") {
	Bignum num = Bignum(16777215); // 2^24 - 1
	REQUIRE(num.toString() == "16777215");

	Bignum two48 = Bignum(281474976710656); // 2^48
	REQUIRE(two48.toString() == "281474976710656");

	// 39614081257132168796771975168 // 2^95
	std::vector<uint8_t> bits = std::vector<uint8_t>();
	for (int i = 0; i < 95; i++) bits.push_back(0);
	bits.push_back(1);

	Bignum two95 = Bignum(bits);
	REQUIRE(two95.toString() == "39614081257132168796771975168");
}