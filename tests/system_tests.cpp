#include "catch.hpp"
#include "algorithm.h"
#include "radamsa_algorithm.h"

void initDefaultConstants(radamsa_constants& constants) {
	constants.maxHashCount = 1024 * 1024;
	constants.searchFuel = 100000;
	constants.searchStopIp = 8;
	constants.storedElements = 10;
	constants.updateProbability = constants.storedElements << 1;
	constants.minScore = 2;
	constants.maxScore = 10;
	constants.pWeakUsually.numerator = 11;
	constants.pWeakUsually.denominator = 20;
	constants.randDeltaDecrease = -1;
	constants.minTexty = 6;
	constants.maxMutations = 16;
	constants.averageBlockSize = 2048;
	constants.minBlockSize = 256;
	constants.initialIp = 24;
	constants.remutateProbability.numerator = 4;
	constants.remutateProbability.denominator = 5;
	constants.maxBlockSize = 2 * constants.averageBlockSize;
	constants.maxPumpLimit = 0b100000000000000;
}

std::string runTestSystemOnInput(Radamsa::RadamsaAlgorithm& algorithm, std::string input) {
	std::unique_ptr<Radamsa::Mutation> muta = algorithm.muxAndGetMutations();
	std::unique_ptr<Radamsa::Generator> gen = algorithm.muxAndGetGenerators();
	std::unique_ptr<Radamsa::Pattern> pat = algorithm.muxAndGetPatterns();

	Radamsa::AlgorithmParameters params{ muta.get(), gen.get(), pat.get() };
	return Radamsa::runAlgorithm(input, params);
}

const std::string abcde = "abcde\n";

TEST_CASE("Full system test with simple configuration on seed 0", "[system]") {
	struct radamsa_constants constants { 0 };
	initDefaultConstants(constants);

	Radamsa::RadamsaAlgorithm alg(0, constants);
	alg.setAllMutationsEnabled(false);
	alg.setAllPatternsEnabled(false);
	alg.setAllGeneratorsEnabled(false);

	alg.setMutationEnabled("lr2", true);
	alg.setPatternEnabled("od", true);
	alg.setGeneratorEnabled("stdin", true);

	std::string result = runTestSystemOnInput(alg, abcde);

	REQUIRE(result == "abcde\nabcde\n");
	REQUIRE(alg.peekRandomState() == 7008760); // The last rs of radamsa
}

TEST_CASE("Full system test focusing on pat-burst (chosen by mux) using stdin and byte insert on seed 0", "[system3]") {
	struct radamsa_constants constants { 0 };
	initDefaultConstants(constants);

	Radamsa::RadamsaAlgorithm alg(0, constants);
	alg.setAllMutationsEnabled(false);
	alg.setAllGeneratorsEnabled(false);

	alg.setMutationEnabled("bi", true);
	alg.setGeneratorEnabled("stdin", true);

	std::string result = runTestSystemOnInput(alg, abcde);

	REQUIRE(alg.peekRandomState() == 8195123); // The last rs of radamsa

	REQUIRE(result.length() == 11);
	REQUIRE(result.front() == 'g');
	REQUIRE(result[3] == '\xAD');
	REQUIRE(result[5] == '\xDD');
	REQUIRE(result[8] == '\xD4');
	REQUIRE(result[9] == 'e');
	REQUIRE(result.back() == '\n');
	
}

TEST_CASE("Full system test focusing on pat-many (chosen by mux) using stdin and byte insert on seed 6", "[system]") {
	struct radamsa_constants constants { 0 };
	initDefaultConstants(constants);

	Radamsa::RadamsaAlgorithm alg(6, constants);
	alg.setAllMutationsEnabled(false);
	alg.setAllGeneratorsEnabled(false);

	alg.setMutationEnabled("bi", true);
	alg.setGeneratorEnabled("stdin", true);

	std::string result = runTestSystemOnInput(alg, abcde);

	REQUIRE(alg.peekRandomState() == 13297610); // The last rs of radamsa

	REQUIRE(result.size() == 9);
	REQUIRE(result.front() == 'a');
	REQUIRE(result[2] == '\xB4');
	REQUIRE(result[5] == '\xB3');
	REQUIRE(result[6] == '\x0B');
	REQUIRE(result.back() == '\n');
}


TEST_CASE("Full system test focusing on the mux fuzzer, using pat-once and stdin on seed 0", "[system-line]") {
	struct radamsa_constants constants { 0 };
	initDefaultConstants(constants);

	Radamsa::RadamsaAlgorithm alg(0, constants);
	alg.setAllMutationsEnabled(false);
	alg.setAllPatternsEnabled(false);
	alg.setAllGeneratorsEnabled(false);

	alg.setMutationEnabled("ld", true);
	alg.setMutationEnabled("lds", true);
	alg.setMutationEnabled("lr2", true);
	alg.setMutationEnabled("li", true);
	alg.setMutationEnabled("lr", true);
	alg.setMutationEnabled("ls", true);
	alg.setMutationEnabled("lp", true);

	alg.setPatternEnabled("od", true);
	alg.setGeneratorEnabled("stdin", true);

	std::string result = runTestSystemOnInput(alg, abcde);

	REQUIRE(alg.peekRandomState() == 3776145); // The last rs of radamsa
	REQUIRE(result.empty()); // Line delete chosen
}

// echo "abcde" | ./radamsa -s 0 -g stdin -M - -m bd,bf,bi,br,bei,bed,ber,ld,lds,lr2,li,lr,ls,lis,lrs
TEST_CASE("Full system bug test case found during demo", "[system-bug]") {
	struct radamsa_constants constants { 0 };
	initDefaultConstants(constants);

	Radamsa::RadamsaAlgorithm alg(0, constants);
	alg.setAllMutationsEnabled(false);
	alg.setAllGeneratorsEnabled(false);

	alg.setMutationEnabled("bd", true);
	alg.setMutationEnabled("bf", true);
	alg.setMutationEnabled("bi", true);
	alg.setMutationEnabled("br", true);
	alg.setMutationEnabled("bei", true);
	alg.setMutationEnabled("bed", true);
	alg.setMutationEnabled("ber", true);

	alg.setMutationEnabled("ld", true);
	alg.setMutationEnabled("lds", true);
	alg.setMutationEnabled("lr2", true);
	alg.setMutationEnabled("li", true);
	alg.setMutationEnabled("lr", true);
	alg.setMutationEnabled("ls", true);
	alg.setMutationEnabled("lis", true);
	alg.setMutationEnabled("lrs", true);

	alg.setGeneratorEnabled("stdin", true);

	std::string result = runTestSystemOnInput(alg, abcde);

	REQUIRE(alg.peekRandomState() == 13126760); // The last rs of radamsa
}


TEST_CASE("Run full system test for exactness against radamsa", "[full-system]") {
	struct radamsa_constants constants { 0 };
	initDefaultConstants(constants);

	/*
	 * echo "abcde" | ./radamsa.exe -s 0 -M -
	 * fuse-old: 4, byte-random: 2, byte-dec: 2, generator: stdin, checksum: "(6191113 170773 6228928 2)", nth: 1, output: stdout, length: 9, pattern: many-dec
	 */
	 SECTION("Seed 0") {
	 	Radamsa::RadamsaAlgorithm alg(0, constants);
	 	std::string result = runTestSystemOnInput(alg, "abcde\n");

	 	REQUIRE(alg.peekRandomState() == 1251858); // The last rs of radamsa
	 	REQUIRE(result.size() == 9);
	 	REQUIRE(result.substr(0, 7) == "abcdabc");
	 	REQUIRE(result[7] == (char)249);
	 	REQUIRE(result.back() == 'v');
	 }

	/*
	 * echo "abcde" | ./radamsa.exe -s 200 -M -
	 * line-del: 1, sed-seq-del: 1, generator: stdin, checksum: "(0 0 0 0)", nth: 1, output: stdout, length: 0, pattern: many-dec
	 */
	SECTION("Seed 200") {
		Radamsa::RadamsaAlgorithm alg(200, constants);
		std::string result = runTestSystemOnInput(alg, "abcde\n");

		REQUIRE(alg.peekRandomState() == 484086); // The last rs of radamsa
		REQUIRE(result.empty());
	}

	/*
	 * echo "abcde" | ./radamsa.exe -s 89 -M -
	 * line-del: 1, fuse-old: 3, byte-insert: 1, generator: stdin, checksum: "(11646988 329577 12170519 3)", nth: 1, output: stdout, length: 12, pattern: many-dec
	 */
	SECTION("Seed 89") {
		Radamsa::RadamsaAlgorithm alg(89, constants);
		std::string result = runTestSystemOnInput(alg, "abcde\n");

		REQUIRE(alg.peekRandomState() == 15976963); // The last rs of radamsa
		REQUIRE(result == "abcbcanbcde\n");
	}

	/*
	 * echo "abcde" | ./radamsa.exe -s 1234567 -M -
	 * byte-drop: 1, generator: stdin, checksum: "(11635717 354403 2835224 0)", nth: 1, output: stdout, length: 5, pattern: many-dec
	 */
	SECTION("Seed 1234567") {
		Radamsa::RadamsaAlgorithm alg(1234567, constants);
		std::string result = runTestSystemOnInput(alg, "abcde\n");

		REQUIRE(alg.peekRandomState() == 15615903); // The last rs of radamsa
		REQUIRE(result == "abcd\n");
	}

	/*
	 * echo "abcde" | ./radamsa.exe -s 987654321 -M -
	 * utf8-insert: 2, byte-perm: 1, generator: stdin, checksum: "(2061 6382275 10958784 30)", nth: 1, output: stdout, length: 13, pattern: burst
	 */
	 SECTION("Seed 987654321") {
	 	Radamsa::RadamsaAlgorithm alg(987654321, constants);
	 	std::string result = runTestSystemOnInput(alg, "abcde\n");

	 	REQUIRE(alg.peekRandomState() == 15164236); // The last rs of radamsa
	 	REQUIRE(result.size() == 13);
	 }

	// echo "Rias made me do this" | ./r.exe -s 1337 -M -
	// utf8-insert: 1, generator: stdin, checksum: "(16599065 5400939 15399568 1635)", nth: 1, output: stdout, length: 25, pattern: once-dec
	SECTION("Seed 1337") {
		Radamsa::RadamsaAlgorithm alg(1337, constants);
		std::string result = runTestSystemOnInput(alg, "Rias made me do this\n");

		REQUIRE(alg.peekRandomState() == 5679373); // The last rs of radamsa
		std::string expected{ 82, 105, 97, 115, 32, 109, 97, 100, 101, 32, 109, 101, (char) 243, (char) 160, (char) 129, (char) 130, 32, 100, 111, 32, 116, 104, 105, 115, 10 };
		REQUIRE(result == expected);
	}

	// echo "Rias made me do this" | ./radamsa.exe -s 111222333 -M -
	// list-ins: 1, generator: stdin, checksum: "(13833258 3873387 15113552 128843)", nth: 1, output: stdout, length: 42, pattern: once-dec
	SECTION("Seed 111222333") {
		Radamsa::RadamsaAlgorithm alg(111222333, constants);
		std::string result = runTestSystemOnInput(alg, "Rias made me do this\n");

		REQUIRE(alg.peekRandomState() == 11910939); // The last rs of radamsa
		
		REQUIRE(result == "Rias made me do this\nRias made me do this\n");
	}

	// "Abracadabra 12345" | ./radamsa.exe -s 13337 -M -
	// muta-num: 1, generator: stdin, checksum: "(3571764 4285048 9825044 7788889)", nth: 1, output: stdout, length: 52, pattern: many-dec
	SECTION("Seed 13337") {
		Radamsa::RadamsaAlgorithm alg(13337, constants);
		std::string result = runTestSystemOnInput(alg, "Abracadabra 12345\n");

		REQUIRE(alg.peekRandomState() == 409311); // The last rs of radamsa

		REQUIRE(result == "Abracadabra 170141183460469231731687303715884093382\n");
	}

	// echo "<Abracadabra>" | ./radamsa.exe -s 11235813 -M -
	// xp-repeat: 1, generator: stdin, checksum: "(1342517 150370 12519936 2652866)", nth: 1, output: stdout, length: 53, pattern: once-dec
	SECTION("SEED 11235813") {
		Radamsa::RadamsaAlgorithm alg(11235813, constants);
		std::string result = runTestSystemOnInput(alg, "<Abracadabra>\n");

		REQUIRE(alg.peekRandomState() == 1229497); // The last rs of radamsa

		REQUIRE(result == "<Abracadabra><Abracadabra><Abracadabra><Abracadabra>\n");
	}

	// echo "the quick brown fox jumped over the lazy dogs" | ./radamsa.exe -s 135790 -M -
	// list-ins: 1, fuse-old: 4, byte-random: 1, generator: stdin, checksum: "(7717970 7628911 13601282 15243509)", nth: 1, output: stdout, length: 82, pattern: many-dec
	SECTION("SEED 135790") {
		Radamsa::RadamsaAlgorithm alg(135790, constants);
		std::string result = runTestSystemOnInput(alg, "the quick brown fox jumped over the lazy dogs\n");

		REQUIRE(alg.peekRandomState() == 15792079); // The last rs of radamsa

		REQUIRE(result == "the quick brown fox jumped over the quick brown fox jumped over the the lazy-dogs\n");
	}
}

TEST_CASE("Test found tricky bug cases for the full system", "[full-system][system-bug][!mayfail]") {
	struct radamsa_constants constants { 0 };
	initDefaultConstants(constants);

	// echo "Rias made me do this" | ./radamsa.exe -s 12481632 -M -
	// ab-string: 1, utf8-insert: 1, seq-repeat: 1, generator: stdin, nth: 1, output: stdout, length: 849, pattern: many-dec
	SECTION("Seed 12481632") {
		Radamsa::RadamsaAlgorithm alg(12481632, constants);
		std::string result = runTestSystemOnInput(alg, "Rias made me do this\n");

		REQUIRE(alg.peekRandomState() == 5679373); // The last rs of radamsa
		std::string expected= "Rias made me do tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho \\r$1$!!%sNaN\"xcalc\\r`xcalc`\\r\\0; xcalc$PATH$(xcalc)$`$& + inf$ & $ + % ntho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho tho this+/v+\n";
		REQUIRE(result == expected);
	}

	/*
	 * echo "abcde" | ./radamsa.exe -s 10 -M -
	 * utf8-insert: 1, seq-repeat: 2, fuse-this: 1, byte-repeat: 1, byte-drop: 2, generator: stdin, checksum: "(6855712 6783970 5193076 2061)", nth: 1, output: stdout, length: 32, pattern: burst
	 */
	 SECTION("Seed 10") {
	 	Radamsa::RadamsaAlgorithm alg(10, constants);
	 	std::string result = runTestSystemOnInput(alg, "abcde\n");

	 	REQUIRE(alg.peekRandomState() == 3660354); // The last rs of radamsa
	 	REQUIRE(result.size() == 32);
	 	REQUIRE(result.front() == (char) 226);
	 	REQUIRE(result.back() == 'a');
	 }

	/*
	 * echo "abcde" | ./radamsa.exe -s 42 -M -
	 * utf8-insert: 2, fuse-old: 1, byte-drop: 1, generator: stdin, checksum: "(9369610 8494485 14082833 6)", nth: 1, output: stdout, length: 10, pattern: burst
	 */
	SECTION("Seed 42") {
		Radamsa::RadamsaAlgorithm alg(42, constants);
		std::string result = runTestSystemOnInput(alg, "abcde\n");

		REQUIRE(alg.peekRandomState() == 12268673); // The last rs of radamsa
	}
}

TEST_CASE("Test setAllMutationEnabled bug", "[setAllMutation]") {
	struct radamsa_constants constants { 0 };
	initDefaultConstants(constants);

	Radamsa::RadamsaAlgorithm alg(0, constants);
	alg.setAllMutationsEnabled(true);

	REQUIRE(alg.isMutationEnabled("bi") == true);
	REQUIRE(alg.isMutationEnabled("bd") == true);
	REQUIRE(alg.isMutationEnabled("bed") == true);
	REQUIRE(alg.isMutationEnabled("bei") == true);
}