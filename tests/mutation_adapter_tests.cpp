#include "catch.hpp"
#include "mutation/mutation_adapter.h"

int superBasicMutation(struct input_chunk* chunkHead, struct radamsa_constants, radamsa_random random) {
	chunkHead->chunkPointer[0] = 'b';
	return 1;
}

int mutationFunctionThatAllocates(struct input_chunk* chunkHead, struct radamsa_constants, radamsa_random random) {
	delete[] chunkHead->chunkPointer;
	int newSize = chunkHead->chunkSize * 2;
	char* newStr = new char[newSize];
	for (int i = 0; i < newSize; ++i) {
		newStr[i] = 'a';
	}
	chunkHead->chunkPointer = newStr;
	chunkHead->chunkSize = newSize;
	return 1;
}

TEST_CASE("Test Mutation Adapter on basic mutation function", "[mutation][muta-adapter]") {
	Radamsa::Random rand(0);
	struct radamsa_constants c { 0 };
	Radamsa::MutationAdapter adapter(superBasicMutation, "basic", 2, c, rand);
	std::deque<std::string> input;
	input.push_back("a");

	adapter.mutate(input);
	REQUIRE(input.front() == "b");
}

TEST_CASE("Test Mutation Adapter on a mutation function that actually does complicated stuff with allocation", "[mutation][muta-adapter][allocation]") {
	Radamsa::Random rand(0);
	struct radamsa_constants c { 0 };
	Radamsa::MutationAdapter adapter(mutationFunctionThatAllocates, "complex", 2, c, rand);
	std::deque<std::string> input;
	input.push_back("abcde");

	adapter.mutate(input);
	std::string res = input.front();
	CHECK(input.size() == 1);
	CHECK(res.size() == 10);
	for (char c : res) {
		REQUIRE(c == 'a');
	}
}