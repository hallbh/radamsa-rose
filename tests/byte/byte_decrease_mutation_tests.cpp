#include "catch.hpp"
#include "iostream"
#include "mutation/byte/byteDecreaseMutation.h"
#include "../mock/mock_random.h"

// echo "abcde" | radamsa -p od -m bed -s seed#    to run Radamsa and test

TEST_CASE("Test construction of Byte Decrease Mutation") {

	struct radamsa_constants constants = { 0 };
	Radamsa::Random random(0);
	Radamsa::ByteDecreaseMutation m { 5, constants, random };	

	REQUIRE(m.score == 0);
	REQUIRE(m.priority == 5);
	REQUIRE(m.getName() == "bed");
}

TEST_CASE("Test Byte Decrease Mutation with empty input string") {
	struct radamsa_constants constants = { 0 };

	SECTION("Input Empty String") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDecreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "");
	}	
}

TEST_CASE("Test Byte Decrease Mutation with input length of 5") {

	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDecreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abbde");
	}

	SECTION("Random seed 37") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(4);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDecreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		
		REQUIRE(output == "abcdd");
	}

	SECTION("Random seed 223") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDecreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "`bcde");
	}

	SECTION("input contain z and seed 334") {
		std::string input = "abzzz";
		MockRandom mock;
		mock.expectedNext(4);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDecreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abzzy");
	}

	SECTION("input contain A and seed 567") {
		std::string input = "abAAA";
		MockRandom mock;
		mock.expectedNext(4);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDecreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abAA@");
	}

	SECTION("input contain B and seed 325846178") {
		std::string input = "abAAA";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDecreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "ab@AA");
	}
}