#include "catch.hpp"
#include "iostream"
#include "mutation/byte/byteRepeatMutation.h"
#include "../mock/mock_random.h"

// echo "abcde" | radamsa -p od -m br -s seed#    to run Radamsa and test


TEST_CASE("Test construction of Byte Repeat Mutation") {

	struct radamsa_constants constants = { 0 };
	Radamsa::Random random(0);
	Radamsa::ByteRepeatMutation m{ 5, constants, random };

	REQUIRE(m.score == 0);
	REQUIRE(m.priority == 5);
	REQUIRE(m.getName() == "br");
}

TEST_CASE("Test Byte Repeat Mutation with empty input string") {
	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		mock.expectedNext(0);
		mock.expectedNext(0);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRepeatMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output.length() == 0);
		REQUIRE(output == "");
	}

	SECTION("Random seed 99") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		mock.expectedNext(1);
		mock.expectedNext(0);
		mock.expectedNext(0);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRepeatMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output.length() == 0);
		REQUIRE(output == "");
	}
}

TEST_CASE("Test Byte Repeat Mutation with input length of 5") {

	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		mock.expectedNext(0);
		mock.expectedNext(0);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRepeatMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 5);
		REQUIRE(output == "abcde");
	}

	SECTION("Random seed 87") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(1);
		mock.expectedNext(1);
		mock.expectedNext(1);
		mock.expectedNext(1);
		mock.expectedNext(1);
		mock.expectedNext(0);
		mock.expectedNext(8);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRepeatMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 13);
		REQUIRE(output == "abbbbbbbbbcde");
	}

	SECTION("Random seed 999") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		mock.expectedNext(0);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRepeatMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 6);
		REQUIRE(output == "aabcde");
	}

	SECTION("Random seed 76543210") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(3);
		mock.expectedNext(1);
		mock.expectedNext(1);
		mock.expectedNext(0);
		mock.expectedNext(2);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRepeatMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 7);
		REQUIRE(output == "abcddde");
	}

	SECTION("Random seed 111111111") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(4);
		mock.expectedNext(1);
		mock.expectedNext(0);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRepeatMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 6);
		REQUIRE(output == "abcdee");
	}
}