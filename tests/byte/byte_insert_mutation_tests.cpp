#include "catch.hpp"
#include "iostream"
#include "mutation/byte/byteInsertMutation.h"
#include "../mock/mock_random.h"

// echo "abcde" | radamsa -p od -m bi -s seed#    to run Radamsa and test

TEST_CASE("Test construction of Byte Insert Mutation") {

	struct radamsa_constants constants = { 0 };
	Radamsa::Random random(0);
	Radamsa::ByteInsertMutation m{ 5, constants, random };

	REQUIRE(m.score == 0);
	REQUIRE(m.priority == 5);
	REQUIRE(m.getName() == "bi");
}

TEST_CASE("Test Byte Insert Mutation with empty input string") {
	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0); // Position
		mock.expectedNext(1); // Delta
		mock.expectedNext(31); // Character
		Radamsa::Random* random = &mock;
		Radamsa::ByteInsertMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output.empty());
		//REQUIRE(output == "\x1f");
	}

	SECTION("Random seed 100") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		mock.expectedNext(29);
		Radamsa::Random* random = &mock;
		Radamsa::ByteInsertMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output.empty());
		//REQUIRE(output == "\x1D");
	}

	SECTION("Random seed 50000") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		mock.expectedNext(137);
		Radamsa::Random* random = &mock;
		Radamsa::ByteInsertMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output.empty());
		//REQUIRE(output == "\x89");
	}
}

TEST_CASE("Test Byte Insert Mutation with input length of 6") {

	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 22") {
		std::string input = "abcde\n";
		MockRandom mock;
		mock.expectedNext(4);
		mock.expectedNext(-1);
		mock.expectedNext(109);
		Radamsa::Random* random = &mock;
		Radamsa::ByteInsertMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(input.length() == 6);
		REQUIRE(output.length() == 7);
		REQUIRE(output == "abcdme\n");
	}

	SECTION("Random seed 222") {
		std::string input = "abcde\n";
		MockRandom mock;
		mock.expectedNext(5);
		mock.expectedNext(-1);
		mock.expectedNext(244);
		Radamsa::Random* random = &mock;
		Radamsa::ByteInsertMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(input.length() == 6);
		REQUIRE(output.length() == 7);
		REQUIRE(output[5] == '\xF4');
	}

	SECTION("Random seed 123123") {
		std::string input = "abcde\n";
		MockRandom mock;
		mock.expectedNext(1);
		mock.expectedNext(-1);
		mock.expectedNext(73);
		Radamsa::Random* random = &mock;
		Radamsa::ByteInsertMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(input.length() == 6);
		REQUIRE(output.length() == 7);
		REQUIRE(output == "aIbcde\n");
	}

	SECTION("input contain z and seed 777777") {
		std::string input = "abzzz\n";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		mock.expectedNext(153);
		Radamsa::Random* random = &mock;
		Radamsa::ByteInsertMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 6);
		REQUIRE(output.length() == 7);
		REQUIRE(output.front() == '\x99');
	}
}