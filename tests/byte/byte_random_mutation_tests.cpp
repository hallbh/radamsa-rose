#include "catch.hpp"
#include "iostream"
#include "mutation/byte/byteRandomMutation.h"
#include "../mock/mock_random.h"

// echo "abcde" | radamsa -p od -m ber -s seed#    to run Radamsa and test

TEST_CASE("Test construction of Byte Random Mutation") {

	struct radamsa_constants constants = { 0 };
	Radamsa::Random random(0);
	Radamsa::ByteRandomMutation m{ 5, constants, random };

	REQUIRE(m.score == 0);
	REQUIRE(m.priority == 5);
	REQUIRE(m.getName() == "ber");
}

TEST_CASE("Test Byte Random Mutation with empty input string") {
	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		mock.expectedNext(103);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRandomMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "");
	}

	SECTION("Random seed 32") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		mock.expectedNext(50);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRandomMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "");
	}
}

TEST_CASE("Test Byte Random Mutation with input length of 5") {

	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		mock.expectedNext(103);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRandomMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		std::string expect = "gbcde";
		REQUIRE(output == expect);
	}

	SECTION("Random seed 123") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(4);
		mock.expectedNext(-1);
		mock.expectedNext(68);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRandomMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		std::string expect = "abcdD";
		REQUIRE(output == expect);
	}

	SECTION("Random seed 321") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(3);
		mock.expectedNext(-1);
		mock.expectedNext(91);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRandomMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		std::string expect = "abc[e";
		REQUIRE(output == expect);
	}

	SECTION("input contain z and seed 666") {
		std::string input = "abzzz";
		MockRandom mock;
		mock.expectedNext(1);
		mock.expectedNext(-1);
		mock.expectedNext(123);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRandomMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		std::string expect = "a{zzz";
		REQUIRE(output == expect);
	}

	SECTION("input contain A and seed 123456") {
		std::string input = "abAAA";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		mock.expectedNext(109);
		Radamsa::Random* random = &mock;
		Radamsa::ByteRandomMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "mbAAA");
	}
}