#include "catch.hpp"
#include "generator/filegenerator.h"

TEST_CASE("Test File Generator on no file path", "[!mayfail]") {
	INFO("Fails when not run in tests directory");

	struct radamsa_constants consts = { 0 };
	consts.minBlockSize = 256;
	consts.maxBlockSize = 4096;
	Radamsa::Random random(0);
	random.nextInteger(10);
	Radamsa::FileGenerator gen(0, consts, random);
	std::deque<std::string> out = gen.generate("");

	REQUIRE(out.size() == 0);
}

TEST_CASE("Test File Generator on non-exist file path", "[!mayfail]") {
	INFO("Fails when not run in tests directory");

	SECTION("One File") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("file.txt");
		REQUIRE(out.front() == "file.txt");
		REQUIRE(out.size() == 1);
	}

	SECTION("Two Files") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("file.txt,non.txt");
		REQUIRE(out.front() == "non.txt");
		REQUIRE(out.size() == 1);
	}
}

TEST_CASE("Test File Generator on non-exist file path with spaces", "[!hide]") {
	INFO("Fails when not run in tests directory");

	SECTION("One File") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("   file.txt  ");
		REQUIRE(out.front() == "   file.txt  ");
		REQUIRE(out.size() == 1);
	}

	SECTION("Two Files") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate(" file.txt,  non.txt");
		REQUIRE(out.front() == "  non.txt");
		REQUIRE(out.size() == 1);
	}
}

TEST_CASE("Test File Generator on empty data file", "[!mayfail][generator][gen-file][gen-file-empty]") {
	INFO("Fails when not run in tests directory");

	struct radamsa_constants consts = { 0 };
	consts.minBlockSize = 256;
	consts.maxBlockSize = 4096;
	Radamsa::Random random(0);
	random.nextInteger(10);
	Radamsa::FileGenerator gen(0, consts, random);

	std::deque<std::string> out = gen.generate("test_data/empty.txt");
	REQUIRE(random.peekState() == 7008760);
	REQUIRE(out.size() == 0);
}

TEST_CASE("Test File Generator on one file path", "[!mayfail][generator][gen-file][gen-file-input]") {
	INFO("Fails when not run in tests directory");

	SECTION("Seed 0") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/testInputFile.txt");
		REQUIRE(random.peekState() == 7008760);
		REQUIRE(out.size() == 4);
		REQUIRE(out.front().front() == '1');
		REQUIRE(out.back().front() == 'a');
	}

	SECTION("Seed 10") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(10);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/testInputFile.txt");
		REQUIRE(random.peekState() == 6807599);
		REQUIRE(out.size() == 3);
		REQUIRE(out.front().front() == '1');
		REQUIRE(out.back().front() == 'v');
	}

	SECTION("Seed 88") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(88);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/testInputFile.txt");
		REQUIRE(random.peekState() == 3546975);
		REQUIRE(out.size() == 2);
		REQUIRE(out.front().front() == '1');
		REQUIRE(out.back().front() == 't');
	}

	SECTION("Seed 123456") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(123456);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/testInputFile.txt");
		REQUIRE(random.peekState() == 791820);
		REQUIRE(out.size() == 3);
		REQUIRE(out.front().front() == '1');
		REQUIRE(out.back().front() == 'h');
	}
}

TEST_CASE("Test File Generator on multiple file paths", "[!mayfail][generator][gen-file]") {
	INFO("Fails when not run in tests directory");

	SECTION("Seed 0") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/testInputFile.txt,test_data//test2.txt");
		REQUIRE(random.peekState() == 7008760);
		REQUIRE(out.size() == 3);
		REQUIRE(out.front().front() == '1');
		REQUIRE(out.back().front() == 'h');
	}

	SECTION("Seed 1") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(1);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/testInputFile.txt, test_data/test2.txt");
		REQUIRE(random.peekState() == 4174385);
		REQUIRE(out.size() == 5);
		REQUIRE(out.front().front() == '1');
		REQUIRE(out.back().front() == 'q');
	}

	SECTION("Seed 50") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(50);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/test2.txt,test_data/test3.txt,test_data/test4.txt");
		REQUIRE(random.peekState() == 8745187);
		REQUIRE(out.size() == 1);
		REQUIRE(out.front().front() == 'm');
		REQUIRE(out.front().back() == 'u');
	}

	SECTION("Seed 322") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(322);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/testInputFile.txt,test_data/test2.txt,test_data/test3.txt,test_data/test4.txt");
		REQUIRE(random.peekState() == 2318672);
		REQUIRE(out.size() == 1);
		REQUIRE(out.front().front() == 'e');
		REQUIRE(out.front().back() == 'w');
	}
}

TEST_CASE("Test File Generator on multiple file paths with spaces", "[!mayfail][generator][gen-file]") {
	INFO("Fails when not run in tests directory");

	SECTION("Seed 0") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("    test_data/testInputFile.txt,test_data//test2.txt  ");
		REQUIRE(random.peekState() == 7008760);
		REQUIRE(out.size() == 3);
		REQUIRE(out.front().front() == '1');
		REQUIRE(out.back().front() == 'h');
	}

	SECTION("Seed 1") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(1);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/testInputFile.txt, test_data/test   2.txt");
		REQUIRE(random.peekState() == 4174385);
		REQUIRE(out.size() == 5);
		REQUIRE(out.front().front() == '1');
		REQUIRE(out.back().front() == 'q');
	}

	SECTION("Seed 50") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(50);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/test   2.txt, test_data/test3.txt,   test_data/test4.txt");
		REQUIRE(random.peekState() == 8745187);
		REQUIRE(out.size() == 1);
		REQUIRE(out.front().front() == 'm');
		REQUIRE(out.front().back() == 'u');
	}

	SECTION("Seed 322") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 256;
		consts.maxBlockSize = 4096;
		Radamsa::Random random(322);
		random.nextInteger(10);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("        test_data/testInputFile.txt,              test_data/test   2.txt,test_data/test3.txt,         test_data/test4.txt");
		REQUIRE(random.peekState() == 2318672);
		REQUIRE(out.size() == 1);
		REQUIRE(out.front().front() == 'e');
		REQUIRE(out.front().back() == 'w');
	}
}


TEST_CASE("Test File Generator with small block size", "[!mayfail][generator][gen-file]") {
	INFO("Fails when not run in tests directory");

	SECTION("Exactly size one") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 1;
		consts.maxBlockSize = 1;
		Radamsa::Random random(0);
		Radamsa::FileGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/smallTestInputFile.txt");
		REQUIRE(out.size() == 5);
		REQUIRE(out.back().front() == 'e');
	}
}