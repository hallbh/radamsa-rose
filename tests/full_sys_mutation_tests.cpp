﻿#include "catch.hpp"
#include "algorithm.h"
#include "radamsa_algorithm.h"
#include "iostream"

std::pair<std::string, uint32_t> runIndiviualMutationOnFullSystemWithPattern(int seed, std::string mutation, std::string pattern, std::string input) {
    struct radamsa_constants constants { 0 };
    constants.maxHashCount = 1024 * 1024;
    constants.searchFuel = 100000;
    constants.searchStopIp = 8;
    constants.storedElements = 10;
    constants.updateProbability = constants.storedElements << 1;
    constants.minScore = 2;
    constants.maxScore = 10;
    constants.pWeakUsually.numerator = 11;
    constants.pWeakUsually.denominator = 20;
    constants.randDeltaDecrease = -1;
    constants.minTexty = 6;
    constants.maxMutations = 16;
    constants.averageBlockSize = 2048;
    constants.minBlockSize = 256;
    constants.initialIp = 24;
    constants.remutateProbability.numerator = 4;
    constants.remutateProbability.denominator = 5;
    constants.maxBlockSize = 2 * constants.averageBlockSize;
    constants.maxPumpLimit = 0b100000000000000;

    Radamsa::RadamsaAlgorithm alg(seed, constants);
    alg.setAllMutationsEnabled(false);
    alg.setAllPatternsEnabled(false);
    alg.setAllGeneratorsEnabled(false);

    alg.setMutationEnabled(mutation, true);
    alg.setPatternEnabled(pattern, true);
    alg.setGeneratorEnabled("stdin", true);

    std::unique_ptr<Radamsa::Generator> gen = alg.muxAndGetGenerators();
    std::unique_ptr<Radamsa::Pattern> pat = alg.muxAndGetPatterns();
    std::unique_ptr<Radamsa::Mutation> muta = alg.muxAndGetMutations();

    Radamsa::AlgorithmParameters params{ muta.get(), gen.get(), pat.get() };
    std::string out = Radamsa::runAlgorithm(input, params);
    return std::pair<std::string, uint32_t>(out, alg.peekRandomState());
}

const std::string defaultInput = "abcde\n";
const std::string inputForLine = "a\nb\nc\nd\ne\n";

TEST_CASE("Test byte insert", "[mutation][byte]") {
    // echo "abcde" | ./radamsa -s 5 -M - -g stdin -p od -m bi
    SECTION("byte Insert once on seed 5") {
        auto result = runIndiviualMutationOnFullSystemWithPattern(5, "bi", "od", defaultInput);

        REQUIRE(result.second == 14053539);  // First ensure the random states are the same

        REQUIRE(result.first.size() == 7);
        REQUIRE((unsigned char)result.first.front() == 235);
        REQUIRE(result.first.substr(1) == defaultInput);
    }

    // echo "abcde" | ./radamsa -s 10 -M - -g stdin -p od -m bu
    SECTION("byte Insert several times on seed 10") {
        auto result = runIndiviualMutationOnFullSystemWithPattern(10, "bi", "bu", defaultInput);

        REQUIRE(result.second == 1667183);  // First ensure the random states are the same

        REQUIRE(result.first.size() == 9);
        REQUIRE((unsigned char)result.first[2] == 185);
        REQUIRE((unsigned char)result.first[4] == 126);
        REQUIRE((unsigned char)result.first[6] == 5);
    }
}

TEST_CASE("Test byte decrease", "[mutation][byte]") {
	// echo "abcde" | ./radamsa -s 5 -M - -g stdin -p od -m bed
	SECTION("byte Decrease once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "bed", "od", defaultInput);

		REQUIRE(result.second == 15403012);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first.front() == 96);
		REQUIRE(result.first.substr(1) == defaultInput.substr(1));
	}

	// echo "abcde" | ./radamsa -s 10 -M - -g stdin -p bu -m bed
	SECTION("byte Decrease several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "bed", "bu", defaultInput);

		REQUIRE(result.second == 11971524);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first.front() == 93);
		REQUIRE((unsigned char)result.first[2] == 97);
		REQUIRE((unsigned char)result.first[3] == 100);
	}

	// echo "abcde" | ./radamsa -s 25 -M - -g stdin -p bu -m bed
	SECTION("byte Decrease several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "bed", "bu", defaultInput);

		REQUIRE(result.second == 861839);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first.front() == 97);
		REQUIRE((unsigned char)result.first[2] == 98);
		REQUIRE((unsigned char)result.first[4] == 100);
	}
}

TEST_CASE("Test byte drop", "[mutation][byte]") {
	// echo "abcde" | ./radamsa -s 5 -M - -g stdin -p od -m bd
	SECTION("byte Drop once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "bd", "od", defaultInput);

		REQUIRE(result.second == 15403012);

		REQUIRE(result.first.size() == 5);
		REQUIRE((unsigned char)result.first.front() == 98);
	}

	// echo "abcde" | ./radamsa -s 11 -M - -g stdin -p bu -m bd
	SECTION("byte Drop several times on seed 11") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(11, "bd", "bu", defaultInput);

		REQUIRE(result.second == 4426659);

		REQUIRE(result.first.size() == 4);
		REQUIRE((unsigned char)result.first[1] == 100);
		REQUIRE((unsigned char)result.first[2] == 101);
	}

	// echo "abcde" | ./radamsa -s 25 -M - -g stdin -p bu -m bd
	SECTION("byte Drop several times on seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "bd", "bu", defaultInput);

		REQUIRE(result.second == 861839);

		REQUIRE(result.first.size() == 2);
		REQUIRE((unsigned char)result.first[0] == 97);
	}
}

TEST_CASE("Test byte flip", "[mutation][byte]") {
	// echo "abcde" | ./radamsa -s 5 -M - -g stdin -p od -m bf
	SECTION("byte Flip once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "bf", "od", defaultInput);

		REQUIRE(result.second == 14053539);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first.front() == 225);
	}

	// echo "abcde" | ./radamsa -s 10 -M - -g stdin -p bu -m bf
	SECTION("byte Flip several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "bf", "bu", defaultInput);

		REQUIRE(result.second == 1667183);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first[2] == 107);
		REQUIRE((unsigned char)result.first[3] == 96);
		REQUIRE((unsigned char)result.first[4] == 37);
	}

	// echo "abcde" | ./radamsa -s 25 -M - -g stdin -p bu -m bf
	SECTION("byte Flip several times on seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "bf", "bu", defaultInput);

		REQUIRE(result.second == 8119458);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first[2] == 98);
		REQUIRE((unsigned char)result.first[4] == 97);
	}
}

TEST_CASE("Test byte increase", "[mutation][byte]") {
	// echo "abcde" | ./radamsa -s 5 -M - -g stdin -p od -m bei
	SECTION("byte Increase once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "bei", "od", defaultInput);

		REQUIRE(result.second == 15403012);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first.front() == 98);
		REQUIRE(result.first.substr(1)== defaultInput.substr(1));
	}

	// echo "abcde" | ./radamsa -s 10 -M - -g stdin -p bu -m bei
	SECTION("byte Increase several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "bei", "bu", defaultInput);

		REQUIRE(result.second == 11971524);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first[0] == 101);
		REQUIRE((unsigned char)result.first[1] == 98);
		REQUIRE((unsigned char)result.first[2] == 101);
		REQUIRE((unsigned char)result.first[3] == 100);
	}

	// echo "abcde" | ./radamsa -s 25 -M - -g stdin -p bu -m bei
	SECTION("byte Increase several times on seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "bei", "bu", defaultInput);

		REQUIRE(result.second == 861839);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first[2] == 100);
		REQUIRE((unsigned char)result.first[4] == 102);
	}
}

TEST_CASE("Test byte random", "[mutation][byte]") {
	// echo "abcde" | ./radamsa -s 5 -M - -g stdin -p od -m ber
	SECTION("byte Random once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "ber", "od", defaultInput);

		REQUIRE(result.second == 14053539);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first.front() == 235);
		REQUIRE(result.first.substr(1) == defaultInput.substr(1));
	}

	// echo "abcde" | ./radamsa -s 10 -M - -g stdin -p bu -m ber
	SECTION("byte Random several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "ber", "bu", defaultInput);

		REQUIRE(result.second == 1667183);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first[2] == 185);
		REQUIRE((unsigned char)result.first[3] == 126);
		REQUIRE((unsigned char)result.first[4] == 5);
	}

	// echo "abcde" | ./radamsa -s 25 -M - -g stdin -p bu -m ber
	SECTION("byte Random several times on seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "ber", "bu", defaultInput);

		REQUIRE(result.second == 8119458);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first[2] == 115);
		REQUIRE((unsigned char)result.first[4] == 232);
	}
}

TEST_CASE("Test byte repeat", "[mutation][byte]") {
	// echo "abcde" | ./radamsa -s 5 -M - -g stdin -p od -m br
	SECTION("byte Repeat once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "br", "od", defaultInput);

		REQUIRE(result.second == 3640253);

		REQUIRE(result.first.size() == 11);
		REQUIRE((unsigned char)result.first.front() == 97);
		REQUIRE((unsigned char)result.first[1] == 97);
		REQUIRE((unsigned char)result.first[2] == 97);
		REQUIRE((unsigned char)result.first[3] == 97);
		REQUIRE((unsigned char)result.first[4] == 97);
		REQUIRE((unsigned char)result.first[5] == 97);
		REQUIRE(result.first.substr(6) == defaultInput.substr(1));
	}

	// echo "abcde" | ./radamsa -s 12 -M - -g stdin -p bu -m br
	SECTION("byte Repeat several times on seed 12") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(12, "br", "bu", defaultInput);

		REQUIRE(result.second == 15302726);

		REQUIRE(result.first.size() == 21);
		REQUIRE((unsigned char)result.first.front() == 97);
		REQUIRE((unsigned char)result.first[1] == 97);
		REQUIRE((unsigned char)result.first[2] == 97);

		REQUIRE((unsigned char)result.first[3] == 98);
		REQUIRE((unsigned char)result.first[4] == 98);
		REQUIRE((unsigned char)result.first[5] == 98);
		REQUIRE((unsigned char)result.first[6] == 98);
		REQUIRE((unsigned char)result.first[7] == 98);
		REQUIRE((unsigned char)result.first[8] == 98);
		REQUIRE((unsigned char)result.first[9] == 98);
		REQUIRE((unsigned char)result.first[10] == 98);
		REQUIRE((unsigned char)result.first[11] == 98);
		REQUIRE((unsigned char)result.first[12] == 98);
		REQUIRE((unsigned char)result.first[13] == 98);
		REQUIRE((unsigned char)result.first[14] == 98);

		REQUIRE((unsigned char)result.first[15] == 99);
		REQUIRE((unsigned char)result.first[16] == 100);

		REQUIRE((unsigned char)result.first[17] == 101);
		REQUIRE((unsigned char)result.first[18] == 101);

		REQUIRE((unsigned char)result.first[19] == 10);
		REQUIRE((unsigned char)result.first[20] == 10);
	}
}

TEST_CASE("Test byte permute", "[mutation][byte][bp]") {
	// echo "abcde" | ./radamsa -s 5 -M - -g stdin -p od -m bp
	SECTION("Byte Permute once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "bp", "od", defaultInput);

		REQUIRE(result.second == 15403012);
			
		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first.front() == 97);
		REQUIRE((unsigned char)result.first[1] == 99);
		REQUIRE((unsigned char)result.first[2] == 10);
		REQUIRE((unsigned char)result.first[3] == 98);
		REQUIRE((unsigned char)result.first[4] == 100);
		REQUIRE((unsigned char)result.first[5] == 101);
	}

	// echo "abcde" | ./radamsa -s 10 -M - -g stdin -p bu -m bp
	SECTION("Byte Permute several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "bp", "bu", defaultInput);

		REQUIRE(result.second == 11971524);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first[0] == 101);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 98);
		REQUIRE((unsigned char)result.first[3] == 99);
		REQUIRE((unsigned char)result.first[4] == 100);
		REQUIRE((unsigned char)result.first[5] == 97);
	}

	// echo "abcde" | ./radamsa -s 25 -M - -g stdin -p bu -m bp
	SECTION("Byte Permute several times on seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "bp", "bu", defaultInput);

		REQUIRE(result.second == 861839);

		REQUIRE(result.first.size() == 6);
		REQUIRE((unsigned char)result.first[2] == 100);
		REQUIRE((unsigned char)result.first[3] == 101);
		REQUIRE((unsigned char)result.first[4] == 99);
	}
}

// More test cases.
// TREE TESTS
const std::string treeInput = "((<{}>)[]\"\")\n";

TEST_CASE("Test tree delete", "[mutation][tree]") {
	SECTION("Pattern once dec with seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "td", "od", treeInput);

		REQUIRE(result.second == 15782562);

		REQUIRE(result.first.size() == 11);
		REQUIRE(result.first == "((<{}>)[])\n");
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "td", "bu", treeInput);

		REQUIRE(result.second == 1667183);

		REQUIRE(result.first.size() == 1);
		REQUIRE(result.first == "\n");
	}

	SECTION("Pattern burst with seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "td", "bu", treeInput);

		REQUIRE(result.second == 13450234);

		REQUIRE(result.first.size() == 9);
		REQUIRE(result.first == "((<>)[])\n");
	}
}

TEST_CASE("Test tree duplicate", "[mutation][tree]") {
	SECTION("Pattern once dec with seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "tr2", "od", treeInput);

		REQUIRE(result.second == 15782562);

		REQUIRE(result.first.size() == 15);
		REQUIRE(result.first == "((<{}>)[]\"\"\"\")\n");
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "tr2", "bu", treeInput);

		REQUIRE(result.second == 1667183);

		REQUIRE(result.first.size() == 35);
		REQUIRE(result.first == "((<{}{}>)(<{}{}{}><{}{}{}>)[]\"\"\"\")\n");
	}

	SECTION("Pattern burst with seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "tr2", "bu", treeInput);

		REQUIRE(result.second == 13450234);

		REQUIRE(result.first.size() == 17);
		REQUIRE(result.first == "((<{}{}>)[]\"\"\"\")\n");
	}
}

TEST_CASE("Test tree stutter", "[mutation][tree]") {
	SECTION("Pattern once dec with seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "tr", "od", treeInput);

		REQUIRE(result.second == 7433824);

		REQUIRE(result.first.size() == 15);
		REQUIRE(result.first == "((<<{}>>)[]\"\")\n");
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "tr", "bu", treeInput);

		REQUIRE(result.second == 5979228);
		REQUIRE(result.first[0] == '(');
		REQUIRE(result.first[113] == '<');
	}
}

TEST_CASE("Test tree swap one", "[mutation][tree]") {
	SECTION("Pattern once dec with seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "ts1", "od", treeInput);

		REQUIRE(result.second == 3640253);

		REQUIRE(result.first.size() == 13);
		REQUIRE(result.first == "((<{}>)[]{})\n");
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "ts1", "bu", treeInput);

		REQUIRE(result.second == 1667183);

		REQUIRE(result.first.size() == 13);
		REQUIRE(result.first == "((([]))[]\"\")\n");
	}

	SECTION("Pattern burst with seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "ts1", "bu", treeInput);

		REQUIRE(result.second == 8119458);

		REQUIRE(result.first.size() == 3);
		REQUIRE(result.first == "{}\n");
	}
}

TEST_CASE("Test tree swap two", "[mutation][tree]") {
	SECTION("Pattern once dec with seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "ts2", "od", treeInput);

		REQUIRE(result.second == 7330700);

		REQUIRE(result.first.size() == 13);
		REQUIRE(result.first == "((<\"\">)[]{})\n");
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "ts2", "bu", treeInput);

		REQUIRE(result.second == 15721807);

		REQUIRE(result.first.size() == 3);
		REQUIRE(result.first == "\"\"\n");
	}

	SECTION("Pattern burst with seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "ts2", "bu", treeInput);

		REQUIRE(result.second == 8119458);

		REQUIRE(result.first.size() == 3);
		REQUIRE(result.first == "{}\n");
	}
}

TEST_CASE("Test Num mutation") {
	std::string numInput = "abcde12345\n";

	SECTION("Pattern once dec with seed 0") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(0, "num", "od", numInput);

		REQUIRE(result.second == 5606025);

		REQUIRE(result.first.size() == 16);
		REQUIRE(result.first == "abcde4294967296\n");
	}

	SECTION("Pattern burst with seed 1000") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(1000, "num", "bu", numInput);

		REQUIRE(result.second == 4192306);

		REQUIRE(result.first.size() == 7);
		REQUIRE(result.first == "abcde1\n");
	}

	SECTION("Pattern burst with seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "num", "bu", numInput);

		REQUIRE(result.second == 14233818);

		REQUIRE(result.first.size() == 10);
		REQUIRE(result.first == "abcde3304\n");
	}
}

TEST_CASE("Test Seq Del", "[seq-delete]") {
	SECTION("Pattern once dec seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "sd", "od", defaultInput);

		REQUIRE(result.second == 14053539);

		REQUIRE(result.first.size() == 1);
		REQUIRE(result.first == "\n");
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "sd", "bu", defaultInput);

		REQUIRE(result.second == 1667183);

		REQUIRE(result.first.size() == 4);
		REQUIRE(result.first == "abd\n");
	}

	SECTION("Pattern burst with seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "sd", "bu", defaultInput);

		REQUIRE(result.second == 8119458);

		REQUIRE(result.first.size() == 6);
		auto expected = std::string("abcde\n");
		REQUIRE(result.first == expected);
	}
}

TEST_CASE("Test Seq Repeat") {
	SECTION("Pattern once dec seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "sr", "od", defaultInput);

		REQUIRE(result.second == 12065776);

		REQUIRE(result.first.size() == 2351);
		REQUIRE(result.first == "abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcde\n");
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "sr", "bu", defaultInput);

		REQUIRE(result.second == 11642100);

		REQUIRE(result.first.size() == 245);
		REQUIRE(result.first == "aaaaaaaaaaaaaabcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcde\n");
	}

	SECTION("Pattern burst with seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "sr", "bu", defaultInput);

		REQUIRE(result.second == 861839);

		REQUIRE(result.first.size() == 24);
		REQUIRE(result.first == "aaaaaaaaaaaabccccccccde\n");
	}
}

TEST_CASE("Test line delete", "[mutation][line]") {
	// echo $'a\nb\nc\nd\ne' | ./radamsa -M - -p od -m ld -s 5 -g stdin
	SECTION("Line Delete once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "ld", "od", inputForLine);

		REQUIRE(result.second == 15782562);
		REQUIRE(result.first.size() == 8);
		REQUIRE((unsigned char)result.first.front() == 98);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 99);
	}

	// echo $'a\nb\nc\nd\ne' | radamsa -M - -p bu -m ld -s 10 -g stdin
	SECTION("Line Delete several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "ld", "bu", inputForLine);

		REQUIRE(result.second == 1667183);

		REQUIRE(result.first.size() == 0);
	}
}

TEST_CASE("Test line delete dequence", "[mutation][line]") {
	// echo $'a\nb\nc\nd\ne' | ./radamsa -M - -p od -m lds -s 5 -g stdin
	SECTION("Line Delete Sequence once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "lds", "od", inputForLine);

		REQUIRE(result.second == 15403012);
		REQUIRE(result.first.size() == 2);
		REQUIRE((unsigned char)result.first.front() == 101);
		REQUIRE((unsigned char)result.first[1] == 10);
	}

	// echo $'a\nb\nc\nd\ne' | radamsa -M - -p bu -m lds -s 11 -g stdin
	SECTION("Line Delete Sequence several times on seed 11") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(11, "lds", "bu", inputForLine);

		REQUIRE(result.second == 4426659);

		REQUIRE(result.first.size() == 4);
		REQUIRE((unsigned char)result.first.front() == 98);
		REQUIRE((unsigned char)result.first[2] == 101);
		REQUIRE((unsigned char)result.first[3] == 10);
	}
}

TEST_CASE("Test line duplicate", "[mutation][line]") {
	// echo $'a\nb\nc\nd\ne' | ./radamsa -M - -p od -m lr2 -s 5 -g stdin
	SECTION("Line Duplicate once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "lr2", "od", inputForLine);

		REQUIRE(result.second == 15782562);
		REQUIRE(result.first.size() == 12);
		REQUIRE((unsigned char)result.first.front() == 97);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE(result.first.substr(2) == inputForLine);
	}

	// echo $'a\nb\nc\nd\ne' | radamsa -M - -p bu -m lr2 -s 10 -g stdin
	SECTION("Line Duplicate several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "lr2", "bu", inputForLine);

		REQUIRE(result.second == 1667183);

		REQUIRE(result.first.size() == 20);
		REQUIRE((unsigned char)result.first.front() == 97);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 97);
		REQUIRE((unsigned char)result.first[4] == 98);
		REQUIRE((unsigned char)result.first[6] == 99);
		REQUIRE((unsigned char)result.first[8] == 99);
		REQUIRE((unsigned char)result.first[10] == 99);
		REQUIRE((unsigned char)result.first[12] == 99);
		REQUIRE((unsigned char)result.first[14] == 100);
		REQUIRE((unsigned char)result.first[16] == 100);
		REQUIRE((unsigned char)result.first[18] == 101);
	}
}

TEST_CASE("Test line clone", "[mutation][line]") {
	// echo $'a\nb\nc\nd\ne' | ./radamsa -M - -p od -m li -s 5 -g stdin
	SECTION("Line Clone once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "li", "od", inputForLine);

		REQUIRE(result.second == 15403012);
		REQUIRE(result.first.size() == 12);
		REQUIRE((unsigned char)result.first[8] == 97);
		REQUIRE((unsigned char)result.first[9] == 10);
	}

	// echo $'a\nb\nc\nd\ne' | radamsa -M - -p bu -m li -s 10 -g stdin
	SECTION("Line Clone several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "li", "bu", inputForLine);

		REQUIRE(result.second == 11971524);

		REQUIRE(result.first.size() == 24);
		REQUIRE((unsigned char)result.first.front() == 97);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 98);
		REQUIRE((unsigned char)result.first[4] == 99);
		REQUIRE((unsigned char)result.first[6] == 98);
		REQUIRE((unsigned char)result.first[8] == 97);
		REQUIRE((unsigned char)result.first[10] == 99);
		REQUIRE((unsigned char)result.first[12] == 97);
		REQUIRE((unsigned char)result.first[14] == 101);
		REQUIRE((unsigned char)result.first[16] == 100);
		REQUIRE((unsigned char)result.first[18] == 98);
		REQUIRE((unsigned char)result.first[20] == 97);
		REQUIRE((unsigned char)result.first[22] == 101);
	}
}

TEST_CASE("Test line repeat", "[mutation][line]") {
	// echo $'a\nb\nc\nd\ne' | ./radamsa -M - -p od -m lr -s 2 -g stdin
	SECTION("Line Repeat once on seed 2") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(2, "lr", "od", inputForLine);

		REQUIRE(result.second == 236029);
		REQUIRE(result.first.size() == 14);
		REQUIRE((unsigned char)result.first[6] == 99);
		REQUIRE((unsigned char)result.first[7] == 10);
		REQUIRE((unsigned char)result.first[8] == 99);
		REQUIRE((unsigned char)result.first[9] == 10);
	}

	// echo $'a\nb\nc\nd\ne' | radamsa -M - -p bu -m lr -s 3 -g stdin
	SECTION("Line Repeat several times on seed 3") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(3, "lr", "bu", inputForLine);

		REQUIRE(result.second == 3620610);

		REQUIRE(result.first.size() == 48);
		REQUIRE((unsigned char)result.first[10] == 101);
		REQUIRE((unsigned char)result.first[12] == 101);
		REQUIRE((unsigned char)result.first[14] == 101);
		REQUIRE((unsigned char)result.first[16] == 101);
		REQUIRE((unsigned char)result.first[18] == 101);
		REQUIRE((unsigned char)result.first[20] == 101);
		REQUIRE((unsigned char)result.first[22] == 101);
		REQUIRE((unsigned char)result.first[24] == 101);
		REQUIRE((unsigned char)result.first[26] == 101);
		REQUIRE((unsigned char)result.first[28] == 101);
		REQUIRE((unsigned char)result.first[30] == 101);
		REQUIRE((unsigned char)result.first[32] == 101);
		REQUIRE((unsigned char)result.first[34] == 101);
		REQUIRE((unsigned char)result.first[36] == 101);
		REQUIRE((unsigned char)result.first[38] == 101);
		REQUIRE((unsigned char)result.first[40] == 101);
		REQUIRE((unsigned char)result.first[42] == 101);
		REQUIRE((unsigned char)result.first[44] == 101);
		REQUIRE((unsigned char)result.first[46] == 101);
		REQUIRE((unsigned char)result.first[47] == 10);

	}
}

TEST_CASE("Test line swap", "[mutation][line]") {
	// echo $'a\nb\nc\nd\ne' | ./radamsa -M - -p od -m ls -s 5 -g stdin
	SECTION("Line Swap once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "ls", "od", inputForLine);

		REQUIRE(result.second == 15782562);
		REQUIRE(result.first.size() == 10);
		REQUIRE((unsigned char)result.first.front() == 98);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 97);
	}

	// echo $'a\nb\nc\nd\ne' | radamsa -M - -p bu -m ls -s 10 -g stdin
	SECTION("Line Swap several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "ls", "bu", inputForLine);

		REQUIRE(result.second == 1667183);

		REQUIRE(result.first.size() == 10);
		REQUIRE((unsigned char)result.first.front() == 99);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 97);
		REQUIRE((unsigned char)result.first[4] == 98);
		REQUIRE((unsigned char)result.first[6] == 101);
		REQUIRE((unsigned char)result.first[8] == 100);
	}
}

TEST_CASE("Test line permute", "[mutation][line][lp]") {
	// echo $'a\nb\nc\nd\ne' | ./radamsa -M - -p od -m lp -s 5 -g stdin
	SECTION("Line Permute once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "lp", "od", inputForLine);

		REQUIRE(result.second == 7433824);
		REQUIRE(result.first.size() == 10);
		REQUIRE((unsigned char)result.first.front() == 98);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 97);
	}

	// echo $'a\nb\nc\nd\ne' | radamsa -M - -p bu -m lp -s 10 -g stdin
	SECTION("Line Permute several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "lp", "bu", inputForLine);

		REQUIRE(result.second == 12454328);

		REQUIRE(result.first.size() == 10);
		REQUIRE((unsigned char)result.first.front() == 100);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 99);
		REQUIRE((unsigned char)result.first[4] == 98);
		REQUIRE((unsigned char)result.first[6] == 97);
		REQUIRE((unsigned char)result.first[8] == 101);
	}

	// echo $'a\nb\nc\nd\ne\nf\ng\nh\ni\nj' | radamsa -M - -p bu -m lp -s 12 -g stdin
	SECTION("Line Permute several times on seed 12") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(12, "lp", "bu", inputForLine+"f\ng\nh\ni\nj\n");

		REQUIRE(result.second == 14540178);

		REQUIRE(result.first.size() == 20);
		REQUIRE((unsigned char)result.first.front() == 98);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 102);
		REQUIRE((unsigned char)result.first[4] == 100);
		REQUIRE((unsigned char)result.first[6] == 105);
		REQUIRE((unsigned char)result.first[8] == 99);
		REQUIRE((unsigned char)result.first[10] == 101);
		REQUIRE((unsigned char)result.first[12] == 104);
		REQUIRE((unsigned char)result.first[14] == 97);
		REQUIRE((unsigned char)result.first[16] == 103);
		REQUIRE((unsigned char)result.first[18] == 106);
	}
}

TEST_CASE("Test line insert", "[mutation][line]") {
	// echo $'a\nb\nc\nd\ne' | ./radamsa -M - -p od -m lis -s 5 -g stdin
	SECTION("Line Insert once on seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "lis", "od", inputForLine);

		REQUIRE(result.second == 171984);
		REQUIRE(result.first.size() == 12);
		REQUIRE((unsigned char)result.first[6] == 99);
		REQUIRE((unsigned char)result.first[7] == 10);
		REQUIRE((unsigned char)result.first[8] == 100);
		REQUIRE((unsigned char)result.first[9] == 10);
		REQUIRE((unsigned char)result.first[10] == 101);
	}

	// echo $'a\nb\nc\nd\ne' | radamsa -M - -p bu -m lis -s 10 -g stdin
	SECTION("Line Insert several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "lis", "bu", inputForLine);

		REQUIRE(result.second == 5153793);

		REQUIRE(result.first.size() == 22);
		REQUIRE((unsigned char)result.first.front() == 100);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 98);
		REQUIRE((unsigned char)result.first[4] == 97);
		REQUIRE((unsigned char)result.first[6] == 98);
		REQUIRE((unsigned char)result.first[8] == 98);
		REQUIRE((unsigned char)result.first[10] == 99);
		REQUIRE((unsigned char)result.first[12] == 99);
		REQUIRE((unsigned char)result.first[14] == 100);
		REQUIRE((unsigned char)result.first[16] == 100);
		REQUIRE((unsigned char)result.first[18] == 100);
		REQUIRE((unsigned char)result.first[20] == 101);
	}
}

TEST_CASE("Test line replace", "[mutation][line]") {
	// echo $'a\nb\nc\nd\ne' | ./radamsa -M - -p od -m lrs -s 4 -g stdin
	SECTION("Line Replace once on seed 4") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(4, "lrs", "od", inputForLine);

		REQUIRE(result.second == 9940886);
		REQUIRE(result.first.size() == 10);
		REQUIRE((unsigned char)result.first[4] == 101);
		REQUIRE((unsigned char)result.first[5] == 10);
	}

	// echo $'a\nb\nc\nd\ne' | radamsa -M - -p bu -m lrs -s 10 -g stdin
	SECTION("Line Replace several times on seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "lrs", "bu", inputForLine);

		REQUIRE(result.second == 5153793);

		REQUIRE(result.first.size() == 10);
		REQUIRE((unsigned char)result.first.front() == 98);
		REQUIRE((unsigned char)result.first[1] == 10);
		REQUIRE((unsigned char)result.first[2] == 98);
		REQUIRE((unsigned char)result.first[4] == 99);
		REQUIRE((unsigned char)result.first[6] == 99);
		REQUIRE((unsigned char)result.first[8] == 100);
	}
}

const std::string fuseString = "A long enough string that will hopefully do something interesting in the fuse mutations\n";

TEST_CASE("Test fuse-this", "[mutation][fuse-this-full]") {
	// echo "A long enough string that will hopefully do something interesting in the fuse mutations" | ./radamsa -s 16 -p od -m ft -g stdin -M -
	SECTION("Fuse a long string on seed 16") {
		auto res = runIndiviualMutationOnFullSystemWithPattern(16, "ft", "od", fuseString);

		REQUIRE(res.second == 3760497);
		REQUIRE(res.first == "A long enough sting in the fuse mutations\n"); // s'tring fused to interes'ting
	}

	SECTION("Fuse a long string on seed 8") {
		auto res = runIndiviualMutationOnFullSystemWithPattern(8, "ft", "od", fuseString);

		REQUIRE(res.second == 15406134);
		REQUIRE(res.first == "A long enpefully do something interesting in the fuse mutations\n");
	}

	SECTION("Burst Fuse a long string on seed 8") {
		auto res = runIndiviualMutationOnFullSystemWithPattern(8, "ft", "bu", fuseString);

		REQUIRE(res.second == 12520843);
		REQUIRE(res.first == "A lonse mutations\n");
	}
}

TEST_CASE("test ascii bad", "[mutation][ascii]") {
	SECTION("Pattern once dec with seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "ab", "od", defaultInput);

		REQUIRE(result.second == 14053539);

		REQUIRE(result.first.size() == 7);
		auto expected = std::string{ 'a', 'b', 'c', 'd', 'e', '\n', '\0' };
		REQUIRE(result.first == expected);
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "ab", "bu", defaultInput);

		REQUIRE(result.second == 5211399);
		
		REQUIRE(result.first.size() == 246);
		REQUIRE(result.first == "abcd$&\\x0d$PATH$`$&%\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\ad$`%n!!!!\'xc!xcalc\\x0d;xcalc&#000;%#x\"xcalcaaaNaN+inf\'xcalc$!!$`+inf\\x0a!xcalc$(xcalc)!xcalc$&$PATH");
	}

	SECTION("Pattern burst with seed 2500") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(2500, "ab", "bu", defaultInput);

		REQUIRE(result.second == 12922009);

		REQUIRE(result.first.size() == 49);
		REQUIRE(result.first == "abc$&!!!!$+$'$!!aaaa%d%n\\r\\n%d\\r\\n!xcalc\\u0000de\n");
	}
}

TEST_CASE("Test fuse-next", "[mutation][fuse-next-full]") {
	// echo "A long enough string that will hopefully do something interesting in the fuse mutations" | ./radamsa -s 8 -p od -m fn -g stdin -M -
	SECTION("Fuse a long string on seed 8") {
		auto res = runIndiviualMutationOnFullSystemWithPattern(8, "fn", "od", fuseString);

		REQUIRE(res.second == 16237365);
		REQUIRE(res.first == "A long enough string enough string that will hopefully do something interesting in the fuse mutations\n");
	}

	// echo "A long enough string that will hopefully do something interesting in the fuse mutations" | ./radamsa -s 16 -p bu -m fn -g stdin -M -
	SECTION("Burst Fuse a long string on seed 16") {
		auto res = runIndiviualMutationOnFullSystemWithPattern(16, "fn", "bu", fuseString);

		REQUIRE(res.second == 1123981);
		REQUIRE(res.first == "A long ens\n");
	}
}

TEST_CASE("Test fuse-old", "[mutation][fuse-old-full]") {
	// echo "A long enough string that will hopefully do something interesting in the fuse mutations" | ./radamsa -s 16 -p od -m fo -g stdin -M -
	SECTION("Fuse a long string on seed 16") {
		auto res = runIndiviualMutationOnFullSystemWithPattern(16, "fo", "od", fuseString);

		REQUIRE(res.second == 5482099);
		REQUIRE(res.first == "A long enough string tg enough string that will hopefully do something interesting in the fuse mutations\n");
	}

	SECTION("Burst Fuse a long string on seed 32") {
		auto res = runIndiviualMutationOnFullSystemWithPattern(32, "fo", "bu", fuseString);

		REQUIRE(res.second == 12422581);
		REQUIRE(res.first == "Ang enough string thathat that will hopefully do something interesting interesting in the fuse mutations\n");
	}
}

TEST_CASE("test utf8 widen", "[mutation][utf8]") {
	SECTION("Pattern once dec with seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "uw", "od", defaultInput);

		REQUIRE(result.second == 15403012);

		REQUIRE(result.first.size() == 6);
		auto expected = std::string{ 'a', 'b', 'c', 'd', 'e', '\n' };
		REQUIRE(result.first == expected);
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "uw", "bu", defaultInput);

		REQUIRE(result.second == 11971524);

		REQUIRE(result.first.size() == 7);
		REQUIRE(result.first[5] == -64);
		REQUIRE(result.first[6] == -118);
	}

	SECTION("Pattern burst with seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "uw", "bu", defaultInput);

		REQUIRE(result.second == 861839);

		REQUIRE(result.first.size() == 7);
		REQUIRE(result.first[5] == -64);
		REQUIRE(result.first[6] == -118);
	}
}

TEST_CASE("test utf8 insert", "[mutation][utf8][!mayfail]") {
	SECTION("Pattern once dec with seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "ui", "od", defaultInput);

		REQUIRE(result.second == 14053539);

		auto expected = std::string{ '\xE2', '\x80', '\x82', 'a','b','c','d','e','\n' };
		REQUIRE(result.first == expected);
	}

	SECTION("Pattern burst with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "ui", "bu", defaultInput);

		REQUIRE(result.second == 1667183);

		CHECK(result.first.size() == 16);
		REQUIRE(result.first.front() == 'a');
		REQUIRE(result.first.back() == '\n');
	}

	SECTION("Pattern burst with seed 25") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(25, "ui", "bu", defaultInput);

		REQUIRE(result.second == 8119458);

		CHECK(result.first.size() == 13);
		REQUIRE(result.first.front() == 'a');
		REQUIRE(result.first.at(2) == (char)'\xf3');
		REQUIRE(result.first.at(5) == (char)'\x8f');
		REQUIRE(result.first.at(8) == (char)'\xE2');
		REQUIRE(result.first.at(10) == (char)'\x80');
		REQUIRE(result.first.back() == '\n');
	}
}

const std::string xmlString = "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty/>\n<emptyWithAttribute thing=\"boxcar\"/>\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n";

TEST_CASE("test xp-dup", "[mutation][xp][xp-dup-full]") {
	// echo 'xmlString' | radamsa.exe -p od -m xp=9 -g stdin -s 11
	SECTION("Pattern once dec with seed 11") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(11, "xp", "od", xmlString);

		REQUIRE(result.second == 12889434);

		REQUIRE(result.first == "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty /><empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n");
	}

	SECTION("Pattern once dec with seed 35") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(35, "xp", "od", xmlString);

		REQUIRE(result.second == 14643585);

		REQUIRE(result.first == "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3><nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n");
	}
}

TEST_CASE("test xp-swap", "[mutation][xp][xp-swap-full]") {
	SECTION("Pattern once dec with seed 5") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(5, "xp", "od", xmlString);

		REQUIRE(result.second == 8795576);

		REQUIRE(result.first == "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<abnormalTag>\n</normalTag>\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</enclosingTag>\n");
	}

	SECTION("Pattern once dec with seed 7") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(7, "xp", "od", xmlString);

		REQUIRE(result.second == 12339710);

		REQUIRE(result.first == "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<abnormalTag>\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<empty />\n</enclosingTag>\n");
	}
}

TEST_CASE("test xp-pump", "[mutation][xp][xp-pump-full]") {
	SECTION("Pattern once dec with seed 1200") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(1200, "xp", "od", xmlString);

		REQUIRE(result.second == 10925509);

		REQUIRE(result.first == "<enclosingTag>\n<normalTag><normalTag><normalTag><normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag><text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag><text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag><text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n");
	}

	SECTION("Pattern once dec with seed 10") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(10, "xp", "od", xmlString);
		std::string expected = "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n";
		REQUIRE(result.second == 11056237);

		REQUIRE(result.first == expected);
	}
}

TEST_CASE("test xp-repeat", "[mutation][xp][xp-repeat-full]") {
	SECTION("Pattern once dec with seed 98") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(98, "xp", "od", xmlString);

		REQUIRE(result.second == 6723811);

		REQUIRE(result.first == "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1><nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1><nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n");
	}

	SECTION("Pattern once dec with seed 84") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(84, "xp", "od", xmlString);

		REQUIRE(result.second == 11594314);

		REQUIRE(result.first == "<enclosingTag>\n<normalTag>\n<text>this is some text</text><text>this is some text</text><text>this is some text</text><text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n");
	}
}

TEST_CASE("test xp-insert", "[mutation][xp][xp-insert-full]") {
	SECTION("Pattern once dec with seed 200") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(200, "xp", "od", xmlString);

		REQUIRE(result.second == 16201580);

		REQUIRE(result.first == "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag><emptyWithAttribute />\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag>\n");
	}

	SECTION("Pattern once dec with seed 1400") {
		auto result = runIndiviualMutationOnFullSystemWithPattern(1400, "xp", "od", xmlString);

		REQUIRE(result.second == 12797502);

		REQUIRE(result.first == "<enclosingTag>\n<normalTag>\n<text>this is some text</text>\n<nested1>\n<nested2>\n<nested3>this is some very nested text</nested3>\n</nested2>\n</nested1>\n</normalTag>\n<normalTag>\n<empty />\n<emptyWithAttribute thing=\"boxcar\" />\n<tagWithAttribute thing1=\"water\" thing2='melon'>Boy it sure is nice out today</tagWithAttribute>\n</normalTag>\n<abnormalTag>\n</enclosingTag><enclosingTag>\n</enclosingTag>");
	}
}
