#include "catch.hpp"
#include "mutation/asciiBadMutation.h"
#include "mock/mock_random.h"

using namespace Radamsa;

// 8 mutations run on radamsa:
TEST_CASE("ascii_bad 1") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	AsciiBadMutation m = AsciiBadMutation(1, constants, mock);

	mock.expectedNext(2);
	mock.expectedNext(2);
	mock.expectedNext(10);
	mock.expectedNext(74);
	mock.expectedNext(3);
	mock.expectedNext(-1);

	std::string in = "And I said \"What the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	std::string expected = "And I said \"What the f Janice ? \" be\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\acause she said she was \'having a medical emergency\' or \'something.";
	std::string actual = m.mutate(in);

	REQUIRE(actual == expected);
}

TEST_CASE("ascii_bad 2") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	AsciiBadMutation m = AsciiBadMutation(1, constants, mock);

	mock.expectedNext(1);
	mock.expectedNext(0);
	mock.expectedNext(1);
	mock.expectedNext(15);
	mock.expectedNext(2);
	mock.expectedNext(28);
	mock.expectedNext(28);
	mock.expectedNext(10);
	mock.expectedNext(15);
	mock.expectedNext(19);
	mock.expectedNext(21);
	mock.expectedNext(24);
	mock.expectedNext(16);
	mock.expectedNext(23);
	mock.expectedNext(28);
	mock.expectedNext(2);
	mock.expectedNext(31);
	mock.expectedNext(13);
	mock.expectedNext(5);
	mock.expectedNext(27);
	mock.expectedNext(1);

	std::string in = "And I said \"What the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	std::string expected = "And I said \"W$&%#x'xcalc$1%s$+$!!\\r!!+inf\\x0d\\r\\n$(xcalc)$+$+%shat the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	std::string actual = m.mutate(in);

	REQUIRE(expected == actual);
}

TEST_CASE("ascii_bad 3") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	AsciiBadMutation m = AsciiBadMutation(1, constants, mock);

	mock.expectedNext(4);
	mock.expectedNext(1);
	mock.expectedNext(3);
	mock.expectedNext(8);
	mock.expectedNext(4);
	mock.expectedNext(18);
	mock.expectedNext(20);
	mock.expectedNext(26);
	mock.expectedNext(28);
	mock.expectedNext(31);
	mock.expectedNext(25);
	mock.expectedNext(31);
	mock.expectedNext(11);
	mock.expectedNext(-1);

	std::string in = "And I said \"What the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	std::string expected = "And I said \"What the f Janice ? \" because she said she was \'having a medical emergency\' or!xcalc$1&#000;$1$+\\u0000NaN\\x0a%p";
	std::string actual = m.mutate(in);

	REQUIRE(expected == actual);
}

TEST_CASE("ascii_bad 4") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	AsciiBadMutation m = AsciiBadMutation(1, constants, mock);

	mock.expectedNext(1);
	mock.expectedNext(3);
	mock.expectedNext(1);

	std::string in = "And I said \"What the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	char expected[] = "And I said \"What the f Janice ? \0\" because she said she was \'having a medical emergency\' or \'something.";
	std::string actual = m.mutate(in);

	REQUIRE(actual.size() == actual.size());
	for (int i = 0; i < actual.size(); i++) {
		REQUIRE(expected[i] == actual[i]);
	}
}

TEST_CASE("ascii_bad 5") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	AsciiBadMutation m = AsciiBadMutation(1, constants, mock);

	mock.expectedNext(1);
	mock.expectedNext(2);
	mock.expectedNext(1);
	mock.expectedNext(4);
	mock.expectedNext(1);

	std::string in = "And I said \"What the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	std::string expected = "And I said \"What\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	std::string actual = m.mutate(in);

	REQUIRE(expected == actual);
}

TEST_CASE("ascii_bad 6") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	AsciiBadMutation m = AsciiBadMutation(1, constants, mock);

	mock.expectedNext(1);
	mock.expectedNext(0);
	mock.expectedNext(4);
	mock.expectedNext(2);
	mock.expectedNext(11);
	mock.expectedNext(13);
	mock.expectedNext(20);
	mock.expectedNext(-1);

	std::string in = "And I said \"What the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	std::string expected = "And I said \"WhatNaN'xcalc!xcalc the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	std::string actual = m.mutate(in);

	REQUIRE(expected == actual);
}
