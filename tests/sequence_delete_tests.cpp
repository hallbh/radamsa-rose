#include "catch.hpp"
#include "mock/mock_random.h"
#include "mutation/sequenceDeleteMutation.h"

using namespace Radamsa;

TEST_CASE("Sequence Delete") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	SequenceDeleteMutation s = SequenceDeleteMutation(12, constants, mock);

	SECTION("spaghetti sce") {
		mock.expectedNext(11);
		mock.expectedNext(2);
		mock.expectedNext(2);
		std::string expected = "spaghetti sce";

		REQUIRE(s.mutate("spaghetti sauce") == expected);
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("spaghetti ce") {
		mock.expectedNext(10);
		mock.expectedNext(3);
		mock.expectedNext(2);
		std::string expected = "spaghetti ce";
		REQUIRE(s.mutate("spaghetti sauce") == expected);
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("spagetti sauce") {
		mock.expectedNext(4);
		mock.expectedNext(1);
		mock.expectedNext(2);
		std::string expected = "spagetti sauce";
		REQUIRE(s.mutate("spaghetti sauce") == expected);
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("spagtti sauce") {
		mock.expectedNext(4);
		mock.expectedNext(2);
		mock.expectedNext(2);
		std::string expected = "spagtti sauce";
		REQUIRE(s.mutate("spaghetti sauce") == expected);
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("spaghetti ") {
		mock.expectedNext(10);
		mock.expectedNext(5);
		mock.expectedNext(2);
		std::string expected = "spaghetti ";
		REQUIRE(s.mutate("spaghetti sauce") == expected);
		REQUIRE(mock.allExpectedCallsMade());
	}
}