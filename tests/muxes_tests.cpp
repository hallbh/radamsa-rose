#include "catch.hpp"
#include <string>
#include <algorithm>
#include "generator/muxGenerator.h"
#include "pattern/muxPattern.h"
#include "mutation/muxFuzzer.h"


class G1 : public Radamsa::Generator {
public:
	int calls;
	G1() : Generator(0, "G1") {
		this->calls = 0;
	}
	std::deque<std::string> generate(std::string input) {
		int chunk = input.size() / 2;
		std::deque<std::string> out;
		out.push_back(input.substr(0, chunk));
		out.push_back(input.substr(chunk, input.length() - chunk));
		this->calls++;
		return out;
	}
};


class G2 : public Radamsa::Generator {
public:
	int calls;
	G2() : Generator(2, "G2") {
		this->calls = 0;
	}

	std::deque<std::string> generate(std::string input) {
		int chunk = input.size() / 2;
		std::deque<std::string> out;
		out.push_back(input.substr(0, chunk));
		out.push_back(input.substr(chunk, input.length() - chunk));
		this->calls++;
		return out;
	}
};


class G3 : public Radamsa::Generator {
public:
	int calls;
	G3() : Generator(4, "G3") {
		this->calls = 0;
	}

	std::deque<std::string> generate(std::string input) {
		int chunk = input.size() / 2;
		std::deque<std::string> out;
		out.push_back(input.substr(0, chunk));
		out.push_back(input.substr(chunk, input.length() - chunk));
		this->calls++;
		return out;
	}
};


class G4 : public Radamsa::Generator {
public:
	int calls;
	G4() : Generator(6, "G4") {
		this->calls = 0;
	}

	std::deque<std::string> generate(std::string input) {
		int chunk = input.size() / 2;
		std::deque<std::string> out;
		out.push_back(input.substr(0, chunk));
		out.push_back(input.substr(chunk, input.length() - chunk));
		this->calls++;
		return out;
	}
};


class G5 : public Radamsa::Generator {
public:
	int calls;
	G5() : Generator(10, "G5") {
		this->calls = 0;
	}

	std::deque<std::string> generate(std::string input) {
		int chunk = input.size() / 2;
		std::deque<std::string> out;
		out.push_back(input.substr(0, chunk));
		out.push_back(input.substr(chunk, input.length() - chunk));
		this->calls++;
		return out;
	}
};


TEST_CASE("Muxes can sort generator list") {
	struct radamsa_constants constants = { 0 };
	Radamsa::Random rand(0);

	G1 g1;
	G2 g2;
	G3 g3;
	G4 g4;
	G5 g5;

	Radamsa::muxGenerator mux(0, constants, rand);
	mux.addGenerator(&g1);
	mux.addGenerator(&g2);
	mux.addGenerator(&g3);
	mux.addGenerator(&g4);
	mux.addGenerator(&g5);

	Radamsa::Generator* gen = &mux;

	std::deque<std::string> chunks = gen->generate("ab");
	REQUIRE(chunks.size() == 2);
	REQUIRE(chunks.front() == "a");
	REQUIRE(chunks.back() == "b");

}


class P1 : public Radamsa::Pattern {
public:
	int calls;
	P1(Radamsa::Random rand) : Pattern(0, radamsa_constants { 0 }, rand, "P1") {
		this->calls = 0;
	}
	std::deque<std::string> run(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
		this->calls++;
		inputChunks.pop_front();
		return inputChunks;
	}
	std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
		return inputChunks;
	}
};

class P2 : public Radamsa::Pattern {
public:
	int calls;
	P2(Radamsa::Random rand) : Pattern(1, radamsa_constants { 0 }, rand, "P2") {
		this->calls = 0;
	}
	std::deque<std::string> run(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
		this->calls++;
		inputChunks.pop_front();
		return inputChunks;
	}
	std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
		return inputChunks;
	}
};

class P3 : public Radamsa::Pattern {
public:
	int calls;
	P3(Radamsa::Random rand) : Pattern(8, radamsa_constants { 0 }, rand, "P3") {
		this->calls = 0;
	}
	std::deque<std::string> run(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
		this->calls++;
		inputChunks.pop_front();
		return inputChunks;
	}
	std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
		return inputChunks;
	}
};

class P4 : public Radamsa::Pattern {
public:
	int calls;
	P4(Radamsa::Random rand) : Pattern(3, radamsa_constants { 0 }, rand, "P4") {
		this->calls = 0;
	}
	std::deque<std::string> run(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
		this->calls++;
		inputChunks.pop_front();
		return inputChunks;
	}
	std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
		return inputChunks;
	}
};


TEST_CASE("Muxes can sort pattern list") {
	struct radamsa_constants constants = { 0 };
	Radamsa::Random rand(0);

	P1 p1(rand);
	P2 p2(rand);
	P3 p3(rand);
	P4 p4(rand);

	Radamsa::muxPattern mux(0, constants, rand);
	mux.addPattern(&p1);
	mux.addPattern(&p2);
	mux.addPattern(&p3);
	mux.addPattern(&p4);


	Radamsa::Pattern* pat = &mux;

	std::deque<std::string> in;
	in.push_back("a");
	in.push_back("b");
	REQUIRE(in.front() == "a");
	REQUIRE(in.back() == "b");

	std::deque<std::string> out = pat->run(in, NULL);
	REQUIRE(out.front() == "b");
	REQUIRE(out.size() == std::size_t(1));

}



class M1 : public Radamsa::Mutation {
public:
	int calls;
	M1() : Mutation(1, 6, "1") {
		this->calls = 0;
	}
	std::string mutate(std::string input) {
		input.insert(0, "\x00");
		return input;
	}
	int nextDelta() {
		return 0;
	}
};


class M2 : public Radamsa::Mutation {
public:
	int calls;
	M2() : Mutation(2, 3, "2") {
		this->calls = 0;
	}
	std::string mutate(std::string input) {
		input.insert(0, "\x00");
		return input;
	}
	int nextDelta() {
		return 0;
	}
};

class M3 : public Radamsa::Mutation {
public:
	int calls;
	M3() : Mutation(3, 5, "3") {
		this->calls = 0;
	}
	std::string mutate(std::string input) {
		input.insert(0, "\x00");
		return input;
	}
	int nextDelta() {
		return 0;
	}
};

class M4 : public Radamsa::Mutation {
public:
	int calls;
	M4() : Mutation(4, 8, "4") {
		this->calls = 0;
	}
	std::string mutate(std::string input) {
		input.insert(0, "\x00");
		return input;
	}
	int nextDelta() {
		return 0;
	}
};

class M5 : public Radamsa::Mutation {
public:
	int calls;
	M5() : Mutation(5, 4, "5") {
		this->calls = 0;
	}
	std::string mutate(std::string input) {
		input.insert(0, "\x00");
		return input;
	}
	int nextDelta() {
		return 0;
	}
};



TEST_CASE("Muxes can sort mutation list") {
	struct radamsa_constants constants = { 0 };
	Radamsa::Random rand(0);

	M1 p1;
	M2 p2;
	M3 p3;
	M4 p4;
	M5 p5;

	Radamsa::muxFuzzer fuzz(0, constants, rand);
	fuzz.addMutation(&p1);
	fuzz.addMutation(&p2);
	fuzz.addMutation(&p3);
	fuzz.addMutation(&p4);
	fuzz.addMutation(&p5);


	Radamsa::Mutation* pat = &fuzz;

	std::string in = "ab";

	std::string out = pat->mutate(in);
}