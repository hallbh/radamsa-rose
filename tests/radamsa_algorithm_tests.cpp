#include "catch.hpp"
#include "radamsa_algorithm.h"

TEST_CASE("Test radamsa_algorithm has enabled all of radamsa's mutations by default", "[algorithm, mutation]") {
	struct radamsa_constants constants { 0 };
	Radamsa::RadamsaAlgorithm rad(0, constants);

	REQUIRE(rad.isMutationEnabled("lr2"));
}

TEST_CASE("Test radamsa_algorithm muxes mutations", "[algorithm, mutation]") {
	struct radamsa_constants constants { 0 };
	Radamsa::RadamsaAlgorithm rad(0, constants);

	std::unique_ptr<Radamsa::Mutation> ptr = rad.muxAndGetMutations();

	REQUIRE(ptr->priority == 0);
}