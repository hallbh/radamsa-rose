#include "catch.hpp"
#include "mutation/line/line_mutation.h"

class TestLineMutationImpl : public Radamsa::AbstractLineMutation {
public:
	std::vector<std::string> foundLines;
	TestLineMutationImpl(Radamsa::Random rand) : AbstractLineMutation(0, radamsa_constants{ 0 }, rand, "Impl") {}
protected:
	void performOperation(std::vector<std::string>& lines) {
		this->foundLines = lines;
	}
};

TEST_CASE("Test abstract line operation with one line", "[mutation][line][abstract]") {
	std::string out;
	Radamsa::AbstractLineMutation *muta = new TestLineMutationImpl(Radamsa::Random(0));
	REQUIRE(muta->nextDelta() == -1);

	SECTION("Do not mutate empty line") {
		out = muta->mutate("");
		REQUIRE(out.empty());
		REQUIRE(muta->nextDelta() == -1);
	}

	SECTION("Mutates line with at least one character") {
		SECTION("Lineop on one character") {
			out = muta->mutate("a");
			REQUIRE(out == "a");
		}

		SECTION("Lineop on two characters") {
			out = muta->mutate("ab");
			REQUIRE(out == "ab");
		}

		REQUIRE(muta->nextDelta() == 1);
	}

	SECTION("Does not mutate a line starting with null byte") {
		std::string in("\x00" "abc", 4);
		out = muta->mutate(in);
		REQUIRE(out.size() == 4);
		REQUIRE(muta->nextDelta() == -1);
	}

	SECTION("Does not mutate a line with null byte") {
		SECTION("Does not mutate a line starting with a null") {
			std::string in("\x00" "abc", 4);
			out = muta->mutate(in);
		}
		SECTION("Does not mutate a line ending with a null") {
			std::string in("abc" "\x00", 4);
			out = muta->mutate(in);
		}
		SECTION("Does not mutate a line containing null") {
			std::string in("ab" "\x00" "c", 4);
			out = muta->mutate(in);
		}

		REQUIRE(out.size() == 4);
		REQUIRE(muta->nextDelta() == -1);
	}

	SECTION("Does not mutate binary-ish data") {
		out = muta->mutate("\x01");
		REQUIRE(muta->nextDelta() == 1);
		
		out = muta->mutate("\x7F");
		REQUIRE(muta->nextDelta() == 1);

		out = muta->mutate("\x80");
		REQUIRE(muta->nextDelta() == -1);

		out = muta->mutate("\xFE");
		REQUIRE(muta->nextDelta() == -1);

		out = muta->mutate("\xFF");
		REQUIRE(muta->nextDelta() == -1);

		out = muta->mutate("abcde\xAF");
		REQUIRE(muta->nextDelta() == -1);
	}

	SECTION("Test line mutation assumes text after 8 characters") {
		out = muta->mutate("1234567\x80");
		REQUIRE(muta->nextDelta() == -1);

		out = muta->mutate("12345678\x80");
		REQUIRE(muta->nextDelta() == 1);

		std::string in1("1234567\x00", 8);
		out = muta->mutate(in1);
		REQUIRE(muta->nextDelta() == -1);

		out = muta->mutate("1234567\x00");
		REQUIRE(muta->nextDelta() == 1);

		std::string in2("12345678\x00", 9);
		out = muta->mutate(in2);
		REQUIRE(muta->nextDelta() == 1);
	}

	delete muta;
}

TEST_CASE("Test abstract line mutation with two lines", "[mutation][line][abstract]") {
	std::string out;
	TestLineMutationImpl muta(Radamsa::Random(0));

	SECTION("Check splits on two empty lines") {
		out = muta.mutate("\n");
		REQUIRE(muta.foundLines.size() == 1);
		REQUIRE(out.size() == 1);
		REQUIRE(muta.nextDelta() == 1);
	}

	SECTION("Check splits on two valid lines") {
		out = muta.mutate("a\nb");
		REQUIRE(muta.foundLines.size() == 2);
		REQUIRE(out.size() == 3);
		REQUIRE(muta.nextDelta() == 1);
	}

	SECTION("Check does not mutate if first line is invalid") {
		out = muta.mutate("\x80\nb");
		REQUIRE(muta.foundLines.empty());
		REQUIRE(out.size() == 3);
		REQUIRE(muta.nextDelta() == -1);
	}

	SECTION("Check does mutate if only second line is invalid") {
		out = muta.mutate("a\n\x80");
		REQUIRE(muta.foundLines.size() == 2);
		REQUIRE(out.size() == 3);
		REQUIRE(muta.nextDelta() == 1);
	}

	SECTION("Ensure does mutate if first line has at least 8 valid characters") {
		out = muta.mutate("12345678\xFF\n\x80\tabcdefg");
		REQUIRE(muta.foundLines.size() == 2);
		REQUIRE(out.size() == 19);
		REQUIRE(muta.nextDelta() == 1);
	}
}

TEST_CASE("Test abstract line mutation with multiple lines", "[mutation][line][abstract]") {
	std::string out;
	TestLineMutationImpl muta(Radamsa::Random(0));

	SECTION("Check splits on five empty lines") {
		out = muta.mutate("\n\n\n\n");
		REQUIRE(muta.foundLines.size() == 4);
		REQUIRE(muta.foundLines.front() == "\n");
		REQUIRE(muta.foundLines.back() == "\n");
		REQUIRE(out.size() == 4);
		REQUIRE(muta.nextDelta() == 1);
	}

	SECTION("Check splits on three valid lines") {
		out = muta.mutate("a\r\nb\n\rc");
		REQUIRE(muta.foundLines.size() == 3);
		REQUIRE(out.size() == 7);
		REQUIRE(muta.nextDelta() == 1);
	}

	SECTION("Check does not mutate if first line is invalid") {
		out = muta.mutate("\x80\nb\nc");
		REQUIRE(muta.foundLines.empty());
		REQUIRE(out.size() == 5);
		REQUIRE(muta.nextDelta() == -1);
	}

	SECTION("Check does mutate if only second line is invalid") {
		out = muta.mutate("a\n\x80\n\r\xFF");
		REQUIRE(muta.foundLines.size() == 3);
		REQUIRE(muta.foundLines.front() == "a\n");
		REQUIRE(muta.foundLines[1] == "\x80\n");
		REQUIRE(muta.foundLines.back() == "\r\xFF");
		REQUIRE(out.size() == 6);
		REQUIRE(muta.nextDelta() == 1);
	}

	SECTION("Check does mutate if a non-first line contains null") {
		std::string in("a\n2\n\t\n\x00\n\x80", 9);
		out = muta.mutate(in);
		REQUIRE(muta.foundLines.size() == 5);
		REQUIRE(out.size() == 9);
		REQUIRE(muta.nextDelta() == 1);
	}

	SECTION("Ensure does mutate if first line has at least 8 valid characters") {
		out = muta.mutate("12345678\xFF\n\x80\tabcdefg\nThis is a third line");
		REQUIRE(muta.foundLines.size() == 3);
		REQUIRE(muta.nextDelta() == 1);
	}
}

TEST_CASE("Test consecutive calls to nextDelta", "[mutation][line][abstract]") {
	Radamsa::AbstractLineMutation* muta = new TestLineMutationImpl(Radamsa::Random(0));
	REQUIRE(muta->nextDelta() == -1);

	std::string out = muta->mutate("a");
	REQUIRE(muta->nextDelta() == 1);
	REQUIRE(muta->nextDelta() == -1);

	delete muta;
}

bool statelessMutaCalled = false;
void statelessMuta(std::vector<std::string>& lines, Radamsa::Random* rand) {
	statelessMutaCalled = true;
}

TEST_CASE("Ensure StatelessLineMutation calls a stateful operation function", "[mutation][line][stateless]") {
	Radamsa::Random rand(0);
	struct radamsa_constants consts { 0 };
	Radamsa::AbstractLineMutation* muta = new Radamsa::StatelessLineMutation(0, consts, rand, statelessMuta, "State");

	REQUIRE(muta->nextDelta() == -1);
	REQUIRE_FALSE(statelessMutaCalled);

	std::string out = muta->mutate("abcde");
	REQUIRE(muta->nextDelta() == 1);
	REQUIRE(statelessMutaCalled);

	REQUIRE(muta->nextDelta() == -1);
	delete muta;
}

bool stateMutaCalled = false;
void stateMuta(std::vector<std::string>& lines, Radamsa::LineState state, Radamsa::Random* rand) {
	stateMutaCalled = true;
}

TEST_CASE("Ensure StatefulLineMutation calls a stateful operation function", "[mutation][line][stateful]") {
	Radamsa::Random rand(0);
	struct radamsa_constants consts { 0 };
	Radamsa::AbstractLineMutation *muta = new Radamsa::StatefulLineMutation(0, consts, rand, stateMuta, "State");
	
	REQUIRE(muta->nextDelta() == -1);
	REQUIRE_FALSE(stateMutaCalled);

	std::string out = muta->mutate("abcde");
	REQUIRE(muta->nextDelta() == 1);
	REQUIRE(stateMutaCalled);
	
	REQUIRE(muta->nextDelta() == -1);
	delete muta;
}


