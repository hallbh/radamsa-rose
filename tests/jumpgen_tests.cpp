#include "catch.hpp"
#include "generator/jumpgenerator.h"

TEST_CASE("Test Jump Generator on no file path", "[!mayfail][generator][gen-jump]") {
	INFO("Fails when not run in tests directory");
	struct radamsa_constants consts = { 0 };
	consts.minBlockSize = 5;
	consts.maxBlockSize = 20;
	consts.searchFuel = 100000;
	consts.searchStopIp = 8;
	Radamsa::Random random(0);
	random.nextInteger(10);
	Radamsa::JumpGenerator gen(0, consts, random);
	std::deque<std::string> out = gen.generate("");

	REQUIRE(out.size() == 0);
}

TEST_CASE("Test Jump Generator on invalid input files", "[!mayfail][generator][gen-jump]") {
	INFO("Fails when not run in tests directory");

	SECTION("One File") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);
		std::deque<std::string> out = gen.generate("   test_data/asdf.txt");

		REQUIRE(out.front() == "   test_data/asdf.txt");
		REQUIRE(out.size() == 1);
	}
	
	SECTION("Two Files") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);
		std::deque<std::string> out = gen.generate("test_data/empty.txt, test_data/asdf.txt");

		REQUIRE(out.front() == " test_data/asdf.txt");
		REQUIRE(out.back() == "test_data/empty.txt");
		REQUIRE(out.size() == 2);
	}
}

TEST_CASE("Test Jump Generator on empty data file", "[!mayfail][generator][gen-jump]") {
	INFO("Fails when not run in tests directory");

	struct radamsa_constants consts = { 0 };
	consts.minBlockSize = 256;
	consts.maxBlockSize = 4096;
	Radamsa::Random random(0);
	random.nextInteger(10);
	Radamsa::JumpGenerator gen(0, consts, random);

	std::deque<std::string> out = gen.generate("test_data/empty.txt");
	REQUIRE(random.peekState() == 11376583);
	REQUIRE(out.size() == 0);
}

TEST_CASE("Test Jump Generator on one file", "[!mayfail][generator][gen-jump]") {
	INFO("Fails when not run in tests directory");

	SECTION("Seed 5") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(5);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpC.txt");

		REQUIRE(random.peekState() == 3640253);

		REQUIRE(out.front() == "abcde");
		REQUIRE(out.back() == "vwxyz");

		REQUIRE(out.size() == 3);
	}

	SECTION("Seed 38") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(38);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpB.txt");

		REQUIRE(random.peekState() == 1407516);

		REQUIRE(out.front() == "opqrstuvwxyz");

		REQUIRE(out.size() == 1);
	}
}

TEST_CASE("Test Jump Generator on two smaller files", "[!mayfail][generator][gen-jump]") {
	INFO("Fails when not run in tests directory");

	SECTION("Seed 0") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpA.txt, test_data/jumpB.txt");

		REQUIRE(random.peekState() == 11376583);

		REQUIRE(out.front() == "fghijklmn");

		REQUIRE(out.size() == 1);
	}

	SECTION("Seed 1") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(1);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpA.txt, test_data/jumpB.txt");

		REQUIRE(random.peekState() == 4529424);

		REQUIRE(out.front() == "abcdevwxyz");

		REQUIRE(out.size() == 1);
	}

	SECTION("Seed 6") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(6);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpA.txt,test_data/jumpB.txt");

		REQUIRE(random.peekState() == 2619630);

		REQUIRE(out.front() == "abcdefghij");
		REQUIRE(out.back() == "tuvwxyz");

		REQUIRE(out.size() == 3);
	}
}

TEST_CASE("Test Jump Generator on two smaller files with spaces in directory and filename", "[!mayfail][generator][gen-jump]") {
	INFO("Fails when not run in tests directory");

	SECTION("Seed 0") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("     test_data/jumpA.txt, test_data/jumpB.txt");

		REQUIRE(random.peekState() == 11376583);

		REQUIRE(out.front() == "fghijklmn");

		REQUIRE(out.size() == 1);
	}

	SECTION("Seed 1") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(1);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpA.txt, test_data/jump B.txt");

		REQUIRE(random.peekState() == 4529424);

		REQUIRE(out.front() == "abcdevwxyz");

		REQUIRE(out.size() == 1);
	}

	SECTION("Seed 6") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(6);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpA.txt,   test_data/jump B.txt");

		REQUIRE(random.peekState() == 2619630);

		REQUIRE(out.front() == "abcdefghij");
		REQUIRE(out.back() == "tuvwxyz");

		REQUIRE(out.size() == 3);
	}
}

TEST_CASE("Test Jump Generator on two larger files", "[!mayfail][generator][gen-jump]") {
	INFO("Fails when not run in tests directory");

	SECTION("Seed 0") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(0);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpC.txt,test_data/jumpD.txt");

		REQUIRE(random.peekState() == 11376583);

		REQUIRE(out.front() == "1234567");
		REQUIRE(out.back() == "tuvwxyz");

		REQUIRE(out.size() == 3);
	}

	SECTION("Seed 12") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(12);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpC.txt, test_data/jumpD.txt");

		REQUIRE(random.peekState() == 287209);

		REQUIRE(out.front() == "12345");
		REQUIRE(out.back() == "6789123456789");

		REQUIRE(out.size() == 5);
	}

	SECTION("Seed 27") {
		struct radamsa_constants consts = { 0 };
		consts.minBlockSize = 5;
		consts.maxBlockSize = 20;
		consts.searchFuel = 100000;
		consts.searchStopIp = 8;
		Radamsa::Random random(27);
		random.nextInteger(10);
		Radamsa::JumpGenerator gen(0, consts, random);

		std::deque<std::string> out = gen.generate("test_data/jumpC.txt,test_data/jumpD.txt");

		REQUIRE(random.peekState() == 9814237);

		REQUIRE(out.front() == "123456789");
		REQUIRE(out.back() == "123456789123uvwxyz");

		REQUIRE(out.size() == 2);
	}
}