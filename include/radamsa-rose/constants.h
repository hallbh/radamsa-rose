#pragma once
#include <stdlib.h>
#include <stdint.h>

struct rational {
	uint32_t numerator;
	uint32_t denominator;
};

struct radamsa_constants {
	int maxHashCount;
	int searchFuel;
	int searchStopIp;
	int storedElements;
	int updateProbability;
	int minScore;
	int maxScore;
	struct rational pWeakUsually;
	int randDeltaIncrease;
	int randDeltaDecrease;
	int minTexty;
	int maxMutations;
	int averageBlockSize;
	int minBlockSize;
	int initialIp;
	struct rational remutateProbability;
	int maxBlockSize;
	int maxPumpLimit;
	// More constants here
};