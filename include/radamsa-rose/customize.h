#pragma once
#include "constants.h"
#include "radamsa_definitions.h"
#include "radamsa-rose_export.h"

RADAMSA_ROSE_EXPORT unsigned long long nextInteger(radamsa_random rand, unsigned long long max);
RADAMSA_ROSE_EXPORT int doesOccur(radamsa_random rand, struct rational probability);
RADAMSA_ROSE_EXPORT unsigned int nextBlockSize(radamsa_random rand, unsigned int minSize, unsigned int maxSize);
RADAMSA_ROSE_EXPORT int randomDelta(radamsa_random rand);
RADAMSA_ROSE_EXPORT int randomDeltaUp(radamsa_random rand, struct rational probability);
RADAMSA_ROSE_EXPORT unsigned long long nextNBit(radamsa_random rand, unsigned long long n);
RADAMSA_ROSE_EXPORT unsigned long long nextLogInteger(radamsa_random rand, unsigned long long max);
RADAMSA_ROSE_EXPORT unsigned long long nextIntegerRange(radamsa_random rand, unsigned long long low, unsigned long long high);
RADAMSA_ROSE_EXPORT uint32_t generateNext(radamsa_random rand);

// Helpers for custom patterns
RADAMSA_ROSE_EXPORT struct input_chunk* chooseChunk(struct input_chunk* head, int initialIp, radamsa_random rand);
RADAMSA_ROSE_EXPORT void runRadamsaMutation(radamsa_mutation muta, struct input_chunk* head);