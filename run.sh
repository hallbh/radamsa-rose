#!/bin/bash


echo "input:"
read input
printf "\n"


echo "seed:"
read seed
printf "\n"



printf "Radamsa:\n"
echo $input | radamsa --seed $seed
rad=$(echo $input | radamsa --seed $seed)

printf "\Radamsa-rose:\n"
echo $input | ./rad_rose $seed
test=$(echo $input | ./rad_rose $seed)

printf "\n"

if [ "$rad" = "$test" ]; then
    echo "Two outputs are equal."
else
    echo "Two outputs are not equal."
fi
