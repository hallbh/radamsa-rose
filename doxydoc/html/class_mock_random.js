var class_mock_random =
[
    [ "MockRandom", "class_mock_random.html#a5a5bfebba43eb784474183c6d409a4c1", null ],
    [ "allExpectedCallsMade", "class_mock_random.html#a61ed8e36ad687a11c4b8f86b305b288e", null ],
    [ "doesOccur", "class_mock_random.html#af83066c6529b806f7e16391a5cca6d1b", null ],
    [ "expectedNext", "class_mock_random.html#a9c656113c676917a4b3d799ecd2563eb", null ],
    [ "nextBlockSize", "class_mock_random.html#abb06c2211edb088624baf8ea8bae39f6", null ],
    [ "nextInteger", "class_mock_random.html#add455a87fa6c5316c0d9dc4f2193000e", null ],
    [ "nextIntegerRange", "class_mock_random.html#a580dfa3b4f6f33d93640a4e5ddc5151b", null ],
    [ "nextLogInteger", "class_mock_random.html#a1014a961582695f500b33fe43210e020", null ],
    [ "nextNBit", "class_mock_random.html#a675d419bc2bc9d051bf159ebc9bff586", null ],
    [ "randomDelta", "class_mock_random.html#a0be28c1031237b96e74f258cb547ee37", null ],
    [ "randomDeltaUp", "class_mock_random.html#afd6eed66d895c8fb41af5eb738e784a8", null ],
    [ "reservoirSample", "class_mock_random.html#a7cbf5035f70fc92ebbbf371f3cbfce5e", null ]
];