var line__operation_8h =
[
    [ "cloneLineOperation", "line__operation_8h.html#aabd5f065d681e2b27770fa0a240fdcc9", null ],
    [ "deleteLineOperation", "line__operation_8h.html#ad3e668716719e79319a4e71ecfbfd296", null ],
    [ "deleteLineSequenceOperation", "line__operation_8h.html#a70ea9cadc4f2ae2f31d093f9d296a03a", null ],
    [ "duplicateLineOperation", "line__operation_8h.html#a0b5cc80d8eecdb439c3a5f96e9143dfa", null ],
    [ "insertLineOperation", "line__operation_8h.html#a824e373dcefcc9b86ea75ef3ef1f22b9", null ],
    [ "permuteLineOperation", "line__operation_8h.html#ac9630d62ddb3967968888bab5b44bf66", null ],
    [ "repeatLineOperation", "line__operation_8h.html#a8561891b16afa30ccd123beeb79b16ef", null ],
    [ "replaceLineOperation", "line__operation_8h.html#a6c124a8025ff5715d94da888767524d6", null ],
    [ "swapLineOperation", "line__operation_8h.html#a43061092a0fead83df25bb32d474aa4a", null ]
];